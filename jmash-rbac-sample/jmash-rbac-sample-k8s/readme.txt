TLS TEST

grpcurl jmash.crenjoy.com:443 grpc.health.v1.Health/Check

# no TLS TEST
kubectl exec -it grpcurl -n jmash -- sh

grpcurl -plaintext jmash-rbac-service:50051 grpc.health.v1.Health/Check