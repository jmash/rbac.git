import subprocess
import argparse
import pathlib 
import sys
# export PYTHONPATH="/data/work/jmash/python/jmash-cmd"
import jmash_cmd.mvn
import jmash_cmd.gw
import jmash_cmd.docker
import jmash_cmd.k8s

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="K8S应用程序管理脚本")
    parser.add_argument("action", choices=["package","publish","deploy", "restart", "delete", "scale", "test","notest"], help="要执行的操作")
    parser.add_argument("arch", choices=["amd64", "arm64"], help="目标架构")
    
    parser.add_argument("--namespace", default="jmash", help="命名空间")
    parser.add_argument("--name", default="rbac-sample", help="应用程序名称")
    parser.add_argument("--version", default="1.0.0", help="版本")
    parser.add_argument("--replicas", default="1", help="副本数量")
            
    args = parser.parse_args()
    # 路径
    args.path= pathlib.Path.cwd()
    print(args);
    mirror="harbor.crenjoy.com:9443"
    
    
    if args.action == "package":
        jmash_cmd.mvn.mvn_main_sample(args.namespace,args.name)   
        jmash_cmd.gw.gw_main(f"../{args.namespace}-{args.name}-gateway",args.arch,args.namespace,args.name)
    elif args.action == "publish":
        jmash_cmd.docker.docker_sample_build_push(args.namespace,args.name,args.arch, args.version,mirror)
    elif args.action == "deploy":
        jmash_cmd.k8s.k8s_deploy_main(".",args.namespace,args.name,args.arch,args.version,mirror)
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","captcha")
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","dict")
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","file")
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","sms")
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","rbac")
        jmash_cmd.k8s.k8s_deploy_rm_subapp("jmash","region")  
    elif args.action == "restart":
        jmash_cmd.k8s.k8s_restart_main(args.namespace,args.name,args.arch)
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-captcha-k8s")
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-dict-k8s")
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-file-k8s")
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-sms-k8s")
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-rbac-k8s")
        jmash_cmd.k8s.k8s_deploy_subapp("./jmash-region-k8s")   
    elif args.action == "delete":
        jmash_cmd.k8s.k8s_delete_main(args.namespace,args.name,args.arch)
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-captcha-k8s")
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-dict-k8s")
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-file-k8s")
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-sms-k8s")
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-rbac-k8s")
        jmash_cmd.k8s.k8s_delete_subapp("./jmash-region-k8s")   
    elif args.action == "scale":
        jmash_cmd.k8s.k8s_scale(args.namespace,args.name,args.replicas)

  
