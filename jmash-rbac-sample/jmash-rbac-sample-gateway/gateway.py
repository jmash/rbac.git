import os
import subprocess
import shutil
import platform
# export PYTHONPATH="/data/work/jmash/python/jmash-cmd"
import jmash_cmd.gw


if __name__ == "__main__":
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-core-gateway","master")
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway","master")
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway","master")
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway","master")
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway","master")
    #jmash_cmd.gw.gw_go_get(".","gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway","master")
        
    jmash_cmd.gw.gw_go_copy_openapi(".","gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway")
    jmash_cmd.gw.gw_go_copy_openapi(".","gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway")
    jmash_cmd.gw.gw_go_copy_openapi(".","gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway")
    jmash_cmd.gw.gw_go_copy_openapi(".","gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway")
    jmash_cmd.gw.gw_go_copy_openapi(".","gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway")
    
    jmash_cmd.gw.gw_main(".","amd64","jmash","rbac-sample")
