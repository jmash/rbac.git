
export CURRENT_DR=$(pwd)

cd $(dirname $0)

#  初始化go mod 
go mod init jmash-rbac-sample-gateway

# go get gitee.com/jmash/jmash/trunk/jmash-core-gateway@master
# go get gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway@master
# go get gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway@master
# go get gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway@master
# go get gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway@master
# go get gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway@master

# 更新mod 
go mod tidy

parts=($(go list -m  gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway))
cp -rf ~/go/pkg/mod/${parts[0]}@${parts[1]}/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

parts=($(go list -m  gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway))
cp -rf ~/go/pkg/mod/${parts[0]}@${parts[1]}/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

parts=($(go list -m  gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway))
cp -rf ~/go/pkg/mod/${parts[0]}@${parts[1]}/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

parts=($(go list -m  gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway))
cp -rf ~/go/pkg/mod/${parts[0]}@${parts[1]}/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

parts=($(go list -m  gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway))
cp -rf ~/go/pkg/mod/${parts[0]}@${parts[1]}/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

# Local openapi.
cp -rf ../../jmash-rbac/trunk/jmash-rbac-gateway/openapi/jmash  ./openapi/
chmod -R u+rw ./openapi

# copy 
cp -rf openapi ./bin/
cp -rf openapi-ui ./bin/

#  build 

export GOOS=windows
export GOARCH=amd64

go build -o ./bin/main.exe ./src/main.go

export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64

go build -o ./bin/main_amd64 ./src/main.go

export GOARCH=arm64
go build -o ./bin/main_arm64 ./src/main.go

cd $CURRENT_DIR
