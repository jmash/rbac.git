# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/sms/sms_rpc.proto](#jmash_sms_sms_rpc-proto)
    - [Sms](#jmash-sms-Sms)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_sms_sms_rpc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/sms/sms_rpc.proto


 

 

 


<a name="jmash-sms-Sms"></a>

### Sms
Sms Service

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| version | [.google.protobuf.Empty](#google-protobuf-Empty) | [.google.protobuf.StringValue](#google-protobuf-StringValue) | 版本 |
| findEnumList | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.EnumValueList](#jmash-protobuf-EnumValueList) | 枚举值列表 |
| findEnumMap | [.google.protobuf.StringValue](#google-protobuf-StringValue) | [.jmash.protobuf.CustomEnumValueMap](#jmash-protobuf-CustomEnumValueMap) | 枚举值Map |
| findEnumEntry | [.jmash.protobuf.EnumEntryReq](#jmash-protobuf-EnumEntryReq) | [.jmash.protobuf.EntryList](#jmash-protobuf-EntryList) | 枚举值 |
| sendSmsCaptcha | [SmsCaptchaReq](#jmash-sms-SmsCaptchaReq) | [SmsResp](#jmash-sms-SmsResp) | 发送短信验证码 |
| sendSms | [SmsBasicReq](#jmash-sms-SmsBasicReq) | [SmsResp](#jmash-sms-SmsResp) | 发送短信 |
| sendEmailCaptcha | [EmailCaptchaReq](#jmash-sms-EmailCaptchaReq) | [SmsResp](#jmash-sms-SmsResp) | 发送邮件验证码 |
| sendEmail | [EmailBasicReq](#jmash-sms-EmailBasicReq) | [SmsResp](#jmash-sms-SmsResp) | 发送电子邮件 |
| findSmsAppConfigPage | [SmsAppConfigReq](#jmash-sms-SmsAppConfigReq) | [SmsAppConfigPage](#jmash-sms-SmsAppConfigPage) | 查询翻页信息消息应用配置 |
| findSmsAppConfigList | [SmsAppConfigReq](#jmash-sms-SmsAppConfigReq) | [SmsAppConfigList](#jmash-sms-SmsAppConfigList) | 查询列表信息消息应用配置 |
| findSmsAppConfigById | [SmsAppConfigKey](#jmash-sms-SmsAppConfigKey) | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | 查询消息应用配置 |
| createSmsAppConfig | [SmsAppConfigCreateReq](#jmash-sms-SmsAppConfigCreateReq) | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | 创建实体消息应用配置 |
| updateSmsAppConfig | [SmsAppConfigUpdateReq](#jmash-sms-SmsAppConfigUpdateReq) | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | 修改实体消息应用配置 |
| deleteSmsAppConfig | [SmsAppConfigKey](#jmash-sms-SmsAppConfigKey) | [SmsAppConfigModel](#jmash-sms-SmsAppConfigModel) | 删除消息应用配置 |
| batchDeleteSmsAppConfig | [SmsAppConfigKeyList](#jmash-sms-SmsAppConfigKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除消息应用配置 |
| syncSmsSign | [SmsAppConfigKey](#jmash-sms-SmsAppConfigKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 同步短信签名 |
| findSmsSignPage | [SmsSignReq](#jmash-sms-SmsSignReq) | [SmsSignPage](#jmash-sms-SmsSignPage) | 查询翻页信息短信签名 |
| findSmsSignList | [SmsSignReq](#jmash-sms-SmsSignReq) | [SmsSignList](#jmash-sms-SmsSignList) | 查询列表信息短信签名 |
| findSmsSignById | [SmsSignKey](#jmash-sms-SmsSignKey) | [SmsSignModel](#jmash-sms-SmsSignModel) | 查询短信签名 |
| deleteSmsSign | [SmsSignKey](#jmash-sms-SmsSignKey) | [SmsSignModel](#jmash-sms-SmsSignModel) | 删除短信签名 |
| batchDeleteSmsSign | [SmsSignKeyList](#jmash-sms-SmsSignKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除短信签名 |
| syncSmsTemplate | [SmsAppConfigKey](#jmash-sms-SmsAppConfigKey) | [.google.protobuf.BoolValue](#google-protobuf-BoolValue) | 同步短消息模板 |
| findSmsTemplatePage | [SmsTemplateReq](#jmash-sms-SmsTemplateReq) | [SmsTemplatePage](#jmash-sms-SmsTemplatePage) | 查询翻页信息短消息模板 |
| findSmsTemplateList | [SmsTemplateReq](#jmash-sms-SmsTemplateReq) | [SmsTemplateList](#jmash-sms-SmsTemplateList) | 查询列表信息短消息模板 |
| findSmsTemplateById | [SmsTemplateKey](#jmash-sms-SmsTemplateKey) | [SmsTemplateModel](#jmash-sms-SmsTemplateModel) | 查询短消息模板 |
| deleteSmsTemplate | [SmsTemplateKey](#jmash-sms-SmsTemplateKey) | [SmsTemplateModel](#jmash-sms-SmsTemplateModel) | 删除短消息模板 |
| batchDeleteSmsTemplate | [SmsTemplateKeyList](#jmash-sms-SmsTemplateKeyList) | [.google.protobuf.Int32Value](#google-protobuf-Int32Value) | 批量删除短消息模板 |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

