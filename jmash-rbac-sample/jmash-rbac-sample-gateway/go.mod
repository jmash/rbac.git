module jmash-rbac-sample-gateway

go 1.22.7

toolchain go1.22.9

require (
	gitee.com/jmash/jmash/trunk/jmash-captcha/jmash-captcha-gateway v0.0.0-20241118082432-c96fe7e9f13b
	gitee.com/jmash/jmash/trunk/jmash-core-gateway v0.0.0-20250211111429-cc2a5c2ee4f3
	gitee.com/jmash/jmash/trunk/jmash-dict/jmash-dict-gateway v0.0.0-20241028084513-2f923d2d6111
	gitee.com/jmash/jmash/trunk/jmash-file/jmash-file-gateway v0.0.0-20250116084302-8338709fd795
	gitee.com/jmash/jmash/trunk/jmash-region/jmash-region-gateway v0.0.0-20241118095426-3155a4d60ebe
	gitee.com/jmash/jmash/trunk/jmash-sms/jmash-sms-gateway v0.0.0-20241118095426-3155a4d60ebe
	gitee.com/jmash/rbac/jmash-rbac/trunk/jmash-rbac-gateway v0.0.0-20241024152031-70d519264d5b
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.23.0
	google.golang.org/grpc v1.68.0
)

require (
	github.com/golang/glog v1.2.3 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	golang.org/x/net v0.29.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20241021214115-324edc3d5d38 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241021214115-324edc3d5d38 // indirect
	google.golang.org/protobuf v1.35.1 // indirect
)

replace gitee.com/jmash/rbac/jmash-rbac/trunk/jmash-rbac-gateway => ../../jmash-rbac/trunk/jmash-rbac-gateway
