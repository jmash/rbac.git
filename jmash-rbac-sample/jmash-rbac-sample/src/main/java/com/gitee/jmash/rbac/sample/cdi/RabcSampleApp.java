package com.gitee.jmash.rbac.sample.cdi;

import com.gitee.jmash.core.grpc.DefaultGrpcServer;
import com.gitee.jmash.rbac.shiro.JmashShiroConfig;
import com.luciad.imageio.webp.WebP;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;

public class RabcSampleApp {

  public static void main(String[] args) throws Exception {
    try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
      // 独立部署 Shiro 初始化.
      JmashShiroConfig.config();
      // 局部模块化部署,注意redis 和 连接配置文件.
      // JmashClientShiroConfig.config();
      // Webp加载
      WebP.loadNativeLibrary();
      final DefaultGrpcServer server = container.select(DefaultGrpcServer.class).get();
      server.start(false);
      server.blockUntilShutdown();
    }
  }
}
