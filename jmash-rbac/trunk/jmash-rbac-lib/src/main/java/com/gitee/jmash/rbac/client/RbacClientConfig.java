
package com.gitee.jmash.rbac.client;

import com.gitee.jmash.common.grpc.client.GrpcChannel;
import com.gitee.jmash.rbac.client.token.ClientAuthProps;
import io.grpc.ManagedChannel;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/** rbac Client Config . */
@ApplicationScoped
public class RbacClientConfig {

  protected static ManagedChannel channel = null;

  public static synchronized ManagedChannel getManagedChannel() {
    if (null != channel && !channel.isShutdown() && !channel.isTerminated()) {
      return channel;
    }
    // k8s环境获取后端服务,本地环境获取测试服务.
    channel = GrpcChannel.getServiceChannel("jmash-rbac-service.jmash.svc.cluster.local");
    return channel;
  }

  /** System User Use. */
  @Inject
  @ConfigProperties
  ClientAuthProps clientAuthProps;

}
