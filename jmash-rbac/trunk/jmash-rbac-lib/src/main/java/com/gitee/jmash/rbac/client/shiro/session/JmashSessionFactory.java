package com.gitee.jmash.rbac.client.shiro.session;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.session.mgt.SimpleSession;

/**
 * Jmash Shiro Session Factory.
 *
 * @author cgd
 */
public class JmashSessionFactory implements SessionFactory {

  /** 创建Session. */
  public Session createSession(SessionContext initData) {
    SimpleSession session = new SimpleSession();
    if (initData != null) {
      session.setHost(initData.getHost());
      session.setId(initData.getSessionId());
    }
    return session;
  }

}
