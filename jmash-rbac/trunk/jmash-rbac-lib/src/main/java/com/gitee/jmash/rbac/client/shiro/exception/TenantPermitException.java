package com.gitee.jmash.rbac.client.shiro.exception;

import com.gitee.jmash.common.security.JmashPrincipal;

/** 跨租户的权限越权异常 */
public class TenantPermitException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  // 访问租户tenant.
  private String tenant;
  // 当前登录用户Token.
  private JmashPrincipal principal;


  public TenantPermitException(String tenant, JmashPrincipal principal) {
    super();
    this.tenant = tenant;
    this.principal = principal;
  }

  @Override
  public String getMessage() {
    return "403 Forbidden User ID " + principal.getName() + " tenant: " + principal.getTenant()
        + " request tenant : " + tenant;
  }

}
