package com.gitee.jmash.rbac.client.shiro.authc;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * Jmash Shiro Token.
 * 
 * @author cgd
 */
public class JmashShiroToken implements AuthenticationToken {

  private static final long serialVersionUID = 1L;

  private String tenant;

  private String directoryId;

  private String userName;

  private String password;

  private String clientId;


  public JmashShiroToken(String tenant, String directoryId, String userName, String password) {
    super();
    this.directoryId = directoryId;
    this.userName = userName;
    this.password = password;
  }

  public JmashShiroToken(String tenant, String directoryId, String userName, String password,
      String clientId) {
    super();
    this.directoryId = directoryId;
    this.userName = userName;
    this.password = password;
    this.clientId = clientId;
  }


  @Override
  public Object getPrincipal() {
    return this.userName;
  }

  @Override
  public Object getCredentials() {
    return this.password;
  }



  public String getTenant() {
    return tenant;
  }

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

  public String getDirectoryId() {
    return directoryId;
  }

  public void setDirectoryId(String directoryId) {
    this.directoryId = directoryId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

}
