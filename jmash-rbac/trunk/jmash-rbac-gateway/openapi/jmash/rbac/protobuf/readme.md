# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [jmash/rbac/protobuf/miniapp_message.proto](#jmash_rbac_protobuf_miniapp_message-proto)
    - [MiniAppBindPhoneReq](#jmash-rbac-MiniAppBindPhoneReq)
    - [MiniAppLoginReq](#jmash-rbac-MiniAppLoginReq)
    - [MiniAppLoginResp](#jmash-rbac-MiniAppLoginResp)
    - [MiniAppPhoneNumberReq](#jmash-rbac-MiniAppPhoneNumberReq)
  
- [jmash/rbac/protobuf/mobileapp_message.proto](#jmash_rbac_protobuf_mobileapp_message-proto)
    - [MobileAppLoginBindPhoneReq](#jmash-rbac-MobileAppLoginBindPhoneReq)
    - [MobileAppLoginReq](#jmash-rbac-MobileAppLoginReq)
    - [MobileAppLoginResp](#jmash-rbac-MobileAppLoginResp)
  
- [jmash/rbac/protobuf/rbac_auth_message.proto](#jmash_rbac_protobuf_rbac_auth_message-proto)
    - [ActiveRoleReq](#jmash-rbac-ActiveRoleReq)
    - [BindPhoneEmailReq](#jmash-rbac-BindPhoneEmailReq)
    - [ChangePwdReq](#jmash-rbac-ChangePwdReq)
    - [LoginQrcodeReq](#jmash-rbac-LoginQrcodeReq)
    - [LoginReq](#jmash-rbac-LoginReq)
    - [LogoutReq](#jmash-rbac-LogoutReq)
    - [Menu](#jmash-rbac-Menu)
    - [MenuList](#jmash-rbac-MenuList)
    - [MenuMeta](#jmash-rbac-MenuMeta)
    - [RefreshTokenReq](#jmash-rbac-RefreshTokenReq)
    - [RolePermSet](#jmash-rbac-RolePermSet)
    - [RunAsReq](#jmash-rbac-RunAsReq)
    - [SendValidCodeReq](#jmash-rbac-SendValidCodeReq)
    - [TokenResp](#jmash-rbac-TokenResp)
    - [TreeList](#jmash-rbac-TreeList)
    - [TreeModel](#jmash-rbac-TreeModel)
    - [ValidCodeLoginReq](#jmash-rbac-ValidCodeLoginReq)
  
- [jmash/rbac/protobuf/rbac_dept_message.proto](#jmash_rbac_protobuf_rbac_dept_message-proto)
    - [DeptCreateReq](#jmash-rbac-DeptCreateReq)
    - [DeptEnableKey](#jmash-rbac-DeptEnableKey)
    - [DeptExportReq](#jmash-rbac-DeptExportReq)
    - [DeptImportReq](#jmash-rbac-DeptImportReq)
    - [DeptKey](#jmash-rbac-DeptKey)
    - [DeptKeyList](#jmash-rbac-DeptKeyList)
    - [DeptList](#jmash-rbac-DeptList)
    - [DeptModel](#jmash-rbac-DeptModel)
    - [DeptModelTotal](#jmash-rbac-DeptModelTotal)
    - [DeptMoveKey](#jmash-rbac-DeptMoveKey)
    - [DeptPage](#jmash-rbac-DeptPage)
    - [DeptReq](#jmash-rbac-DeptReq)
    - [DeptUpdateReq](#jmash-rbac-DeptUpdateReq)
    - [EnableDeptReq](#jmash-rbac-EnableDeptReq)
  
- [jmash/rbac/protobuf/rbac_log_message.proto](#jmash_rbac_protobuf_rbac_log_message-proto)
    - [LogDelReq](#jmash-rbac-LogDelReq)
    - [LogExportReq](#jmash-rbac-LogExportReq)
    - [LogKey](#jmash-rbac-LogKey)
    - [LogKeyList](#jmash-rbac-LogKeyList)
    - [LogList](#jmash-rbac-LogList)
    - [LogModel](#jmash-rbac-LogModel)
    - [LogModelTotal](#jmash-rbac-LogModelTotal)
    - [LogPage](#jmash-rbac-LogPage)
    - [LogReq](#jmash-rbac-LogReq)
  
- [jmash/rbac/protobuf/rbac_module_message.proto](#jmash_rbac_protobuf_rbac_module_message-proto)
    - [ModuleCheck](#jmash-rbac-ModuleCheck)
    - [ModuleCreateReq](#jmash-rbac-ModuleCreateReq)
    - [ModuleExportReq](#jmash-rbac-ModuleExportReq)
    - [ModuleKey](#jmash-rbac-ModuleKey)
    - [ModuleKeyList](#jmash-rbac-ModuleKeyList)
    - [ModuleList](#jmash-rbac-ModuleList)
    - [ModuleModel](#jmash-rbac-ModuleModel)
    - [ModuleModelTotal](#jmash-rbac-ModuleModelTotal)
    - [ModuleMoveKey](#jmash-rbac-ModuleMoveKey)
    - [ModulePage](#jmash-rbac-ModulePage)
    - [ModuleReq](#jmash-rbac-ModuleReq)
    - [ModuleUpdateReq](#jmash-rbac-ModuleUpdateReq)
  
- [jmash/rbac/protobuf/rbac_open_app_message.proto](#jmash_rbac_protobuf_rbac_open_app_message-proto)
    - [OpenAppCreateReq](#jmash-rbac-OpenAppCreateReq)
    - [OpenAppKey](#jmash-rbac-OpenAppKey)
    - [OpenAppKeyList](#jmash-rbac-OpenAppKeyList)
    - [OpenAppList](#jmash-rbac-OpenAppList)
    - [OpenAppModel](#jmash-rbac-OpenAppModel)
    - [OpenAppModelTotal](#jmash-rbac-OpenAppModelTotal)
    - [OpenAppPage](#jmash-rbac-OpenAppPage)
    - [OpenAppReq](#jmash-rbac-OpenAppReq)
    - [OpenAppUpdateReq](#jmash-rbac-OpenAppUpdateReq)
  
- [jmash/rbac/protobuf/rbac_opens_message.proto](#jmash_rbac_protobuf_rbac_opens_message-proto)
    - [OpensKey](#jmash-rbac-OpensKey)
    - [OpensKeyList](#jmash-rbac-OpensKeyList)
    - [OpensList](#jmash-rbac-OpensList)
    - [OpensModel](#jmash-rbac-OpensModel)
    - [OpensReq](#jmash-rbac-OpensReq)
    - [UnionIdList](#jmash-rbac-UnionIdList)
    - [UserOpensReq](#jmash-rbac-UserOpensReq)
  
    - [OpensType](#jmash-rbac-OpensType)
  
- [jmash/rbac/protobuf/rbac_operation_message.proto](#jmash_rbac_protobuf_rbac_operation_message-proto)
    - [OperationCheck](#jmash-rbac-OperationCheck)
    - [OperationCreateReq](#jmash-rbac-OperationCreateReq)
    - [OperationExportReq](#jmash-rbac-OperationExportReq)
    - [OperationKey](#jmash-rbac-OperationKey)
    - [OperationKeyList](#jmash-rbac-OperationKeyList)
    - [OperationList](#jmash-rbac-OperationList)
    - [OperationModel](#jmash-rbac-OperationModel)
    - [OperationModelTotal](#jmash-rbac-OperationModelTotal)
    - [OperationMoveKey](#jmash-rbac-OperationMoveKey)
    - [OperationPage](#jmash-rbac-OperationPage)
    - [OperationReq](#jmash-rbac-OperationReq)
    - [OperationUpdateReq](#jmash-rbac-OperationUpdateReq)
  
- [jmash/rbac/protobuf/rbac_perm_message.proto](#jmash_rbac_protobuf_rbac_perm_message-proto)
    - [PermCheck](#jmash-rbac-PermCheck)
    - [PermCreateReq](#jmash-rbac-PermCreateReq)
    - [PermKey](#jmash-rbac-PermKey)
    - [PermKeyList](#jmash-rbac-PermKeyList)
    - [PermList](#jmash-rbac-PermList)
    - [PermModel](#jmash-rbac-PermModel)
    - [PermModelTotal](#jmash-rbac-PermModelTotal)
    - [PermPage](#jmash-rbac-PermPage)
    - [PermReq](#jmash-rbac-PermReq)
    - [PermUpdateReq](#jmash-rbac-PermUpdateReq)
    - [ResourcePerm](#jmash-rbac-ResourcePerm)
    - [ResourcePermList](#jmash-rbac-ResourcePermList)
  
- [jmash/rbac/protobuf/rbac_resource_message.proto](#jmash_rbac_protobuf_rbac_resource_message-proto)
    - [ResourceCreateReq](#jmash-rbac-ResourceCreateReq)
    - [ResourceExportReq](#jmash-rbac-ResourceExportReq)
    - [ResourceImportReq](#jmash-rbac-ResourceImportReq)
    - [ResourceKey](#jmash-rbac-ResourceKey)
    - [ResourceKeyList](#jmash-rbac-ResourceKeyList)
    - [ResourceList](#jmash-rbac-ResourceList)
    - [ResourceModel](#jmash-rbac-ResourceModel)
    - [ResourceModelTotal](#jmash-rbac-ResourceModelTotal)
    - [ResourceMoveKey](#jmash-rbac-ResourceMoveKey)
    - [ResourcePage](#jmash-rbac-ResourcePage)
    - [ResourceReq](#jmash-rbac-ResourceReq)
    - [ResourceUpdateReq](#jmash-rbac-ResourceUpdateReq)
    - [VerifyResourceReq](#jmash-rbac-VerifyResourceReq)
  
    - [ResourceType](#jmash-rbac-ResourceType)
  
- [jmash/rbac/protobuf/rbac_role_message.proto](#jmash_rbac_protobuf_rbac_role_message-proto)
    - [DsdRoleListResp](#jmash-rbac-DsdRoleListResp)
    - [RoleCreateReq](#jmash-rbac-RoleCreateReq)
    - [RoleDuty](#jmash-rbac-RoleDuty)
    - [RoleDutyModel](#jmash-rbac-RoleDutyModel)
    - [RoleKey](#jmash-rbac-RoleKey)
    - [RoleKeyList](#jmash-rbac-RoleKeyList)
    - [RoleList](#jmash-rbac-RoleList)
    - [RoleModel](#jmash-rbac-RoleModel)
    - [RoleModelTotal](#jmash-rbac-RoleModelTotal)
    - [RoleMoveKey](#jmash-rbac-RoleMoveKey)
    - [RolePage](#jmash-rbac-RolePage)
    - [RolePermReq](#jmash-rbac-RolePermReq)
    - [RoleReq](#jmash-rbac-RoleReq)
    - [RoleUpdateReq](#jmash-rbac-RoleUpdateReq)
    - [VerifyRoleReq](#jmash-rbac-VerifyRoleReq)
  
    - [DutyType](#jmash-rbac-DutyType)
    - [RoleType](#jmash-rbac-RoleType)
  
- [jmash/rbac/protobuf/rbac_user_log_message.proto](#jmash_rbac_protobuf_rbac_user_log_message-proto)
    - [UserLogDelReq](#jmash-rbac-UserLogDelReq)
    - [UserLogExportReq](#jmash-rbac-UserLogExportReq)
    - [UserLogKey](#jmash-rbac-UserLogKey)
    - [UserLogKeyList](#jmash-rbac-UserLogKeyList)
    - [UserLogList](#jmash-rbac-UserLogList)
    - [UserLogModel](#jmash-rbac-UserLogModel)
    - [UserLogModelTotal](#jmash-rbac-UserLogModelTotal)
    - [UserLogPage](#jmash-rbac-UserLogPage)
    - [UserLogReq](#jmash-rbac-UserLogReq)
  
- [jmash/rbac/protobuf/rbac_user_message.proto](#jmash_rbac_protobuf_rbac_user_message-proto)
    - [ApprovedUserReq](#jmash-rbac-ApprovedUserReq)
    - [DirectoryListResp](#jmash-rbac-DirectoryListResp)
    - [EnableUserReq](#jmash-rbac-EnableUserReq)
    - [JobInfo](#jmash-rbac-JobInfo)
    - [LockUserReq](#jmash-rbac-LockUserReq)
    - [OrganUserCreateReq](#jmash-rbac-OrganUserCreateReq)
    - [ScanCodeCreateUserReq](#jmash-rbac-ScanCodeCreateUserReq)
    - [UpdateUserReq](#jmash-rbac-UpdateUserReq)
    - [UserCreateReq](#jmash-rbac-UserCreateReq)
    - [UserDeptJobInfoRes](#jmash-rbac-UserDeptJobInfoRes)
    - [UserEnableKey](#jmash-rbac-UserEnableKey)
    - [UserExportReq](#jmash-rbac-UserExportReq)
    - [UserImportReq](#jmash-rbac-UserImportReq)
    - [UserInfo](#jmash-rbac-UserInfo)
    - [UserInfoList](#jmash-rbac-UserInfoList)
    - [UserInfoPage](#jmash-rbac-UserInfoPage)
    - [UserJobs](#jmash-rbac-UserJobs)
    - [UserKey](#jmash-rbac-UserKey)
    - [UserKeyList](#jmash-rbac-UserKeyList)
    - [UserList](#jmash-rbac-UserList)
    - [UserModel](#jmash-rbac-UserModel)
    - [UserModelTotal](#jmash-rbac-UserModelTotal)
    - [UserNameReq](#jmash-rbac-UserNameReq)
    - [UserPage](#jmash-rbac-UserPage)
    - [UserReq](#jmash-rbac-UserReq)
    - [UserResetPwdReq](#jmash-rbac-UserResetPwdReq)
    - [UserRoleReq](#jmash-rbac-UserRoleReq)
    - [UserUpdateReq](#jmash-rbac-UserUpdateReq)
    - [VerifyUserReq](#jmash-rbac-VerifyUserReq)
  
    - [UserStatus](#jmash-rbac-UserStatus)
  
- [Scalar Value Types](#scalar-value-types)



<a name="jmash_rbac_protobuf_miniapp_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/miniapp_message.proto



<a name="jmash-rbac-MiniAppBindPhoneReq"></a>

### MiniAppBindPhoneReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| phone_code | [string](#string) |  | 手机号code |
| app_id | [string](#string) |  | 小程序ID |
| component_appid | [string](#string) |  | 第三方平台APPID，可选 |






<a name="jmash-rbac-MiniAppLoginReq"></a>

### MiniAppLoginReq
小程序登录请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| login_code | [string](#string) |  | 登录code |
| app_id | [string](#string) |  | 小程序ID |
| component_appid | [string](#string) |  | 第三方平台APPID，可选 |






<a name="jmash-rbac-MiniAppLoginResp"></a>

### MiniAppLoginResp
小程序登录响应.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [bool](#bool) |  | 成功状态 |
| token | [TokenResp](#jmash-rbac-TokenResp) |  | 成功时返回的Token |
| message | [string](#string) |  | 信息 |






<a name="jmash-rbac-MiniAppPhoneNumberReq"></a>

### MiniAppPhoneNumberReq
小程序获取手机号请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| phone_code | [string](#string) |  | 手机号code |
| login_code | [string](#string) |  | 登录code |
| app_id | [string](#string) |  | 小程序ID |
| component_appid | [string](#string) |  | 第三方平台APPID，可选 |
| nick_name | [string](#string) |  | 昵称 |





 

 

 

 



<a name="jmash_rbac_protobuf_mobileapp_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/mobileapp_message.proto



<a name="jmash-rbac-MobileAppLoginBindPhoneReq"></a>

### MobileAppLoginBindPhoneReq
移动App微信登录绑定手机号.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| app_id | [string](#string) |  | 移动微信ID |
| cache_key | [string](#string) |  | 缓存key |
| phone_number | [string](#string) |  | 手机号码 |
| verify_code | [string](#string) |  | 验证码 |
| nick_name | [string](#string) |  | 昵称 |






<a name="jmash-rbac-MobileAppLoginReq"></a>

### MobileAppLoginReq
移动App微信登录请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| login_code | [string](#string) |  | 登录code |
| app_id | [string](#string) |  | 移动微信appID |






<a name="jmash-rbac-MobileAppLoginResp"></a>

### MobileAppLoginResp
移动App微信登录响应.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [bool](#bool) |  | 成功状态 |
| token | [TokenResp](#jmash-rbac-TokenResp) |  | 成功时返回的Token |
| cache_key | [string](#string) |  | 缓存key |
| message | [string](#string) |  | 信息 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_auth_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_auth_message.proto



<a name="jmash-rbac-ActiveRoleReq"></a>

### ActiveRoleReq
激活角色请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_ids | [string](#string) | repeated | Active Role IDS. |






<a name="jmash-rbac-BindPhoneEmailReq"></a>

### BindPhoneEmailReq
绑定手机号/邮箱请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| name | [string](#string) |  | Email/Phone |
| valid_code | [string](#string) |  | 验证码 |






<a name="jmash-rbac-ChangePwdReq"></a>

### ChangePwdReq
密码修改请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| encode_old_pwd | [string](#string) |  | 原密码 |
| encode_new_pwd | [string](#string) |  | 新密码 |






<a name="jmash-rbac-LoginQrcodeReq"></a>

### LoginQrcodeReq
扫码登录请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| directory_id | [string](#string) |  | 目录ID |
| authorizer_appid | [string](#string) |  | 授权公众号AppID. |
| ticket | [string](#string) |  | 二维码扫码凭证. |
| client_id | [string](#string) |  | 客户端ID |
| scope | [string](#string) |  | 范围 |






<a name="jmash-rbac-LoginReq"></a>

### LoginReq
登录请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| directory_id | [string](#string) |  | 目录ID |
| user_name | [string](#string) |  | 登录用户. |
| encode_pwd | [string](#string) |  | 加密密钥. |
| client_id | [string](#string) |  | 客户端ID |
| scope | [string](#string) |  | 范围 |
| captcha_id | [string](#string) |  | 验证码ID Captcha ID ticket. |
| captcha_code | [string](#string) |  | 用户输入验证码文本. |






<a name="jmash-rbac-LogoutReq"></a>

### LogoutReq
登出.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| client_id | [string](#string) |  | 客户端ID |
| access_token | [string](#string) |  | 登录用户Token. |






<a name="jmash-rbac-Menu"></a>

### Menu
菜单


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| path | [string](#string) |  |  |
| component_name | [string](#string) |  |  |
| redirect | [string](#string) |  |  |
| name | [string](#string) |  |  |
| meta | [MenuMeta](#jmash-rbac-MenuMeta) |  |  |
| children | [Menu](#jmash-rbac-Menu) | repeated |  |






<a name="jmash-rbac-MenuList"></a>

### MenuList
菜单列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| menus | [Menu](#jmash-rbac-Menu) | repeated |  |






<a name="jmash-rbac-MenuMeta"></a>

### MenuMeta
菜单Meta


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| title | [string](#string) |  |  |
| icon | [string](#string) |  |  |
| hidden | [bool](#bool) |  |  |
| keep_alive | [bool](#bool) |  |  |






<a name="jmash-rbac-RefreshTokenReq"></a>

### RefreshTokenReq
刷新token


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| refresh_token | [string](#string) |  | 刷新Token. |
| client_id | [string](#string) |  | 客户端ID |






<a name="jmash-rbac-RolePermSet"></a>

### RolePermSet
角色列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| role_codes | [string](#string) | repeated | 角色编码列表 |
| perm_codes | [string](#string) | repeated | 权限编码列表 |






<a name="jmash-rbac-RunAsReq"></a>

### RunAsReq
切换身份


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 用户ID |
| scope | [string](#string) |  | 范围 |






<a name="jmash-rbac-SendValidCodeReq"></a>

### SendValidCodeReq
发送验证码.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| directory_id | [string](#string) |  | 目录ID |
| name | [string](#string) |  | Email/phone |
| captcha_id | [string](#string) |  | 验证码ID Captcha ID ticket. |
| captcha_code | [string](#string) |  | 用户输入验证码文本. |
| product | [string](#string) |  | 产品名称 |
| send_valid_code | [bool](#bool) |  | 是否发送验证码 false手机号存在不发送验证码. |






<a name="jmash-rbac-TokenResp"></a>

### TokenResp
登录成功响应.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| access_token | [string](#string) |  | 访问Token. |
| expires_in | [int32](#int32) |  | 过期时间(单位:毫秒). |
| refresh_token | [string](#string) |  | 刷新Token. |
| token_type | [string](#string) |  | Token类型. |






<a name="jmash-rbac-TreeList"></a>

### TreeList



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [TreeModel](#jmash-rbac-TreeModel) | repeated |  |






<a name="jmash-rbac-TreeModel"></a>

### TreeModel



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| value | [string](#string) |  | 资源ID |
| label | [string](#string) |  | 资源名称 |
| parentId | [string](#string) |  | 父级ID |
| children | [TreeModel](#jmash-rbac-TreeModel) | repeated | 子节点 |






<a name="jmash-rbac-ValidCodeLoginReq"></a>

### ValidCodeLoginReq
验证码登录


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| directory_id | [string](#string) |  | 目录ID |
| name | [string](#string) |  | Email/Phone |
| valid_code | [string](#string) |  | 验证码 |
| client_id | [string](#string) |  | 客户端ID |
| scope | [string](#string) |  | 范围 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_dept_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_dept_message.proto



<a name="jmash-rbac-DeptCreateReq"></a>

### DeptCreateReq
组织机构新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| dept_code | [string](#string) |  | 部门编码 |
| dept_name | [string](#string) |  | 部门名称 |
| dept_type | [string](#string) |  | 部门类型 |
| parent_id | [string](#string) |  | 父部门 |
| description_ | [string](#string) |  | 部门描述 |
| status_ | [bool](#bool) |  | 状态 |
| is_open | [bool](#bool) |  | 开放访问 |






<a name="jmash-rbac-DeptEnableKey"></a>

### DeptEnableKey
启用/禁用


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| dept_id | [string](#string) |  | 部门ID |
| enable | [bool](#bool) |  | 是否启用 |






<a name="jmash-rbac-DeptExportReq"></a>

### DeptExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [DeptReq](#jmash-rbac-DeptReq) |  | 筛选条件 |






<a name="jmash-rbac-DeptImportReq"></a>

### DeptImportReq
部门导入请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| file_names | [string](#string) |  | 文件名 |
| add_flag | [bool](#bool) |  | 是否新增标识 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |






<a name="jmash-rbac-DeptKey"></a>

### DeptKey
组织机构Key


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| dept_id | [string](#string) |  | 部门ID |






<a name="jmash-rbac-DeptKeyList"></a>

### DeptKeyList
组织机构List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| dept_id | [string](#string) | repeated | 部门ID |






<a name="jmash-rbac-DeptList"></a>

### DeptList
组织机构列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DeptModel](#jmash-rbac-DeptModel) | repeated | 当前页内容 |






<a name="jmash-rbac-DeptModel"></a>

### DeptModel
组织机构实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| dept_id | [string](#string) |  | 部门ID |
| dept_code | [string](#string) |  | 部门编码 |
| dept_name | [string](#string) |  | 部门名称 |
| dept_type | [string](#string) |  | 部门类型 |
| parent_id | [string](#string) |  | 父部门 |
| description_ | [string](#string) |  | 部门描述 |
| depth_ | [int32](#int32) |  | 深度 |
| order_by | [int32](#int32) |  | 排序 |
| status_ | [bool](#bool) |  | 状态 |
| version_ | [int32](#int32) |  | 乐观锁 |
| create_by | [string](#string) |  | 创建人 |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |
| update_by | [string](#string) |  | 更新人 |
| update_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 更新时间 |
| children | [DeptModel](#jmash-rbac-DeptModel) | repeated | 孩子资源列表 |
| is_open | [bool](#bool) |  | 开放访问 |






<a name="jmash-rbac-DeptModelTotal"></a>

### DeptModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-DeptMoveKey"></a>

### DeptMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| dept_id | [string](#string) |  | 部门ID |
| up | [bool](#bool) |  | 是否上移 |






<a name="jmash-rbac-DeptPage"></a>

### DeptPage
组织机构分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [DeptModel](#jmash-rbac-DeptModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [DeptModelTotal](#jmash-rbac-DeptModelTotal) |  | 本页小计 |
| total_dto | [DeptModelTotal](#jmash-rbac-DeptModelTotal) |  | 合计 |






<a name="jmash-rbac-DeptReq"></a>

### DeptReq
组织机构查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| create_by | [string](#string) |  | 创建人 |
| like_dept_name | [string](#string) |  | 部门名称 |
| has_status | [bool](#bool) |  | 是否包含状态字段 |
| status | [bool](#bool) |  | 部门状态 |
| exclude_id | [string](#string) |  | 需要隐藏的部门 |
| parent_id | [string](#string) |  | 上级部门 |
| dept_type | [string](#string) |  | 部门类型 |
| has_open | [bool](#bool) |  | 是否包含开放访问 |
| is_open | [bool](#bool) |  | 是否开放访问 |






<a name="jmash-rbac-DeptUpdateReq"></a>

### DeptUpdateReq
组织机构修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| dept_id | [string](#string) |  | 部门ID |
| dept_code | [string](#string) |  | 部门编码 |
| dept_name | [string](#string) |  | 部门名称 |
| dept_type | [string](#string) |  | 部门类型 |
| parent_id | [string](#string) |  | 父部门 |
| description_ | [string](#string) |  | 部门描述 |
| status_ | [bool](#bool) |  | 状态 |
| is_open | [bool](#bool) |  | 开放访问 |






<a name="jmash-rbac-EnableDeptReq"></a>

### EnableDeptReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| enable | [bool](#bool) |  | 是否启用 |
| dept_id | [string](#string) | repeated | 部门 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_log_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_log_message.proto



<a name="jmash-rbac-LogDelReq"></a>

### LogDelReq
清理N年前日志功能


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| number | [int32](#int32) |  | 清理N年前日志功能 |






<a name="jmash-rbac-LogExportReq"></a>

### LogExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [LogReq](#jmash-rbac-LogReq) |  | 筛选条件 |






<a name="jmash-rbac-LogKey"></a>

### LogKey
操作日志主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| log_id | [string](#string) |  |  |
| partition_ | [int32](#int32) |  |  |






<a name="jmash-rbac-LogKeyList"></a>

### LogKeyList
操作日志List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| log_key | [LogKey](#jmash-rbac-LogKey) | repeated |  |






<a name="jmash-rbac-LogList"></a>

### LogList
操作日志列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [LogModel](#jmash-rbac-LogModel) | repeated | 当前页内容 |






<a name="jmash-rbac-LogModel"></a>

### LogModel
操作日志实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| log_id | [string](#string) |  | 日志 |
| partition_ | [int32](#int32) |  | 表分区 |
| log_name | [string](#string) |  | 日志名称 |
| log_level | [string](#string) |  | 日志级别 |
| log_msg | [string](#string) |  | 日志信息 |
| env_props | [string](#string) |  | 环境参数 |
| log_content | [string](#string) |  | 日志内容 |
| user_ip | [string](#string) |  | 用户IP |
| proxy_ip | [string](#string) |  | 代理IP |
| create_by | [string](#string) |  | 创建人/用户ID |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间/操作时间 |






<a name="jmash-rbac-LogModelTotal"></a>

### LogModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-LogPage"></a>

### LogPage
操作日志分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [LogModel](#jmash-rbac-LogModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [LogModelTotal](#jmash-rbac-LogModelTotal) |  | 本页小计 |
| total_dto | [LogModelTotal](#jmash-rbac-LogModelTotal) |  | 合计 |






<a name="jmash-rbac-LogReq"></a>

### LogReq
操作日志查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| create_by | [string](#string) |  | 创建人 |
| start_create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间开始 |
| end_create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间结束 |
| partition | [int32](#int32) |  | 月份 |
| like_log_name | [string](#string) |  | 日志名称 |
| log_level | [string](#string) |  | 日志级别 |
| like_log_msg | [string](#string) |  | 日志信息 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_module_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_module_message.proto



<a name="jmash-rbac-ModuleCheck"></a>

### ModuleCheck
模块检查


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| module_code | [string](#string) |  | 编码 |






<a name="jmash-rbac-ModuleCreateReq"></a>

### ModuleCreateReq
系统模块新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| module_code | [string](#string) |  | 模块编码 |
| module_name | [string](#string) |  | 模块名称 |
| description_ | [string](#string) |  | 模块描述 |






<a name="jmash-rbac-ModuleExportReq"></a>

### ModuleExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [ModuleReq](#jmash-rbac-ModuleReq) |  | 筛选条件 |






<a name="jmash-rbac-ModuleKey"></a>

### ModuleKey
系统模块Key.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| module_id | [string](#string) |  | 系统模块ID. |






<a name="jmash-rbac-ModuleKeyList"></a>

### ModuleKeyList
系统模块List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| module_id | [string](#string) | repeated | 系统模块ID. |






<a name="jmash-rbac-ModuleList"></a>

### ModuleList
系统模块列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [ModuleModel](#jmash-rbac-ModuleModel) | repeated | 当前页内容 |






<a name="jmash-rbac-ModuleModel"></a>

### ModuleModel
系统模块实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| module_id | [string](#string) |  | 模块ID |
| module_code | [string](#string) |  | 模块编码 |
| module_name | [string](#string) |  | 模块名称 |
| order_by | [int32](#int32) |  | 模块排序 |
| description_ | [string](#string) |  | 模块描述 |






<a name="jmash-rbac-ModuleModelTotal"></a>

### ModuleModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-ModuleMoveKey"></a>

### ModuleMoveKey
上移/下移.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| module_id | [string](#string) |  | 系统模块ID. |
| up | [bool](#bool) |  | 是否上移 |






<a name="jmash-rbac-ModulePage"></a>

### ModulePage
系统模块分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [ModuleModel](#jmash-rbac-ModuleModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [ModuleModelTotal](#jmash-rbac-ModuleModelTotal) |  | 本页小计 |
| total_dto | [ModuleModelTotal](#jmash-rbac-ModuleModelTotal) |  | 合计 |






<a name="jmash-rbac-ModuleReq"></a>

### ModuleReq
系统模块查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| module_code | [string](#string) |  | 模块编码 |
| like_module_name | [string](#string) |  | 模块名称 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |






<a name="jmash-rbac-ModuleUpdateReq"></a>

### ModuleUpdateReq
系统模块修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| module_id | [string](#string) |  | 模块ID |
| module_code | [string](#string) |  | 模块编码 |
| module_name | [string](#string) |  | 模块名称 |
| description_ | [string](#string) |  | 模块描述 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_open_app_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_open_app_message.proto



<a name="jmash-rbac-OpenAppCreateReq"></a>

### OpenAppCreateReq
开发平台应用新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| app_id | [string](#string) |  | 应用ID |
| app_name | [string](#string) |  | 应用名称 |
| open_type | [OpensType](#jmash-rbac-OpensType) |  | 三方OpenID Type |
| app_secret | [string](#string) |  | 密钥(加密存储) |
| description_ | [string](#string) |  | 描述 |






<a name="jmash-rbac-OpenAppKey"></a>

### OpenAppKey
开发平台应用主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| open_app_id | [string](#string) |  |  |






<a name="jmash-rbac-OpenAppKeyList"></a>

### OpenAppKeyList
开发平台应用List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| open_app_id | [string](#string) | repeated |  |






<a name="jmash-rbac-OpenAppList"></a>

### OpenAppList
开发平台应用列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [OpenAppModel](#jmash-rbac-OpenAppModel) | repeated | 当前页内容 |






<a name="jmash-rbac-OpenAppModel"></a>

### OpenAppModel
开发平台应用实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| open_app_id | [string](#string) |  | 开放应用ID |
| app_id | [string](#string) |  | 应用ID |
| app_name | [string](#string) |  | 应用名称 |
| open_type | [string](#string) |  | 三方OpenID Type |
| app_secret | [string](#string) |  | 密钥(加密存储) |
| description_ | [string](#string) |  | 描述 |






<a name="jmash-rbac-OpenAppModelTotal"></a>

### OpenAppModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-OpenAppPage"></a>

### OpenAppPage
开发平台应用分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [OpenAppModel](#jmash-rbac-OpenAppModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [OpenAppModelTotal](#jmash-rbac-OpenAppModelTotal) |  | 本页小计 |
| total_dto | [OpenAppModelTotal](#jmash-rbac-OpenAppModelTotal) |  | 合计 |






<a name="jmash-rbac-OpenAppReq"></a>

### OpenAppReq
开发平台应用查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |






<a name="jmash-rbac-OpenAppUpdateReq"></a>

### OpenAppUpdateReq
开发平台应用修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  |  |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| open_app_id | [string](#string) |  | 开放应用ID |
| app_id | [string](#string) |  | 应用ID |
| app_name | [string](#string) |  | 应用名称 |
| open_type | [string](#string) |  | 三方OpenID Type |
| app_secret | [string](#string) |  | 密钥(加密存储) |
| description_ | [string](#string) |  | 描述 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_opens_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_opens_message.proto



<a name="jmash-rbac-OpensKey"></a>

### OpensKey
三方登录主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| app_id | [string](#string) |  |  |
| user_id | [string](#string) |  |  |
| open_id | [string](#string) |  |  |






<a name="jmash-rbac-OpensKeyList"></a>

### OpensKeyList
三方登录List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| opens_key | [OpensKey](#jmash-rbac-OpensKey) | repeated |  |






<a name="jmash-rbac-OpensList"></a>

### OpensList
三方登录列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [OpensModel](#jmash-rbac-OpensModel) | repeated | 当前页内容 |






<a name="jmash-rbac-OpensModel"></a>

### OpensModel
三方登录实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| open_id | [string](#string) |  | OpenID |
| user_id | [string](#string) |  | 账号ID |
| app_id | [string](#string) |  | 应用ID |
| union_id | [string](#string) |  | 唯一ID |
| nick_name | [string](#string) |  | 昵称 |
| open_type | [OpensType](#jmash-rbac-OpensType) |  | 三方OpenID Type |
| create_by | [string](#string) |  | 创建人 |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |
| update_by | [string](#string) |  | 更新人 |
| update_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 更新时间 |






<a name="jmash-rbac-OpensReq"></a>

### OpensReq
三方登录查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 账号ID |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |






<a name="jmash-rbac-UnionIdList"></a>

### UnionIdList
用户UnionId


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| union_ids | [string](#string) | repeated |  |






<a name="jmash-rbac-UserOpensReq"></a>

### UserOpensReq
用户开放平台信息查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| app_id | [string](#string) |  | APPID |
| open_type | [OpensType](#jmash-rbac-OpensType) |  | 三方OpenID Type |
| user_id | [string](#string) | repeated | 用户ID |





 


<a name="jmash-rbac-OpensType"></a>

### OpensType
三方OpenID Type枚举

| Name | Number | Description |
| ---- | ------ | ----------- |
| wechat | 0 | 微信 |
| ali_pay | 1 | 支付宝 |
| union_pay | 2 | 云闪付 |


 

 

 



<a name="jmash_rbac_protobuf_rbac_operation_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_operation_message.proto



<a name="jmash-rbac-OperationCheck"></a>

### OperationCheck
操作校验


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| operation_code | [string](#string) |  | 操作编码 |






<a name="jmash-rbac-OperationCreateReq"></a>

### OperationCreateReq
操作表新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| operation_code | [string](#string) |  | 操作编码 |
| operation_name | [string](#string) |  | 操作名称 |






<a name="jmash-rbac-OperationExportReq"></a>

### OperationExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [OperationReq](#jmash-rbac-OperationReq) |  | 筛选条件 |






<a name="jmash-rbac-OperationKey"></a>

### OperationKey
操作表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| operation_id | [string](#string) |  |  |






<a name="jmash-rbac-OperationKeyList"></a>

### OperationKeyList
操作表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| operation_id | [string](#string) | repeated |  |






<a name="jmash-rbac-OperationList"></a>

### OperationList
操作表列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [OperationModel](#jmash-rbac-OperationModel) | repeated | 当前页内容 |






<a name="jmash-rbac-OperationModel"></a>

### OperationModel
操作表实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| operation_id | [string](#string) |  | 操作ID |
| operation_code | [string](#string) |  | 操作编码 |
| operation_name | [string](#string) |  | 操作名称 |






<a name="jmash-rbac-OperationModelTotal"></a>

### OperationModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-OperationMoveKey"></a>

### OperationMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| operation_id | [string](#string) |  | 操作ID |
| up | [bool](#bool) |  | 是否上移 |






<a name="jmash-rbac-OperationPage"></a>

### OperationPage
操作表分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [OperationModel](#jmash-rbac-OperationModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [OperationModelTotal](#jmash-rbac-OperationModelTotal) |  | 本页小计 |
| total_dto | [OperationModelTotal](#jmash-rbac-OperationModelTotal) |  | 合计 |






<a name="jmash-rbac-OperationReq"></a>

### OperationReq
操作表查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| operation_code | [string](#string) |  | 操作编码 |
| like_operation_name | [string](#string) |  | 操作名称 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |






<a name="jmash-rbac-OperationUpdateReq"></a>

### OperationUpdateReq
操作表修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| operation_id | [string](#string) |  | 操作ID |
| operation_code | [string](#string) |  | 操作编码 |
| operation_name | [string](#string) |  | 操作名称 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_perm_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_perm_message.proto



<a name="jmash-rbac-PermCheck"></a>

### PermCheck
权限编码校验


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| perm_code | [string](#string) |  | 权限编码 |






<a name="jmash-rbac-PermCreateReq"></a>

### PermCreateReq
权限表新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| perm_code | [string](#string) |  | 权限编码 |
| perm_name | [string](#string) |  | 权限名称 |






<a name="jmash-rbac-PermKey"></a>

### PermKey
权限表Key


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| perm_id | [string](#string) |  |  |






<a name="jmash-rbac-PermKeyList"></a>

### PermKeyList
权限表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| perm_id | [string](#string) | repeated |  |






<a name="jmash-rbac-PermList"></a>

### PermList
权限表列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [PermModel](#jmash-rbac-PermModel) | repeated | 当前页内容 |






<a name="jmash-rbac-PermModel"></a>

### PermModel
权限表实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| perm_id | [string](#string) |  | 权限ID |
| perm_code | [string](#string) |  | 权限编码 |
| perm_name | [string](#string) |  | 权限名称 |






<a name="jmash-rbac-PermModelTotal"></a>

### PermModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-PermPage"></a>

### PermPage
权限表分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [PermModel](#jmash-rbac-PermModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [PermModelTotal](#jmash-rbac-PermModelTotal) |  | 本页小计 |
| total_dto | [PermModelTotal](#jmash-rbac-PermModelTotal) |  | 合计 |






<a name="jmash-rbac-PermReq"></a>

### PermReq
权限表查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| like_perm_code | [string](#string) |  | 权限编码 |
| like_perm_name | [string](#string) |  | 权限名称 |






<a name="jmash-rbac-PermUpdateReq"></a>

### PermUpdateReq
权限表修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| perm_id | [string](#string) |  | 权限ID |
| perm_code | [string](#string) |  | 权限编码 |
| perm_name | [string](#string) |  | 权限名称 |






<a name="jmash-rbac-ResourcePerm"></a>

### ResourcePerm
资源权限


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| resource_id | [string](#string) |  |  |
| resource_name | [string](#string) |  |  |
| parent_id | [string](#string) |  |  |
| perms | [PermModel](#jmash-rbac-PermModel) | repeated |  |
| children | [ResourcePerm](#jmash-rbac-ResourcePerm) | repeated |  |






<a name="jmash-rbac-ResourcePermList"></a>

### ResourcePermList
资源权限列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [ResourcePerm](#jmash-rbac-ResourcePerm) | repeated |  |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_resource_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_resource_message.proto



<a name="jmash-rbac-ResourceCreateReq"></a>

### ResourceCreateReq
资源表新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| module_id | [string](#string) |  | 模块ID |
| resource_code | [string](#string) |  | 资源编码 |
| resource_name | [string](#string) |  | 资源名称 |
| resource_type | [ResourceType](#jmash-rbac-ResourceType) |  | 资源类型 |
| parent_id | [string](#string) |  | 父资源ID |
| target_ | [string](#string) |  | 目标 |
| url_ | [string](#string) |  | URL |
| hidden_ | [bool](#bool) |  | 是否隐藏 |
| icon_ | [string](#string) |  | 图标 |
| oper_codes | [string](#string) | repeated | 操作ID列表 |






<a name="jmash-rbac-ResourceExportReq"></a>

### ResourceExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| resource_id | [string](#string) |  | 资源ID |
| req | [ResourceReq](#jmash-rbac-ResourceReq) |  | 筛选条件 |






<a name="jmash-rbac-ResourceImportReq"></a>

### ResourceImportReq
资源导入请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| file_names | [string](#string) |  | 文件名 |
| add_flag | [bool](#bool) |  | 是否新增标识 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |






<a name="jmash-rbac-ResourceKey"></a>

### ResourceKey
资源表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| resource_id | [string](#string) |  |  |






<a name="jmash-rbac-ResourceKeyList"></a>

### ResourceKeyList
资源表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| resource_id | [string](#string) | repeated |  |






<a name="jmash-rbac-ResourceList"></a>

### ResourceList
资源表列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [ResourceModel](#jmash-rbac-ResourceModel) | repeated | 当前页内容 |






<a name="jmash-rbac-ResourceModel"></a>

### ResourceModel
资源表实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| resource_id | [string](#string) |  | 资源ID |
| module_id | [string](#string) |  | 模块ID |
| resource_code | [string](#string) |  | 资源编码 |
| resource_name | [string](#string) |  | 资源名称 |
| resource_type | [string](#string) |  | 资源类型 |
| parent_id | [string](#string) |  | 父资源ID |
| depth_ | [int32](#int32) |  | 深度 |
| order_by | [int32](#int32) |  | 排序 |
| target_ | [string](#string) |  | 目标 |
| url_ | [string](#string) |  | URL |
| hidden_ | [bool](#bool) |  | 是否隐藏 |
| children | [ResourceModel](#jmash-rbac-ResourceModel) | repeated | 孩子资源列表 |
| oper_codes | [string](#string) | repeated | 操作编码列表 |
| module_name | [string](#string) |  | 模块名称 |
| icon_ | [string](#string) |  | 图标 |






<a name="jmash-rbac-ResourceModelTotal"></a>

### ResourceModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-ResourceMoveKey"></a>

### ResourceMoveKey



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| resource_id | [string](#string) |  | 资源ID |
| up | [bool](#bool) |  | 移动方式 |






<a name="jmash-rbac-ResourcePage"></a>

### ResourcePage
资源表分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [ResourceModel](#jmash-rbac-ResourceModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [ResourceModelTotal](#jmash-rbac-ResourceModelTotal) |  | 本页小计 |
| total_dto | [ResourceModelTotal](#jmash-rbac-ResourceModelTotal) |  | 合计 |






<a name="jmash-rbac-ResourceReq"></a>

### ResourceReq
资源表查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| resource_id | [string](#string) |  | 获取资源ID及其孩子 |
| module_id | [string](#string) |  | 模块ID |
| resource_code | [string](#string) |  | 资源编码 |
| like_resource_name | [string](#string) |  | 资源名称 |
| resource_type | [string](#string) |  | 资源类型 |
| parent_id | [string](#string) |  | 父资源ID |
| has_hidden | [bool](#bool) |  | 是否包含Hidden字段. |
| hidden_ | [bool](#bool) |  | 是否隐藏 |
| exclude_id | [string](#string) |  | 需要隐藏的资源 |






<a name="jmash-rbac-ResourceUpdateReq"></a>

### ResourceUpdateReq
资源表修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| resource_id | [string](#string) |  | 资源ID |
| module_id | [string](#string) |  | 模块ID |
| resource_code | [string](#string) |  | 资源编码 |
| resource_name | [string](#string) |  | 资源名称 |
| resource_type | [ResourceType](#jmash-rbac-ResourceType) |  | 资源类型 |
| parent_id | [string](#string) |  | 父资源ID |
| target_ | [string](#string) |  | 目标 |
| url_ | [string](#string) |  | URL |
| hidden_ | [bool](#bool) |  | 是否隐藏 |
| icon_ | [string](#string) |  | 图标 |
| oper_codes | [string](#string) | repeated | 操作ID列表 |






<a name="jmash-rbac-VerifyResourceReq"></a>

### VerifyResourceReq
校验资源信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| url | [string](#string) |  | 资源路径 |





 


<a name="jmash-rbac-ResourceType"></a>

### ResourceType
资源类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| catalog | 0 | 目录 |
| menu | 1 | 菜单 |
| other | 2 | 其他 |


 

 

 



<a name="jmash_rbac_protobuf_rbac_role_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_role_message.proto



<a name="jmash-rbac-DsdRoleListResp"></a>

### DsdRoleListResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| roleList | [RoleModel](#jmash-rbac-RoleModel) | repeated |  |






<a name="jmash-rbac-RoleCreateReq"></a>

### RoleCreateReq
角色/职务表新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| role_code | [string](#string) |  | 角色编码/职务编码 |
| role_name | [string](#string) |  | 角色名称/职务名称 |
| role_type | [RoleType](#jmash-rbac-RoleType) |  | 角色/职务 |
| parent_id | [string](#string) |  | 父角色 |
| description_ | [string](#string) |  | 描述 |
| perm_codes | [string](#string) | repeated | 角色权限 |
| role_duty | [RoleDuty](#jmash-rbac-RoleDuty) | repeated | 角色互斥 |






<a name="jmash-rbac-RoleDuty"></a>

### RoleDuty
角色互斥.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| role_id | [string](#string) |  | 角色ID/职务ID |
| duty_type | [DutyType](#jmash-rbac-DutyType) |  | 职责分离类型 |






<a name="jmash-rbac-RoleDutyModel"></a>

### RoleDutyModel
角色互斥.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| role_id | [string](#string) |  | 角色ID/职务ID |
| role_name | [string](#string) |  | 角色/职务名称 |
| duty_type | [DutyType](#jmash-rbac-DutyType) |  | 职责分离类型 |






<a name="jmash-rbac-RoleKey"></a>

### RoleKey
角色/职务Key


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_id | [string](#string) |  |  |






<a name="jmash-rbac-RoleKeyList"></a>

### RoleKeyList
角色/职务表List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_id | [string](#string) | repeated |  |






<a name="jmash-rbac-RoleList"></a>

### RoleList
角色/职务表列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [RoleModel](#jmash-rbac-RoleModel) | repeated | 当前页内容 |






<a name="jmash-rbac-RoleModel"></a>

### RoleModel
角色/职务表实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| role_id | [string](#string) |  | 角色ID/职务ID |
| role_code | [string](#string) |  | 角色编码/职务编码 |
| role_name | [string](#string) |  | 角色名称/职务名称 |
| role_type | [RoleType](#jmash-rbac-RoleType) |  | 角色/职务 |
| parent_id | [string](#string) |  | 父角色 |
| depth_ | [int32](#int32) |  | 深度 |
| order_by | [int32](#int32) |  | 排序 |
| description_ | [string](#string) |  | 描述 |
| create_by | [string](#string) |  | 创建人 |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |
| children | [RoleModel](#jmash-rbac-RoleModel) | repeated | 子角色. |
| perm_codes | [string](#string) | repeated | 角色权限 |
| role_duty_model | [RoleDutyModel](#jmash-rbac-RoleDutyModel) | repeated | 角色互斥 |






<a name="jmash-rbac-RoleModelTotal"></a>

### RoleModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-RoleMoveKey"></a>

### RoleMoveKey
上移/下移


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_id | [string](#string) |  | 角色ID/职务ID |
| up | [bool](#bool) |  | 是否上移 |






<a name="jmash-rbac-RolePage"></a>

### RolePage
角色/职务表分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [RoleModel](#jmash-rbac-RoleModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [RoleModelTotal](#jmash-rbac-RoleModelTotal) |  | 本页小计 |
| total_dto | [RoleModelTotal](#jmash-rbac-RoleModelTotal) |  | 合计 |






<a name="jmash-rbac-RolePermReq"></a>

### RolePermReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_id | [string](#string) |  | 角色ID/职务ID |
| perm_codes | [string](#string) | repeated | 权限编码 |






<a name="jmash-rbac-RoleReq"></a>

### RoleReq
角色/职务表查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| create_by | [string](#string) |  | 创建人 |
| role_code | [string](#string) |  | 角色编码/职务编码 |
| like_role_name | [string](#string) |  | 角色名称/职务名称 |
| has_role_type | [bool](#bool) |  | 是否包含角色类型 |
| role_type | [RoleType](#jmash-rbac-RoleType) |  | 角色/职务 |
| exclude_id | [string](#string) |  | 需要隐藏的角色 |






<a name="jmash-rbac-RoleUpdateReq"></a>

### RoleUpdateReq
角色/职务表修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| role_id | [string](#string) |  | 角色ID/职务ID |
| role_code | [string](#string) |  | 角色编码/职务编码 |
| role_name | [string](#string) |  | 角色名称/职务名称 |
| role_type | [RoleType](#jmash-rbac-RoleType) |  | 角色/职务 |
| parent_id | [string](#string) |  | 父角色 |
| description_ | [string](#string) |  | 描述 |
| perm_codes | [string](#string) | repeated | 角色权限 |
| role_duty | [RoleDuty](#jmash-rbac-RoleDuty) | repeated | 角色互斥 |






<a name="jmash-rbac-VerifyRoleReq"></a>

### VerifyRoleReq
校验角色信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| role_code | [string](#string) |  | 角色编码 |
| add_flag | [bool](#bool) |  | 新增/更新 |
| role_id | [string](#string) |  | 当前角色id |





 


<a name="jmash-rbac-DutyType"></a>

### DutyType
职责分离类型.

| Name | Number | Description |
| ---- | ------ | ----------- |
| SSD | 0 | 静态职责分离 |
| DSD | 1 | 动态职责分离 |



<a name="jmash-rbac-RoleType"></a>

### RoleType
角色类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| role | 0 | 角色 |
| job | 1 | 职务 |


 

 

 



<a name="jmash_rbac_protobuf_rbac_user_log_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_user_log_message.proto



<a name="jmash-rbac-UserLogDelReq"></a>

### UserLogDelReq
清理N年前安全日志功能


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| number | [int32](#int32) |  | 清理N年前安全日志功能 |






<a name="jmash-rbac-UserLogExportReq"></a>

### UserLogExportReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [UserLogReq](#jmash-rbac-UserLogReq) |  | 筛选条件 |






<a name="jmash-rbac-UserLogKey"></a>

### UserLogKey
安全日志主键


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| partition_ | [int32](#int32) |  |  |
| log_id | [string](#string) |  |  |






<a name="jmash-rbac-UserLogKeyList"></a>

### UserLogKeyList
安全日志List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_log_key | [UserLogKey](#jmash-rbac-UserLogKey) | repeated |  |






<a name="jmash-rbac-UserLogList"></a>

### UserLogList
安全日志列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserLogModel](#jmash-rbac-UserLogModel) | repeated | 当前页内容 |






<a name="jmash-rbac-UserLogModel"></a>

### UserLogModel
安全日志实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| log_id | [string](#string) |  | 日志 |
| partition_ | [int32](#int32) |  | 表分区 |
| log_name | [string](#string) |  | 操作类型 |
| log_msg | [string](#string) |  | 操作内容/日志信息 |
| env_props | [string](#string) |  | 环境参数 |
| device_id | [string](#string) |  | 设备标识ID |
| user_ip | [string](#string) |  | 用户IP |
| proxy_ip | [string](#string) |  | 代理IP |
| create_by | [string](#string) |  | 创建人/用户ID |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间/操作时间 |






<a name="jmash-rbac-UserLogModelTotal"></a>

### UserLogModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-UserLogPage"></a>

### UserLogPage
安全日志分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserLogModel](#jmash-rbac-UserLogModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [UserLogModelTotal](#jmash-rbac-UserLogModelTotal) |  | 本页小计 |
| total_dto | [UserLogModelTotal](#jmash-rbac-UserLogModelTotal) |  | 合计 |






<a name="jmash-rbac-UserLogReq"></a>

### UserLogReq
安全日志查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| create_by | [string](#string) |  | 创建人 |
| start_create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间开始 |
| end_create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间结束 |
| partition | [int32](#int32) |  | 月份 |
| like_log_name | [string](#string) |  | 日志名称 |





 

 

 

 



<a name="jmash_rbac_protobuf_rbac_user_message-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## jmash/rbac/protobuf/rbac_user_message.proto



<a name="jmash-rbac-ApprovedUserReq"></a>

### ApprovedUserReq
审核/取消审核请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| approved | [bool](#bool) |  | 审核/取消审核 |
| user_id | [string](#string) | repeated | 用户Id |






<a name="jmash-rbac-DirectoryListResp"></a>

### DirectoryListResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| directory_id | [string](#string) | repeated |  |






<a name="jmash-rbac-EnableUserReq"></a>

### EnableUserReq
启禁用用户请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| enabled | [bool](#bool) |  | 启用/禁用 |
| user_id | [string](#string) | repeated | 用户Id |






<a name="jmash-rbac-JobInfo"></a>

### JobInfo



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |
| job_name | [string](#string) |  |  |
| dept_id | [string](#string) |  |  |
| dept_name | [string](#string) |  |  |






<a name="jmash-rbac-LockUserReq"></a>

### LockUserReq
锁/解锁用户请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| lock | [bool](#bool) |  | 锁/解锁 |
| user_id | [string](#string) | repeated | 用户Id |






<a name="jmash-rbac-OrganUserCreateReq"></a>

### OrganUserCreateReq
组织用户创建.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| organ_tenant | [string](#string) |  | 组织租户 |
| directory_id | [string](#string) |  | 目录ID |
| real_name | [string](#string) |  | 姓名 |
| creator | [bool](#bool) |  | 是否初始创建用户. |
| dept_id | [string](#string) | repeated | 所属部门 |






<a name="jmash-rbac-ScanCodeCreateUserReq"></a>

### ScanCodeCreateUserReq
扫码创建用户.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID. |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行. |
| authorizer_appid | [string](#string) |  | 授权公众号AppID. |
| ticket | [string](#string) |  | 二维码凭证. |
| directory_id | [string](#string) |  | 目录ID. |
| mobile_phone | [string](#string) |  | 手机号. |
| valid_code | [string](#string) |  | 验证码. |






<a name="jmash-rbac-UpdateUserReq"></a>

### UpdateUserReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户 |
| nick_name | [string](#string) |  | 昵称 |
| real_name | [string](#string) |  | 姓名 |
| gender_ | [jmash.protobuf.Gender](#jmash-protobuf-Gender) |  | 性别 |
| avatar | [string](#string) |  | 头像 |
| birth_date | [string](#string) |  | 出生日期 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码 |
| request_id | [string](#string) |  | 请求ID |






<a name="jmash-rbac-UserCreateReq"></a>

### UserCreateReq
用户新增实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| directory_id | [string](#string) |  | 目录ID |
| login_name | [string](#string) |  | 用户名/登录名 |
| mobile_phone | [string](#string) |  | 手机号 |
| email_ | [string](#string) |  | 电子邮件 |
| real_name | [string](#string) |  | 姓名 |
| nick_name | [string](#string) |  | 昵称 |
| avatar_ | [string](#string) |  | 头像 |
| birth_date | [string](#string) |  | 出生日期 |
| gender_ | [jmash.protobuf.Gender](#jmash-protobuf-Gender) |  | 性别 |
| approved_ | [bool](#bool) |  | 用户通过审核 |
| status_ | [UserStatus](#jmash-rbac-UserStatus) |  | 用户状态 |
| pwd | [string](#string) |  | 密钥 |
| repeat_pwd | [string](#string) |  | 重复密钥 |
| role_ids | [string](#string) | repeated | 用户角色 |
| user_jobs | [UserJobs](#jmash-rbac-UserJobs) | repeated | 所属部门岗位 |






<a name="jmash-rbac-UserDeptJobInfoRes"></a>

### UserDeptJobInfoRes
用户部门岗位信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | 用户ID |
| nick_name | [string](#string) |  | 用户昵称 |
| job_info | [JobInfo](#jmash-rbac-JobInfo) | repeated | 部门及岗位信息 |
| real_name | [string](#string) |  | 用户真实姓名 |
| avatar_ | [string](#string) |  | 头像 |
| storage_ | [string](#string) |  | 个人信息存储区 |






<a name="jmash-rbac-UserEnableKey"></a>

### UserEnableKey
启禁用用户请求.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| enabled | [bool](#bool) |  | 启用/禁用 |
| user_id | [string](#string) |  | 用户Id |






<a name="jmash-rbac-UserExportReq"></a>

### UserExportReq
用户导出请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| title | [string](#string) |  | 标题 |
| table_heads | [jmash.protobuf.TableHead](#jmash-protobuf-TableHead) | repeated | 字段列表 |
| file_name | [string](#string) |  | 显示文件名 |
| req | [UserReq](#jmash-rbac-UserReq) |  | 筛选条件 |






<a name="jmash-rbac-UserImportReq"></a>

### UserImportReq
用户导入请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| file_names | [string](#string) |  | 文件名 |
| add_flag | [bool](#bool) |  | 是否新增标识 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |






<a name="jmash-rbac-UserInfo"></a>

### UserInfo
用户信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  | 用户ID |
| value | [string](#string) |  | 用户真实姓名 |
| mobile_phone_ins | [string](#string) |  | 手机号码 |
| dept_id | [string](#string) |  | 部门ID |
| dept_name | [string](#string) |  | 部门 |
| job_id | [string](#string) |  | 岗位ID |
| job_name | [string](#string) |  | 岗位 |






<a name="jmash-rbac-UserInfoList"></a>

### UserInfoList
用户信息列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserInfo](#jmash-rbac-UserInfo) | repeated |  |






<a name="jmash-rbac-UserInfoPage"></a>

### UserInfoPage
用户分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserInfo](#jmash-rbac-UserInfo) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [UserModelTotal](#jmash-rbac-UserModelTotal) |  | 本页小计 |
| total_dto | [UserModelTotal](#jmash-rbac-UserModelTotal) |  | 合计 |






<a name="jmash-rbac-UserJobs"></a>

### UserJobs



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| dept_id | [string](#string) |  | 所属部门 |
| job_id | [string](#string) |  | 所属岗位 |






<a name="jmash-rbac-UserKey"></a>

### UserKey
用户Key.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 用户ID. |






<a name="jmash-rbac-UserKeyList"></a>

### UserKeyList
用户List


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) | repeated |  |






<a name="jmash-rbac-UserList"></a>

### UserList
用户列表


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserModel](#jmash-rbac-UserModel) | repeated | 当前页内容 |






<a name="jmash-rbac-UserModel"></a>

### UserModel
用户实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | 账号 |
| unified_id | [string](#string) |  | 统一身份Id |
| directory_id | [string](#string) |  | 目录ID |
| login_name | [string](#string) |  | 用户名/登录名 |
| mobile_phone | [string](#string) |  | 手机号 |
| mobile_phone_ins | [string](#string) |  | 手机号脱敏 |
| email_ | [string](#string) |  | 电子邮件 |
| real_name | [string](#string) |  | 姓名 |
| nick_name | [string](#string) |  | 昵称 |
| avatar_ | [string](#string) |  | 头像 |
| birth_date | [string](#string) |  | 出生日期 |
| gender_ | [jmash.protobuf.Gender](#jmash-protobuf-Gender) |  | 性别 |
| phone_approved | [bool](#bool) |  | 手机通过审核 |
| email_approved | [bool](#bool) |  | 邮箱通过审核 |
| approved_ | [bool](#bool) |  | 用户通过审核 |
| status_ | [UserStatus](#jmash-rbac-UserStatus) |  | 用户状态 |
| storage_ | [string](#string) |  | 个人信息存储区 |
| last_lockout_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 上次被锁/禁用时间 |
| failed_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 登录失败时间 |
| failed_count | [int32](#int32) |  | 登录失败次数 |
| last_login_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 上次登录时间 |
| version_ | [int32](#int32) |  | 乐观锁 |
| create_by | [string](#string) |  | 创建人 |
| create_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 创建时间 |
| update_by | [string](#string) |  | 更新人 |
| update_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 更新时间 |
| delete_by | [string](#string) |  | 删除人 |
| deleted_ | [bool](#bool) |  | 删除状态 |
| delete_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | 删除时间 |
| role_ids | [string](#string) | repeated | 用户角色 |
| user_jobs | [UserJobs](#jmash-rbac-UserJobs) | repeated | 用户职务岗位 |
| run_as | [bool](#bool) |  | 是否运行. |
| primary_user_id | [string](#string) |  | 主用户身份. |
| email_ins | [string](#string) |  | 电子邮件脱敏 |
| real_name_ins | [string](#string) |  | 姓名脱敏 |






<a name="jmash-rbac-UserModelTotal"></a>

### UserModelTotal
合计


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_size | [int32](#int32) |  | 总记录数 |






<a name="jmash-rbac-UserNameReq"></a>

### UserNameReq
用户名请求信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_name | [string](#string) |  | 用户名、手机号、邮箱 |
| directory_id | [string](#string) |  | 目录ID |






<a name="jmash-rbac-UserPage"></a>

### UserPage
用户分页


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| results | [UserModel](#jmash-rbac-UserModel) | repeated | 当前页内容 |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| total_size | [int32](#int32) |  | 总记录数 |
| sub_total_dto | [UserModelTotal](#jmash-rbac-UserModelTotal) |  | 本页小计 |
| total_dto | [UserModelTotal](#jmash-rbac-UserModelTotal) |  | 合计 |






<a name="jmash-rbac-UserReq"></a>

### UserReq
用户查询


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| cur_page | [int32](#int32) |  | 当前页码 |
| page_size | [int32](#int32) |  | 页尺寸 |
| order_name | [string](#string) |  | 排序名称 |
| order_asc | [bool](#bool) |  | 是否升序排序 |
| show_deleted | [bool](#bool) |  | 是否显示已删除数据 |
| create_by | [string](#string) |  | 创建人 |
| dept_id | [string](#string) |  | 部门ID |
| login_name | [string](#string) |  | 用户名 |
| has_user_status | [bool](#bool) |  | 是否包含用户状态 |
| user_status | [UserStatus](#jmash-rbac-UserStatus) |  | 状态 |
| role_code | [string](#string) |  | 角色编码 |
| directory_id | [string](#string) |  | 目录ID |
| user_id | [string](#string) | repeated | 用户ID |






<a name="jmash-rbac-UserResetPwdReq"></a>

### UserResetPwdReq
重置密码


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 用户Id |
| pwd | [string](#string) |  | 密钥 |
| repeat_pwd | [string](#string) |  | 重复密钥 |






<a name="jmash-rbac-UserRoleReq"></a>

### UserRoleReq
用户角色


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 用户Id |
| role_codes | [string](#string) | repeated | 角色编码 |






<a name="jmash-rbac-UserUpdateReq"></a>

### UserUpdateReq
用户修改实体


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| request_id | [string](#string) |  | 用于检测重复请求的唯一字符串ID |
| validate_only | [bool](#bool) |  | 如果为true，则表示给定的请求仅需要被检验，而不是被执行 |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  | 更新字段掩码. |
| user_id | [string](#string) |  | 账号 |
| directory_id | [string](#string) |  | 目录ID |
| login_name | [string](#string) |  | 用户名/登录名 |
| mobile_phone | [string](#string) |  | 手机号 |
| email_ | [string](#string) |  | 电子邮件 |
| real_name | [string](#string) |  | 姓名 |
| nick_name | [string](#string) |  | 昵称 |
| avatar_ | [string](#string) |  | 头像 |
| birth_date | [string](#string) |  | 出生日期 |
| gender_ | [jmash.protobuf.Gender](#jmash-protobuf-Gender) |  | 性别 |
| phone_approved | [bool](#bool) |  | 手机通过审核 |
| email_approved | [bool](#bool) |  | 邮箱通过审核 |
| approved_ | [bool](#bool) |  | 用户通过审核 |
| status_ | [UserStatus](#jmash-rbac-UserStatus) |  | 用户状态 |
| role_ids | [string](#string) | repeated | 用户角色 |
| user_jobs | [UserJobs](#jmash-rbac-UserJobs) | repeated | 所属部门岗位 |






<a name="jmash-rbac-VerifyUserReq"></a>

### VerifyUserReq
校验用户信息(用户名、手机号、邮箱唯一性)


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tenant | [string](#string) |  | 租户. |
| user_id | [string](#string) |  | 用户Id |
| user_name | [string](#string) |  | 用户名、手机号、邮箱 |
| directory_id | [string](#string) |  | 目录ID |





 


<a name="jmash-rbac-UserStatus"></a>

### UserStatus
用户状态

| Name | Number | Description |
| ---- | ------ | ----------- |
| enabled | 0 | 启用 |
| disabled | 1 | 禁用 |
| locked | 2 | 锁定 |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

