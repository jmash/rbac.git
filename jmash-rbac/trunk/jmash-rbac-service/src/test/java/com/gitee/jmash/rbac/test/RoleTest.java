
package com.gitee.jmash.rbac.test;

import com.google.protobuf.FieldMask;
import java.util.UUID;
import jmash.protobuf.TenantReq;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.DsdRoleListResp;
import jmash.rbac.protobuf.RoleCreateReq;
import jmash.rbac.protobuf.RoleKey;
import jmash.rbac.protobuf.RoleList;
import jmash.rbac.protobuf.RoleModel;
import jmash.rbac.protobuf.RoleReq;
import jmash.rbac.protobuf.RoleType;
import jmash.rbac.protobuf.RoleUpdateReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class RoleTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final RoleReq.Builder request = RoleReq.newBuilder();
    request.setTenant(TENANT);
    request.setRoleType(RoleType.job);
    final RoleList modelList = rbacStub.findRoleList(request.build());
    System.out.println(modelList);
  }



  @Test
  @Order(4)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    RoleCreateReq.Builder req = RoleCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setRoleName("测试1");
    req.setRoleCode("test-1");
    req.setRoleType(RoleType.role);
    // ...
    RoleModel entity = rbacStub.createRole(req.build());

    // findById
    RoleKey pk = RoleKey.newBuilder().setTenant(TENANT).setRoleId(entity.getRoleId()).build();
    entity = rbacStub.findRoleById(pk);

    // Update
    RoleUpdateReq.Builder updateReq = RoleUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT).setRoleId(entity.getRoleId());
    updateReq.setRoleName("测试2");
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("roleName");
    updateReq.setUpdateMask(fieldMask.build());
    entity = rbacStub.updateRole(updateReq.build());

    // Delete
    pk = RoleKey.newBuilder().setTenant(TENANT).setRoleId(entity.getRoleId()).build();
    entity = rbacStub.deleteRole(pk);
  }

  @Test
  @Order(3)
  public void findTreeList() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final RoleReq.Builder request = RoleReq.newBuilder();
    request.setTenant(TENANT);
    request.setRoleType(RoleType.job);
    final RoleList modelList = rbacStub.findRoleList(request.build());
    System.out.println(modelList);
  }

  @Test
  @Order(4)
  public void selectDsdRoles() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final TenantReq.Builder request = TenantReq.newBuilder();
    request.setTenant(TENANT);
    final DsdRoleListResp resp = rbacStub.selectDsdRoles(request.build());
    System.out.println(resp.getRoleListCount());
  }

}
