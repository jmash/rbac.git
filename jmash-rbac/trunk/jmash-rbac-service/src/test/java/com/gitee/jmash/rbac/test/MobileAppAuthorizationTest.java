package com.gitee.jmash.rbac.test;

import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.MiniAppLoginReq;
import jmash.rbac.protobuf.MiniAppLoginResp;
import jmash.rbac.protobuf.MiniAppPhoneNumberReq;
import jmash.rbac.protobuf.MobileAppLoginBindPhoneReq;
import jmash.rbac.protobuf.MobileAppLoginReq;
import jmash.rbac.protobuf.MobileAppLoginResp;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@Disabled
@TestMethodOrder(OrderAnnotation.class)
public class MobileAppAuthorizationTest extends RbacTest{

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  /** wx.login获取到的code. */
  public static final String LOGIN_CODE = "0913Zp000bHeKT1hB7200QO9xE43Zp0O";
  /** appId. */
  public static final String APP_ID = "wx7c0ba247f3c9549b";

  @Test
  public void mobileLogin() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    MobileAppLoginReq.Builder loginReq = MobileAppLoginReq.newBuilder();
    loginReq.setTenant(TENANT);
    loginReq.setLoginCode(LOGIN_CODE);
    loginReq.setAppId(APP_ID);
    MobileAppLoginResp resp = rbacStub.mobileAppLogin(loginReq.build());
    System.out.println(resp);
  }

  @Test
  public void mobileLoginBindPhone() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    MobileAppLoginBindPhoneReq.Builder bindPhoneReq = MobileAppLoginBindPhoneReq.newBuilder();
    bindPhoneReq.setTenant(TENANT);
    bindPhoneReq.setAppId(APP_ID);
    bindPhoneReq.setCacheKey("3b2cd193-7a92-47fb-aa89-14787d945b88");
    bindPhoneReq.setPhoneNumber("13834234716");
    bindPhoneReq.setVerifyCode("1234");
    bindPhoneReq.setNickName("13834234716");
    MobileAppLoginResp resp = rbacStub.mobileAppLoginBindPhone(bindPhoneReq.build());
    System.out.println(resp);
  }

}
