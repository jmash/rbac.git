
package com.gitee.jmash.rbac.test;

import com.google.protobuf.BoolValue;
import com.google.protobuf.FieldMask;
import java.util.UUID;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.OperationCheck;
import jmash.rbac.protobuf.OperationCreateReq;
import jmash.rbac.protobuf.OperationKey;
import jmash.rbac.protobuf.OperationModel;
import jmash.rbac.protobuf.OperationPage;
import jmash.rbac.protobuf.OperationReq;
import jmash.rbac.protobuf.OperationUpdateReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class OperationTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final OperationReq.Builder request = OperationReq.newBuilder();
    request.setCurPage(1);
    request.setPageSize(10);
    request.setTenant(TENANT);
    request.setLikeOperationName("新");
    final OperationPage modelPage = rbacStub.findOperationPage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void checkTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    OperationCheck.Builder req = OperationCheck.newBuilder();
    req.setTenant(TENANT);
    req.setOperationCode("add1");

    BoolValue result = rbacStub.checkOperationCode(req.build());
    System.out.println(result);
  }

  @Test
  @Order(3)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    OperationCreateReq.Builder req = OperationCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setOperationCode("test-oper");
    req.setOperationName("测试操作");
    OperationModel entity = rbacStub.createOperation(req.build());

    // findById
    OperationKey pk =
        OperationKey.newBuilder().setTenant(TENANT).setOperationId(entity.getOperationId()).build();
    entity = rbacStub.findOperationById(pk);

    // Update
    OperationUpdateReq.Builder updateReq = OperationUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT);
    updateReq.setOperationId(entity.getOperationId());
    updateReq.setOperationName("测试操作2");
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("operation_name");
    updateReq.setUpdateMask(fieldMask.build());
    entity = rbacStub.updateOperation(updateReq.build());

    // Delete
    pk = OperationKey.newBuilder().setTenant(TENANT).setOperationId(entity.getOperationId())
        .build();
    entity = rbacStub.deleteOperation(pk);
  }

}
