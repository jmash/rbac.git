
package com.gitee.jmash.rbac.test;

import com.google.protobuf.BoolValue;
import com.google.protobuf.FieldMask;
import java.util.UUID;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.PermCheck;
import jmash.rbac.protobuf.PermCreateReq;
import jmash.rbac.protobuf.PermKey;
import jmash.rbac.protobuf.PermModel;
import jmash.rbac.protobuf.PermPage;
import jmash.rbac.protobuf.PermReq;
import jmash.rbac.protobuf.PermUpdateReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class PermTest extends RbacTest {


  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final PermReq.Builder request = PermReq.newBuilder();
    request.setTenant(TENANT);
    final PermPage modelPage = rbacStub.findPermPage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void checkTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    PermCheck.Builder req = PermCheck.newBuilder();
    req.setTenant(TENANT);
    req.setPermCode("rbac:user:add1");

    BoolValue result = rbacStub.checkPermCode(req.build());
    System.out.println(result);
  }

  @Test
  @Order(3)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    PermCreateReq.Builder req = PermCreateReq.newBuilder();
    req.setTenant(TENANT);
    req.setRequestId(UUID.randomUUID().toString());
    req.setPermCode("test:test:add");
    req.setPermName("测试");
    PermModel entity = rbacStub.createPerm(req.build());

    // find
    PermKey pk = PermKey.newBuilder().setPermId(entity.getPermId()).setTenant(TENANT).build();
    entity = rbacStub.findPermById(pk);

    // Update
    PermUpdateReq.Builder updateReq = PermUpdateReq.newBuilder();
    updateReq.setPermId(entity.getPermId());
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT);
    updateReq.setPermName("abc");
    updateReq.setValidateOnly(false);
    // ...
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("permName");
    updateReq.setUpdateMask(fieldMask.build());
    entity = rbacStub.updatePerm(updateReq.build());
    // findById
    pk = PermKey.newBuilder().setPermId(entity.getPermId()).setTenant(TENANT).build();
    entity = rbacStub.findPermById(pk);
    // Delete
    entity = rbacStub.deletePerm(pk);
  }



}
