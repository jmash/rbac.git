package com.gitee.jmash.rbac.test;

import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.OpensType;
import jmash.rbac.protobuf.UnionIdList;
import jmash.rbac.protobuf.UserOpensReq;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class OpensTest extends RbacTest{
  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  public void findOpensUnionId(){
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    UserOpensReq.Builder req=UserOpensReq.newBuilder().setTenant(TENANT).setAppId("wxf201f46341e0adf6").setOpenType(OpensType.wechat);
    req.addUserId("21864DCCCA23465C9630DBBAC7B892BF").addUserId("6188B58E9A9D4AD5B211CAEBB5BDF6E4");
    UnionIdList list=rbacStub.findOpensUnionId(req.build());
    System.out.println(list);
  }
}
