
package com.gitee.jmash.rbac.test;

import com.crenjoy.proto.utils.TypeUtil;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.LogDelReq;
import jmash.rbac.protobuf.LogExportReq;
import jmash.rbac.protobuf.LogPage;
import jmash.rbac.protobuf.LogReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class LogTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final LogReq.Builder request = LogReq.newBuilder();
    request.setTenant(TENANT);
    final LogPage modelPage = rbacStub.findLogPage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void Test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    LogDelReq.Builder request = LogDelReq.newBuilder().setTenant(TENANT).setNumber(1);
    rbacStub.deleteLog(request.build());
  }

  @Test
  @Order(3)
  public void exportLog() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    LogExportReq.Builder request = LogExportReq.newBuilder();
    request.setTenant(TENANT);
    request.setReq(LogReq.newBuilder()
        .setStartCreateTime(TypeUtil.INSTANCE.toProtoTimestamp("2024-02-23 12:00:00"))
        .setEndCreateTime(TypeUtil.INSTANCE.toProtoTimestamp("2024-02-23 18:00:00")).build());
    rbacStub.exportLog(request.build());
  }


}
