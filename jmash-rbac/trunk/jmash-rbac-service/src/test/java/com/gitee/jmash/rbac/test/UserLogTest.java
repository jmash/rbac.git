
package com.gitee.jmash.rbac.test;

import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.UserLogExportReq;
import jmash.rbac.protobuf.UserLogPage;
import jmash.rbac.protobuf.UserLogReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class UserLogTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;



  //@Test
  @Order(2)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub= rbacStub.withCallCredentials(getOAuthCallCredentials());
    final UserLogReq.Builder request = UserLogReq.newBuilder();
    request.setTenant(TENANT);
    final UserLogPage modelPage = rbacStub.findUserLogPage(request.build());
    System.out.println(modelPage);
  }

  @Test
  public void exportUserLog(){
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getOAuthCallCredentials());
    UserLogReq request = UserLogReq.newBuilder()
            .setTenant(TENANT)
            .setLikeLogName("Login")
            .build();
    UserLogExportReq req = UserLogExportReq.newBuilder()
            .setTenant(TENANT)
            .setReq(request)
            .build();
    rbacStub.exportUserLog(req);
  }



}
