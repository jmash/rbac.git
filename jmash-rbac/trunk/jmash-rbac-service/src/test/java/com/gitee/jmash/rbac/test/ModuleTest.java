
package com.gitee.jmash.rbac.test;

import com.google.protobuf.FieldMask;
import java.util.UUID;
import jmash.rbac.RbacGrpc;
import jmash.rbac.protobuf.ModuleCheck;
import jmash.rbac.protobuf.ModuleCreateReq;
import jmash.rbac.protobuf.ModuleKey;
import jmash.rbac.protobuf.ModuleModel;
import jmash.rbac.protobuf.ModuleMoveKey;
import jmash.rbac.protobuf.ModulePage;
import jmash.rbac.protobuf.ModuleReq;
import jmash.rbac.protobuf.ModuleUpdateReq;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 测试.
 *
 * @author CGD
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class ModuleTest extends RbacTest {

  private RbacGrpc.RbacBlockingStub rbacStub = null;

  @Test
  @Order(1)
  public void findPage() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    final ModuleReq.Builder request = ModuleReq.newBuilder();
    request.setTenant(TENANT);
    request.setOrderName("orderBy");
    request.setOrderAsc(true);
    final ModulePage modelPage = rbacStub.findModulePage(request.build());
    System.out.println(modelPage);
  }

  @Test
  @Order(2)
  public void checkTest() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());
    ModuleCheck.Builder req = ModuleCheck.newBuilder();
    req.setTenant(TENANT);
    req.setModuleCode("rbac");
    rbacStub.checkModuleCode(req.build());
  }

  @Test
  @Order(3)
  public void test() {
    rbacStub = RbacGrpc.newBlockingStub(channel);
    rbacStub = rbacStub.withCallCredentials(getAuthcCallCredentials());

    // 创建
    ModuleCreateReq.Builder req = ModuleCreateReq.newBuilder();
    req.setRequestId(UUID.randomUUID().toString());
    req.setTenant(TENANT);
    req.setModuleCode("test-code");
    req.setModuleName("测试模块");
    ModuleModel entity = rbacStub.createModule(req.build());

    ModuleMoveKey moveKey = ModuleMoveKey.newBuilder().setTenant(TENANT)
        .setModuleId(entity.getModuleId()).setUp(true).build();
    rbacStub.moveModule(moveKey);

    // findById
    ModuleKey pk =
        ModuleKey.newBuilder().setTenant(TENANT).setModuleId(entity.getModuleId()).build();
    entity = rbacStub.findModuleById(pk);

    // Update
    ModuleUpdateReq.Builder updateReq = ModuleUpdateReq.newBuilder();
    updateReq.setRequestId(UUID.randomUUID().toString());
    updateReq.setTenant(TENANT).setModuleId(entity.getModuleId());
    updateReq.setModuleName("测试模块2");
    FieldMask.Builder fieldMask = FieldMask.newBuilder();
    fieldMask.addPaths("moduleName");

    updateReq.setUpdateMask(fieldMask.build());

    entity = rbacStub.updateModule(updateReq.build());

    // Delete
    pk = ModuleKey.newBuilder().setTenant(TENANT).setModuleId(entity.getModuleId()).build();
    entity = rbacStub.deleteModule(pk);
  }

}
