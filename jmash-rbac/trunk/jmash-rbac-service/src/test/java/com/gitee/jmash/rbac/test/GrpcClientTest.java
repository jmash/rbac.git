package com.gitee.jmash.rbac.test;

import com.gitee.jmash.common.grpc.auth.OAuth2Credentials;
import com.gitee.jmash.rbac.client.RbacClient;
import com.gitee.jmash.rbac.client.token.SystemAccessToken;
import org.junit.jupiter.api.Test;

public class GrpcClientTest extends RbacTest {

  @Test
  void test() {
    OAuth2Credentials token = RbacClient.getSystemCredentials(TENANT);
    System.out.println(token);
    SystemAccessToken.clearAccessToken(TENANT);
  }

}
