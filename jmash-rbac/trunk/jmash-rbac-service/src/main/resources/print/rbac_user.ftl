<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .tbody {width: 100%; font-size: 14pt; font-family: 'SimSun'; line-height: 28pt; position: relative; margin: auto; background: #FFFFFF; }
        table,.box {border-collapse: collapse; table-layout: fixed; margin-top: -1px; width: 100%; border-color: #999999; background: #FFFFFF;}
        h3,h5 {padding: 0px; margin: 0px; color: #000;}
        h3 {font-size: 30px;}
        h5 {font-weight: normal;}
        table tr td {font-size: 12px; height: 30px; border-color: #999999; line-height: 15px; padding: 0 5px}
        /*table tr td {font-size: 12px; height: 32px; border-color: #999999; line-height: 16px; padding: 0 5px}*/
        table tr td input {height: 100%; padding: 0px; border: none;}
        table tbody tr td {border:0.5px solid #000000}
        .border td {border:0.5px solid #000000}
        .box tr td {height: 175px; border-color: #999999; padding: 0 5px; text-align: left;}
        .box tr td span {display: block;}
        .box tr td b {display: block; font-weight: normal; margin-top: 40px; text-indent: 80px;}
        .box tr td i {font-style: normal; height: 48px; display: block; line-height: 24px; margin-top: 45px;vertical-align:middle; }
        .box tr td strong {font-weight: normal; display: block; margin-top: 5px; text-align: right;}
        .cont{ height:118px;}
        .cont2{ height:45px;}
        .heiti
        {
            font-family: simsun-bold;
        }
    </style>
</head>
<body>
<div class="tbody">
    <table style="table-layout: auto;repeat-footer:yes;">
        <thead>
        <tr class="border">
            <td colspan="${tableHeads?size -1 }" align="center"><h3>${title!''}</h3></td>
        </tr>
        <tr class="border">
            <#list tableHeads as head>
                <#if head.prop = "NO">
					<td width="60" style="text-align:center">${head.label}</td>
				<#elseif head.prop = "userId">
				<#elseif head.prop = "directoryId">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "loginName">	
					<td width="100" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "mobilePhone">	
					<td width="120" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "email">	
					<td width="150" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "realName">	
					<td width="120" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "nickName">	
					<td width="120" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "birthDate">	
					<td width="130" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "gender">	
					<td width="60" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "approved">	
					<td width="100" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "status">	
					<td width="100" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "lastLockoutTime">	
					<td width="105" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "lastLoginTime">	
					<td width="135" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "deleteTime">	
					<td width="105" style="text-align:center">${head.label!''}</td>
				<#else>	
					<td width="100" style="text-align:center" >${head.label!''}</td>
				</#if>			
            </#list>
        </tr>
        </thead>
        <tbody>
        	<#list userList as entity>
            <tr>
                <#list tableHeads as head>
                    <#if head.prop = "NO">
						<td align="center">${ entity?counter }</td>
					<#elseif head.prop = "userId">	
					<#elseif head.prop = "directoryId">	
						<td align="left">${entity.directoryId!''}</td>
					<#elseif head.prop = "loginName">	
						<td align="left">${entity.loginName!''}</td>
					<#elseif head.prop = "mobilePhone">	
						<td align="left">${entity.mobilePhone!''}</td>
					<#elseif head.prop = "email">	
						<td align="left">${entity.email!''}</td>
					<#elseif head.prop = "realName">	
						<td align="left">${entity.realName!''}</td>
					<#elseif head.prop = "nickName">	
						<td align="left">${entity.nickName!''}</td>
					<#elseif head.prop = "birthDate">	
						<td align="center">
							<#if entity.birthDate??>
								${entity.birthDate?string["yyyy-MM-dd"]}
							</#if>
						</td>
					<#elseif head.prop = "gender">	
						<td align="center">${genderMap[entity.gender?string]!''}</td>
					<#elseif head.prop = "approved">	
						<td align="center">${approvedMap[entity.approved?string]!''}</td>
					<#elseif head.prop = "status">	
						<td align="center">${statusMap[entity.status?string]!''}</td>
					<#elseif head.prop = "lastLockoutTime">	
						<td align="center">
							<#if entity.status.name() == "locked" >
							${entity.lastLockoutTime?string["yyyy-MM-dd"]}
							</#if>
						</td>
					<#elseif head.prop = "lastLoginTime">	
						<td align="center">
							<#if (entity.lastLoginTime?string["yyyy"] != "1970") >
							${entity.lastLoginTime?string["yyyy-MM-dd"]}
							</#if>
						</td>
					<#elseif head.prop = "deleteTime">	
						<td align="center">
							<#if entity.deleted >
							${entity.deleteTime?string["yyyy-MM-dd"]}
							</#if>
						</td>
					<#else>	
						<td></td>
					</#if>
                </#list>
            </tr>
            </#list>
        </tbody>
    </table>
</div>
</body>
</html>