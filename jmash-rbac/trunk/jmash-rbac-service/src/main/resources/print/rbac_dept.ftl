<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .tbody {width: 100%; font-size: 14pt; font-family: 'SimSun'; line-height: 28pt; position: relative; margin: auto; background: #FFFFFF; }
        table,.box {border-collapse: collapse; table-layout: fixed; margin-top: -1px; width: 100%; border-color: #999999; background: #FFFFFF;}
        h3,h5 {padding: 0px; margin: 0px; color: #000;}
        h3 {font-size: 30px;}
        h5 {font-weight: normal;}
        table tr td {font-size: 12px; height: 30px; border-color: #999999; line-height: 15px; padding: 0 5px}
        /*table tr td {font-size: 12px; height: 32px; border-color: #999999; line-height: 16px; padding: 0 5px}*/
        table tr td input {height: 100%; padding: 0px; border: none;}
        table tbody tr td {border:0.5px solid #000000}
        .border td {border:0.5px solid #000000}
        .box tr td {height: 175px; border-color: #999999; padding: 0 5px; text-align: left;}
        .box tr td span {display: block;}
        .box tr td b {display: block; font-weight: normal; margin-top: 40px; text-indent: 80px;}
        .box tr td i {font-style: normal; height: 48px; display: block; line-height: 24px; margin-top: 45px;vertical-align:middle; }
        .box tr td strong {font-weight: normal; display: block; margin-top: 5px; text-align: right;}
        .cont{ height:118px;}
        .cont2{ height:45px;}
        .heiti
        {
            font-family: simsun-bold;
        }
    </style>
</head>
<body>
<div class="tbody">
    <table style="table-layout: auto;repeat-footer:yes;">
        <thead>
        <tr class="border">
            <td colspan="${tableHeads?size -1 }" align="center"><h3>${title!''}</h3></td>
        </tr>
        <tr class="border">
            <td width="80" style="text-align:center">序号</td>
            <#list tableHeads as head>
                <#if head.prop = "NO">
					<td width="60" style="text-align:center">${head.label}</td>
				<#elseif head.prop = "deptId">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "deptCode">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "deptName">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "deptType">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "parentId">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "description">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "depth">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "orderBy">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "status">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "vision">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "createBy">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "createTime">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "updateBy">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#elseif head.prop = "updateTime">	
					<td width="80" style="text-align:center">${head.label!''}</td>
				<#else>	
					<td width="100" style="text-align:center" >${head.label!''}</td>
				</#if>			
            </#list>            
        </tr>
        </thead>
        <tbody>
            <#list deptList as entity>
            <tr>
                <#list tableHeads as head>
                    <#if head.prop = "NO">
						<td align="center">${ entity?counter }</td>			
					<#elseif head.prop = "deptId">	
						<td align="left">${entity.deptId!''}</td>
					<#elseif head.prop = "deptCode">	
						<td align="left">${entity.deptCode!''}</td>
					<#elseif head.prop = "deptName">	
						<td align="left">${entity.deptName!''}</td>
					<#elseif head.prop = "deptType">	
						<td align="left">${entity.deptType!''}</td>
					<#elseif head.prop = "parentId">	
						<td align="left">${entity.parentId!''}</td>
					<#elseif head.prop = "description">	
						<td align="left">${entity.description!''}</td>
					<#elseif head.prop = "depth">	
						<td align="left">${entity.depth!''}</td>
					<#elseif head.prop = "orderBy">	
						<td align="left">${entity.orderBy!''}</td>
					<#elseif head.prop = "status">	
						<td align="left">${entity.status!''}</td>
					<#elseif head.prop = "vision">	
						<td align="left">${entity.vision!''}</td>
					<#elseif head.prop = "createBy">	
						<td align="left">${entity.createBy!''}</td>
					<#elseif head.prop = "createTime">	
						<td align="left">${entity.createTime!''}</td>
					<#elseif head.prop = "updateBy">	
						<td align="left">${entity.updateBy!''}</td>
					<#elseif head.prop = "updateTime">	
						<td align="left">${entity.updateTime!''}</td>
					<#else>	
						<td></td>
					</#if>
                </#list>
            </tr>
            </#list>
        </tbody>
    </table>
</div>
</body>
</html>