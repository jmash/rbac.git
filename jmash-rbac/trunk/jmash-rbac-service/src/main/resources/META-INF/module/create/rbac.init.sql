/*!40101 SET NAMES utf8 */;

INSERT INTO rbac_operation (operation_id,operation_code,operation_name,order_by) VALUES
	 (0x8AA96E9EB64C401EA84389AC206999DD,'list','查询',1),
	 (0x43AB026FBDD3495CB4CE300D1BD132F8,'view','查看',2),
	 (0x17A39354857946DA81AC167FA18B81E5,'add','新增',3),
	 (0x960BE49D726F416BA02F2BDF35F8E887,'update','修改',4),
	 (0x7813FAE6CC784AF5A87E04F85DCB8300,'delete','删除',5),
	 (0x1DC877909759497F9ED2B54982670E58,'approve','审核',6),
	 (0x5FBA6305027844AC873838402F14DAE9,'upload','上传',7),
	 (0x2C0166E883074C7CB5A2D10CA857397F,'download','下载',8), 
	 (0x445F52E606A74A419A5C6A74F6FAA069,'export','导出',9),
	 (0x9E0F5ED0D1B4428AB240082E893E2AA9,'print','打印',10);
	
INSERT INTO rbac_operation (operation_id,operation_code,operation_name,order_by) VALUES
	 (0xA03676CE03C74FD28556F2A1A9D7DC74,'import','导入',11),
	 (0x4306E0B6F9C14CD7B048AE9A8D80E0BD,'enable','启用/禁用',12),
	 (0x8A96BA71C97641F4BECCBB3DF629939B, 'move', '移动', 13),
	 (0x39910CFF21F14CCAAE76EB20A092EF00,'switch','切换',14),
	 (0xDB1FCBB8921E4B64A37DEBDC115B72FF,'lock','锁定',15),
	 (0x020CBC973DF2480D8FA25CF5CECF7DA8,'unlock','解锁',16),
	 (0xDD175731CAFA4D67B1FF9CD3583996B5,'reset','重置',17),
	 (0x5E027C831BA14C5D86A8782B102BC91A,'unbind','解除认证',18),
	 (0x11CD08924145470AA82EA6A14A411919,'dissolve','解散',19);
	 
INSERT INTO rbac_module (module_id, module_code, module_name, order_by, description_) VALUES
     (0xC415075752D14BFAA606521EAAE11C5F, 'rbac', '系统模块', 1, '标准RBAC模块'),
     (0xBFE95C4534BF4E70AE9D0993D252B2ED, 'dict', '数据字典模块', 2, '');

INSERT INTO rbac_resource (resource_id,module_id,resource_code,resource_name,resource_type,parent_id,depth_,order_by,target_,url_,hidden_,icon_) VALUES
     (0x8C9F6898F6B0440BB28EA587C11B6E5E,0xC415075752D14BFAA606521EAAE11C5F,'system','系统管理','catalog',0x00000000000000000000000000000000,1,1,'','/system',0,'system'),
     (0xBA2FA65E14724CDBBC94D54A671F7DA4,0xC415075752D14BFAA606521EAAE11C5F,'user','用户管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,1,'','user',0,'yonghuguanli'),
     (0x0DCEE6DCBD074BCBA20F90A84C72E106,0xC415075752D14BFAA606521EAAE11C5F,'opens','三方登录','other',0xBA2FA65E14724CDBBC94D54A671F7DA4,3,1,'','opens',0,'drag'),
     (0x1203E0AD69BE452FBB1AB5737E7AA41D,0xC415075752D14BFAA606521EAAE11C5F,'role','角色管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,2,'','role',0,'role'),
     (0xCE1B3B0F870642C48ED22AD862B58048,0xC415075752D14BFAA606521EAAE11C5F,'module','模块管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,3,'','module',0,'menu'),
     (0x4975C3BC855B4F04AA48A83D8AEA46BD,0xC415075752D14BFAA606521EAAE11C5F,'resource','资源管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,4,'','menu',0,'menu'),
     (0x6FD7178D691D41B5BFF8D815871F5680,0xC415075752D14BFAA606521EAAE11C5F,'operation','操作管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,5,'','operation',0,'caozuoguanliyuan'),
     (0xBE241363BA6C47389CF1591E24CE2A45,0xC415075752D14BFAA606521EAAE11C5F,'perm','权限管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,6,'','permission',0,'quanxianguanli'),
                  
     (0xE15DBE7E1CA4492DBE754746F92358BF,0xC415075752D14BFAA606521EAAE11C5F,'dept','部门管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,7,'','dept',0,'bumenguanli'),
	 (0x76E6E1C132E34EE4936114840894EC54,0xC415075752D14BFAA606521EAAE11C5F,'job','岗位管理','menu',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,8,'','job',0,'gangweiguanli'),
	
	 (0xFA6A5DC325964800B6E6F4A6B2B46984,0xC415075752D14BFAA606521EAAE11C5F,'logManage','日志管理','catalog',0x8C9F6898F6B0440BB28EA587C11B6E5E,2,9,'','log',0,'log'),
     (0x174EA4AA0BDD45BF8D1359164600C651,0xC415075752D14BFAA606521EAAE11C5F,'log','操作日志','menu',0xFA6A5DC325964800B6E6F4A6B2B46984,3,1,'','oplog',0,'jiemianxijietubiao-26'),        
     (0xBD2EFCB36E5A4193B4F607D072F53EB1,0xC415075752D14BFAA606521EAAE11C5F,'userLog','安全日志','menu',0xFA6A5DC325964800B6E6F4A6B2B46984,3,2,'','userLog',0,'anquanrizhi');
	
INSERT INTO rbac_resource (resource_id,module_id,resource_code,resource_name,resource_type,parent_id,depth_,order_by,target_,url_,hidden_,icon_) VALUES	
	 (0x78A73ED733B84DB2B4118677FEAC9461,0xBFE95C4534BF4E70AE9D0993D252B2ED,'dict','数据字典','catalog',0x00000000000000000000000000000000,1,2,'','/dict',0,'system'),
	 (0x90F063B9F36340ECA6E32756D6FBCB62,0xBFE95C4534BF4E70AE9D0993D252B2ED,'dict_type','数据字典类型','menu',0x78A73ED733B84DB2B4118677FEAC9461,2,1,'','os-dict-type',0,'setting'),
	 (0x911F6ADE6D84455B899E34F035A769CC,0xBFE95C4534BF4E70AE9D0993D252B2ED,'dict_entry','普通数据字典','menu',0x78A73ED733B84DB2B4118677FEAC9461,2,2,'','os-dict-entry',0,'setting'),
	 (0x38DAFF5165D64D6B88232BC52EA7CA85,0xBFE95C4534BF4E70AE9D0993D252B2ED,'dict_lay_entry','层级字典管理','menu',0x78A73ED733B84DB2B4118677FEAC9461,2,3,'','os-dict-lay-entry',0,'setting');   
	 
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0x0EAC69DCE138454D8B8BB7214D3EBB1A,'rbac:user_log:view','系统模块->安全日志->查看'),
	 (0xF38D4E4E2CF24423A225E95349C38D85,'rbac:user_log:list','系统模块->安全日志->查询'),
	 (0x1F2E88489C874D55819084A65EFA2385,'rbac:user_log:export','系统模块->安全日志->导出'),
	 (0x92DE8F5FE0844BA8A8F1D12CAA0FF9A6,'rbac:user_log:delete','系统模块->安全日志->删除'),
	 (0x3B2D7C665D3F4639996E26330F7F2827,'rbac:user:view','系统模块->用户管理->查看'),
	 (0xDDD6D51B707C4610A40EFF355DA8883D,'rbac:user:update','系统模块->用户管理->修改'),
	 (0xFA781FAD4C56467B9B4562F0203E8C74,'rbac:user:unlock','系统模块->用户管理->解锁'),
	 (0x0F22CC602D994D5DA66338131B24A7B8,'rbac:user:switch','系统模块->用户管理->切换'),
	 (0x463C35CAD4484FD38B9DCD39DC80F049,'rbac:user:resetPassword','系统模块->用户管理->重置密码'),
	 (0x21ACE4A73B134537912C54E70946B466,'rbac:user:reset','系统模块->用户管理->重置');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0xE7B4D192231941828599EA4ACA80730E,'rbac:user:print','系统模块->用户管理->打印'),
	 (0x68E7898CD65F4F6AAFEDD630826F9C4B,'rbac:user:lock','系统模块->用户管理->锁定'),
	 (0x49FD8444ED7B44AABFDCDFE0E595BC38,'rbac:user:list','系统模块->用户管理->查询'),
	 (0xC5F722C8280C4CE7BFC3DE8561F9CA82,'rbac:user:import','系统模块->用户管理->导入'),
	 (0x476270BBAF7F4AA386CCA6961C2B59D8,'rbac:user:export','系统模块->用户管理->导出'),
	 (0x3B4FCBBAB57B45768401300E896C2F8D,'rbac:user:enable','系统模块->用户管理->启用/禁用'),
	 (0xD5E9B729175947D49BD09C45A98B4EA3,'rbac:user:delete','系统模块->用户管理->删除'),
	 (0xF33D3352A5924311BA769600A6B8D6C0,'rbac:user:add','系统模块->用户管理->新增'),
	 (0xFB3A465B2BB04F0F90CCB7620FC42947,'rbac:role:view','系统模块->角色管理->查看'),
	 (0xCD373EB9DF4A4DCBAC72E4125BE59402,'rbac:role:update','系统模块->角色管理->修改');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0xF8E2654AE1D841A28A0C3ABF139D0F7F,'rbac:role:list','系统模块->角色管理->查询'),
	 (0xD4B2131EF8F5439EBF26AC6D369C6A78,'rbac:role:delete','系统模块->角色管理->删除'),
	 (0xFD829999E08D41DE93CE369A472B42FE,'rbac:role:add','系统模块->角色管理->新增'),
	 (0x09D880ED304E4E918E89A4EE88F71A28,'rbac:resource:view','系统模块->资源管理->查看'),
	 (0x26CDD639B8A348F4BDBB5ADAF2A5B626,'rbac:resource:update','系统模块->资源管理->修改'),
	 (0xD3E6F9AA8F6E4864AD94B6421BD8A13F,'rbac:resource:list','系统模块->资源管理->查询'),
	 (0x906F6E6A9FC74DB79C4E07E5B9E43392,'rbac:resource:import','系统模块->资源管理->导入'),
	 (0xDED9333CBE774A409331BAEA48BF219A,'rbac:resource:export','系统模块->资源管理->导出'),
	 (0xFBDE57A6E28E475CA5ADA5F77191F020,'rbac:resource:download','系统模块->资源管理->下载'),
	 (0xC78CAB88800A47AA89999777BC9DAF9C,'rbac:resource:delete','系统模块->资源管理->删除');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0x01E31B50038C46CA95C87F683879E33C,'rbac:resource:add','系统模块->资源管理->新增'),
	 (0x2ACAB220EA9141E797452E6440931EB5,'rbac:perm:view','系统模块->权限管理->查看'),
	 (0xF5C791CD10124B1CAE9852624E69145C,'rbac:perm:update','系统模块->权限管理->修改'),
	 (0x671EFCA53D264180AF1A1D5F8FBF31D2,'rbac:perm:list','系统模块->权限管理->查询'),
	 (0xB6D7B47046454F15818E60697905BD31,'rbac:perm:delete','系统模块->权限管理->删除'),
	 (0x913BD3A942C747EB87FED005E82E4E59,'rbac:perm:add','系统模块->权限管理->新增'),
	 (0xE7EC81BE7B9343A89061B9F146EC30B1,'rbac:operation:view','系统模块->操作管理->查看'),
	 (0x1E588E552FEB44B7B816C9E477292C0C,'rbac:operation:update','系统模块->操作管理->修改'),
	 (0x2C2215F9DD104F768DA4DEC656E9C5E1,'rbac:operation:list','系统模块->操作管理->查询'),
	 (0x0B26B5825E85431E994324FE6CC63FCB,'rbac:operation:delete','系统模块->操作管理->删除');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0x4B8B63F774A24FF0AE8CBC480ECD3A04,'rbac:operation:add','系统模块->操作管理->新增'),
	 (0xE12D67E8F5E2402CB27E1DE633AAC0A6,'rbac:opens:view','系统模块->三方登录->查看'),
	 (0xDD06ADC36DD04AC09D25F45A2942FB0C,'rbac:opens:list','系统模块->三方登录->查询'),
	 (0xE12ACCFA36284CEAAB164B319C409ABF,'rbac:opens:delete','系统模块->三方登录->删除'),
	 (0xDD840E5250584B1489C77486CDD975CD,'rbac:module:view','系统模块->模块管理->查看'),
	 (0x6C5EDD52389C495C9FB3A634AA42DEEB,'rbac:module:update','系统模块->模块管理->修改'),
	 (0x856C884F1BFF4B449635405F257BD0D4,'rbac:module:list','系统模块->模块管理->查询'),
	 (0x1DB4A39CA8C94FF0951BA0708149AF8F,'rbac:module:delete','系统模块->模块管理->删除'),
	 (0xB4DB6CBD67764C7880B54C9F4852082A,'rbac:module:add','系统模块->模块管理->新增'),
	 (0xA5052AD4A02345A1AF206311CAB51253,'rbac:log:view','系统模块->操作日志->查看');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0x8DB17BDE37D04EAF9FFBEF1AA6C491A7,'rbac:log:list','系统模块->操作日志->查询'),
	 (0x53B891DBB3604F66B931D4E092F0E0A9,'rbac:log:export','系统模块->操作日志->导出'),
	 (0x5F36A441C09D4CDC9DD96A3C0141DB76,'rbac:log:delete','系统模块->操作日志->删除'),
	 (0x3C44BFC8F5974AC49F72AB44B077E66E,'rbac:job:view','系统模块->岗位管理->查看'),
	 (0xD2436DF4B108419B8E3D9033698FA950,'rbac:job:update','系统模块->岗位管理->修改'),
	 (0xFDF50C3373364AA6BA24BC5D1D51D8D5,'rbac:job:list','系统模块->岗位管理->查询'),
	 (0x04A7271CE8B6453D86A4FE82DEE8AC28,'rbac:job:delete','系统模块->岗位管理->删除'),
	 (0x01667FA871284D65A8763EA2E9E93F4E,'rbac:job:add','系统模块->岗位管理->新增'),
	 (0x32B0E381276C4FFEB61A6D59D8ECD50C,'rbac:dept:view','系统模块->部门管理->查看'),
	 (0x62B2570D2D424A5083F48B3E206A3FEB,'rbac:dept:update','系统模块->部门管理->修改');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0xC267814DA9FA40178369527F696D0DEE,'rbac:dept:list','系统模块->部门管理->查询'),
	 (0xDAB27B1E6C7D4805A2CF4747650A94F6,'rbac:dept:import','系统模块->部门管理->导入'),
	 (0x55EBDC98DEE84C558620578C4901CF62,'rbac:dept:export','系统模块->部门管理->导出'),
	 (0xCDF6CF1AD7DD4ADC822BF1505BB3C8B5,'rbac:dept:download','系统模块->部门管理->下载'),
	 (0xC9B4B956FF7C487184F8E3E802BE229E,'rbac:dept:delete','系统模块->部门管理->删除'),
	 (0xE034F712252F4775AEA14DBCA1B362A9,'rbac:dept:add','系统模块->部门管理->新增'),
	 (0x6AFF5D7AD9494C60A2D01264C6AFA14C,'dict:dict_type:view','数据字典模块->数据字典类型->查看'),
	 (0xE8C2B26B17134184BE07FE7C612F77B2,'dict:dict_type:update','数据字典模块->数据字典类型->修改'),
	 (0x9CC2340EF3144C9FA3E57988F4BF546D,'dict:dict_type:move','数据字典模块->数据字典类型->移动'),
	 (0xF1932BE5CB27486B899FBBEB9A14F699,'dict:dict_type:list','数据字典模块->数据字典类型->查询');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0xA2AD8C6E56CC4589957DF20FF98D42BF,'dict:dict_type:import','数据字典模块->数据字典类型->导入'),
	 (0xA8AC91CF9B584CC1B5536046E80E5C38,'dict:dict_type:export','数据字典模块->数据字典类型->导出'),
	 (0x1B386FCD332F43F29347F9F6AF6CD283,'dict:dict_type:enable','数据字典模块->数据字典类型->启用/禁用'),
	 (0xD8BED835B3FE4717ADB0EB5715D394A2,'dict:dict_type:delete','数据字典模块->数据字典类型->删除'),
	 (0x0354267EAE714B1DA7CDDEDC50AD621B,'dict:dict_type:add','数据字典模块->数据字典类型->新增'),
	 (0xC91EDBACC64C464593B0D5D0EDDFCDE7,'dict:dict_lay_entry:view','数据字典模块->层级字典管理->查看'),
	 (0x07DFCC9C6091438AA46B1CB39A7F0E6F,'dict:dict_lay_entry:update','数据字典模块->层级字典管理->修改'),
	 (0xDA3E2532C94F4F259C16169F43A35EBA,'dict:dict_lay_entry:move','数据字典模块->层级字典管理->移动'),
	 (0xEFBF17CAC716484B83E8E977A322962B,'dict:dict_lay_entry:list','数据字典模块->层级字典管理->查询'),
	 (0xFD633715E35F4F2F9B4D49E242AA719B,'dict:dict_lay_entry:enable','数据字典模块->层级字典管理->启用/禁用');
INSERT INTO rbac_perm (perm_id,perm_code,perm_name) VALUES
	 (0xF067BAD7E4184339B491B36B1D233ACA,'dict:dict_lay_entry:delete','数据字典模块->层级字典管理->删除'),
	 (0x0BCE088A8E814A6E8793537F699004FA,'dict:dict_lay_entry:add','数据字典模块->层级字典管理->新增'),
	 (0xEB92AED7CDF94710BBC95AF9B3108F1F,'dict:dict_entry:view','数据字典模块->普通数据字典->查看'),
	 (0x4D61EB7B19EE48B3B4B86C79A4F73520,'dict:dict_entry:update','数据字典模块->普通数据字典->修改'),
	 (0x7E8ACB855D8E42E688984D83C43A113C,'dict:dict_entry:move','数据字典模块->普通数据字典->移动'),
	 (0xED8B99DFCC9F4D6CBB742432A582DF2B,'dict:dict_entry:list','数据字典模块->普通数据字典->查询'),
	 (0x6E7641E9B001469E83D91916B0DC4210,'dict:dict_entry:enable','数据字典模块->普通数据字典->启用/禁用'),
	 (0xB9ED673854A04F7EA12972E5D78F4D72,'dict:dict_entry:delete','数据字典模块->普通数据字典->删除'),
	 (0xD369371EC52F47BDBD44796CF1422ACC,'dict:dict_entry:add','数据字典模块->普通数据字典->新增');
     	 
INSERT INTO rbac_role (role_id, role_code, role_name, role_type, parent_id, depth_, order_by, description_, create_by, create_time) VALUES
     (0x5848CF883D1E4E79A969B6D07868231B, 'admin', '管理员角色', 'role', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0x56800B0E52DC4FE997FD68162A58B46B, 'user', '普通用户', 'role', 0x00000000000000000000000000000000, 1, 2, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0x74378066CA9148A8B967E08BBB558AC1, 'tester', '测试角色', 'role', 0x00000000000000000000000000000000, 1, 3, '系统测试/运维', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0xF35DECE412744A7385BF8683D650796C, 'audit', '审计员', 'role', 0x00000000000000000000000000000000, 1, 4, '日志审计', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0xEF461FDB8E844931BDF6D949F5B301B1, 'system', '系统角色', 'role', 0x00000000000000000000000000000000, 1, 7, '', 0xA12CB34C198549A69DA6F220EDF50E1F, '2024-04-30 14:39:29.000'),
     (0xB52AD69AA61C11EF9315003EE1CFC1F9, 'member', '职工', 'job', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0x23F5BAF7488C429C9FA30670EEAC83BD, 'organ', '组织管理员', 'role', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0x0F9AC518EBB14B51ACCC2634FB587CD2, 'cms', '网站角色', 'role', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59');
	 
INSERT INTO rbac_user (tenant_id,user_id,unified_id, directory_id, login_name, mobile_phone, mobile_phone_ins, email_, real_name, nick_name, avatar_, birth_date, gender_, phone_approved, email_approved, approved_, status_,storage_, last_lockout_time, failed_time, failed_count, last_login_time, version_, create_by, create_time, update_by, update_time, delete_by, deleted_, delete_time) VALUES
     ('default',0xA12CB34C198549A69DA6F220EDF50E1F,'', 'jmash', 'admin', '390F3A5949CC6F76EA93F5BC62E6F6CE', '183****1112', '', '名码云联', '名码云联', '', '2018-09-03', 2, 1, 0, 1, 'enabled', '', '2018-09-03 23:59:59', '1970-01-01 23:59:59', 0, '2018-09-03 23:59:59', 0, 0x00000000000000000000000000000000, '2018-09-03 23:59:59', 0xA12CB34C198549A69DA6F220EDF50E1F, '2018-09-03 23:59:59', NULL, 0, '2018-09-03 23:59:59'),
     ('default',0x2886094B087B40F0874C7AC8A2003841,'', 'jmash', 'jmash', '8201CCC2F98683387ECACBFEAA9EE4DF', '186****5682', '', 'Jmash', 'Jmash', '', '2022-09-18', 0, 1, 0, 1, 'enabled', '','2022-09-18 23:59:59', '1970-01-01 23:59:59', 0, '2022-09-18 23:59:59', 0, 0x00000000000000000000000000000000, '2022-09-18 23:59:59', 0x2886094B087B40F0874C7AC8A2003841, '2022-09-18 23:59:59', NULL, 0, '2022-09-18 23:59:59');


INSERT INTO rbac_user_secret (secret_id, user_id, secret_type, pwd_value, pwd_format, pwd_alt, is_temp, version_, create_by, create_time, update_by, update_time) VALUES
     (0x7725A0BA4E43425E96DA23A1CEB1EEBE, 0xA12CB34C198549A69DA6F220EDF50E1F, 'Login', '0d792df507ff6131d016a976688e5e807ca5426fe9a5dc910008662aca672347', 'SM3', '410831e5a8924aca98a613f3d790ecfc', 0, 3, 0x00000000000000000000000000000000, '2018-09-03 23:59:59', 0xA12CB34C198549A69DA6F220EDF50E1F, '2018-09-03 23:59:59'),
     (0xCF068B67BF1D4403860BA2A851BD5AD0, 0x2886094B087B40F0874C7AC8A2003841, 'Login', 'ef831004ebfd8ff19540a2d4749eb110947ef5ec538e99a587d2a8b7aef1025f', 'SM3', '503e2919a33445759dc50e082e40f5f8', 0, 5, 0x00000000000000000000000000000000, '2022-09-18 23:59:59', 0xA12CB34C198549A69DA6F220EDF50E1F, '2022-09-18 23:59:59');
     

INSERT INTO rbac_users_roles (tenant_id,user_id, role_id) VALUES
     ('default',0xA12CB34C198549A69DA6F220EDF50E1F, 0x5848CF883D1E4E79A969B6D07868231B),
     ('default',0xA12CB34C198549A69DA6F220EDF50E1F, 0x74378066CA9148A8B967E08BBB558AC1),
     ('default',0xA12CB34C198549A69DA6F220EDF50E1F, 0xEF461FDB8E844931BDF6D949F5B301B1),
     ('default',0x2886094B087B40F0874C7AC8A2003841, 0x74378066CA9148A8B967E08BBB558AC1),
     ('default',0x2886094B087B40F0874C7AC8A2003841, 0xEF461FDB8E844931BDF6D949F5B301B1);
     
     
     
     
     
     