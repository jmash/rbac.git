/*!40101 SET NAMES utf8 */;

DROP TABLE IF EXISTS rbac_users_roles;

DROP TABLE IF EXISTS rbac_users_jobs;

DROP TABLE IF EXISTS rbac_user_secret;

DROP TABLE IF EXISTS rbac_users_jobs;

DROP TABLE IF EXISTS rbac_roles_duty;

DROP TABLE IF EXISTS rbac_roles_perms;

DROP TABLE IF EXISTS rbac_perms_depend;

DROP TABLE IF EXISTS rbac_user_log;

DROP TABLE IF EXISTS rbac_opens;

DROP TABLE IF EXISTS rbac_log;

DROP TABLE IF EXISTS rbac_token;

DROP TABLE IF EXISTS rbac_dept;

DROP TABLE IF EXISTS rbac_role;

DROP TABLE IF EXISTS rbac_perm;

DROP TABLE IF EXISTS rbac_resource;

DROP TABLE IF EXISTS rbac_operation;

DROP TABLE IF EXISTS rbac_module;

DROP TABLE IF EXISTS rbac_open_app;

DROP TABLE IF EXISTS rbac_user;