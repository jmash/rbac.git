/*!40101 SET NAMES utf8 */;

update rbac_role set role_name = '管理员角色' where role_code='admin' ;

INSERT INTO rbac_role (role_id, role_code, role_name, role_type, parent_id, depth_, order_by, description_, create_by, create_time)
VALUES(0xEF461FDB8E844931BDF6D949F5B301B1, 'system', '系统角色', 'role', 0x00000000000000000000000000000000, 1, 7, '', 0xA12CB34C198549A69DA6F220EDF50E1F, '2024-04-30 14:39:29.000');

INSERT INTO rbac_users_roles (user_id, role_id) 
VALUES (0x2886094B087B40F0874C7AC8A2003841, 0xEF461FDB8E844931BDF6D949F5B301B1);

