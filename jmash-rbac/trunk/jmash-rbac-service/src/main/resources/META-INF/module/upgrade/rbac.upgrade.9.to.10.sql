/*!40101 SET NAMES utf8 */;
ALTER TABLE rbac_user ADD storage_ varchar(30) NULL COMMENT '个人信息存储区' AFTER status_;
update rbac_user set storage_ = '' where storage_ is null;

INSERT INTO rbac_users_roles (user_id, role_id) VALUES
     (0xA12CB34C198549A69DA6F220EDF50E1F, 0xEF461FDB8E844931BDF6D949F5B301B1);