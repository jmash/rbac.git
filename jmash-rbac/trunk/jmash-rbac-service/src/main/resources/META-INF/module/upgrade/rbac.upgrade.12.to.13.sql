/*!40101 SET NAMES utf8 */;

ALTER TABLE rbac_user MODIFY COLUMN nick_name varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL NULL COMMENT '昵称';
ALTER TABLE rbac_user MODIFY COLUMN login_name varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL NULL COMMENT '用户名/登录名';

ALTER TABLE rbac_opens MODIFY COLUMN open_id varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'OpenID';
ALTER TABLE rbac_opens MODIFY COLUMN nick_name varchar(120) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL NULL COMMENT '昵称';

ALTER TABLE rbac_token MODIFY COLUMN issuer_ varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL NULL COMMENT '发行机构';


INSERT INTO rbac_role (role_id, role_code, role_name, role_type, parent_id, depth_, order_by, description_, create_by, create_time) VALUES
     (0xB52AD69AA61C11EF9315003EE1CFC1F9, 'member', '职工', 'job', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59'),
     (0x23F5BAF7488C429C9FA30670EEAC83BD, 'organ', '组织管理员', 'role', 0x00000000000000000000000000000000, 1, 1, '', 0x00000000000000000000000000000000, '2018-09-03 23:59:59');




