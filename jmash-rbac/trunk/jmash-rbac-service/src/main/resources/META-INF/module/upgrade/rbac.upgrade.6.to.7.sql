/*!40101 SET NAMES utf8 */;

ALTER TABLE rbac_user CHANGE vision_ version_ int(11) NOT NULL COMMENT '乐观锁';
ALTER TABLE rbac_user_secret CHANGE vision_ version_ int(11) NOT NULL COMMENT '乐观锁';
ALTER TABLE rbac_opens CHANGE vision_ version_ int(11) NOT NULL COMMENT '乐观锁';
ALTER TABLE rbac_token CHANGE vision_ version_ int(11) NOT NULL COMMENT '乐观锁';
ALTER TABLE rbac_dept CHANGE vision_ version_ int(11) NOT NULL COMMENT '乐观锁';

