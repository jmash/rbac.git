/*!40101 SET NAMES utf8 */;
ALTER TABLE rbac_dept  ADD COLUMN is_open bool AFTER status_;
update rbac_dept set is_open = false where is_open is null;

