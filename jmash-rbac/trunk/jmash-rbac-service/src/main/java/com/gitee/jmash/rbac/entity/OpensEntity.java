
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.BasicEntity;
import com.gitee.jmash.rbac.entity.OpensEntity.OpensPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;
import jmash.rbac.protobuf.OpensType;


/**
 * 本代码为自动生成工具生成 三方登录表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_opens")
@IdClass(OpensPk.class)
public class OpensEntity  extends BasicEntity<OpensPk>  {

  private static final long serialVersionUID = 1L;
  
  /** OpenID. */
  private String openId= UUID.randomUUID().toString()  ;
  /** 账号ID. */
  private UUID userId= UUID.randomUUID()  ;
  /** 应用ID. */
  private String appId= UUID.randomUUID().toString()  ;
  /** 唯一ID. */
  private String unionId ;
  /** 昵称. */
  private String nickName ;
  /** 三方OpenID Type. */
  private OpensType openType ;

  /**
  * 默认构造函数.
  */
  public OpensEntity(){
    super();
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public OpensPk  getEntityPk(){
    OpensPk pk=new OpensPk();
    pk.setAppId(this.appId);
    pk.setUserId(this.userId);
    pk.setOpenId(this.openId);
    return pk;
  }
  /** 实体主键. */
  public void setEntityPk(OpensPk  pk){
    this.appId=pk.getAppId();
    this.userId=pk.getUserId();
    this.openId=pk.getOpenId();
  }
  
   /** OpenID(OpenId). */
  @Id

  @Column(name="open_id",nullable = false)
  public String getOpenId() {
    return openId;
  }
  /** OpenID(OpenId). */
  public void setOpenId(String openId) {
    this.openId = openId;
  }
   /** 账号ID(UserId). */
  @Id
  @Column(name="user_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }
  /** 账号ID(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }
   /** 应用ID(AppId). */
  @Id

  @Column(name="app_id",nullable = false)
  public String getAppId() {
    return appId;
  }
  /** 应用ID(AppId). */
  public void setAppId(String appId) {
    this.appId = appId;
  }
   /** 唯一ID(UnionId). */
  @Column(name="union_id")
  public String getUnionId() {
    return unionId;
  }
  /** 唯一ID(UnionId). */
  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }
   /** 昵称(NickName). */
  @Column(name="nick_name")
  public String getNickName() {
    return nickName;
  }
  /** 昵称(NickName). */
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
   /** 三方OpenID Type(OpenType). */
  @Column(name="open_type")
  @Enumerated(EnumType.STRING)
  public OpensType getOpenType() {
    return openType;
  }
  /** 三方OpenID Type(OpenType). */
  public void setOpenType(OpensType openType) {
    this.openType = openType;
  }

	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OpensEntity other = (OpensEntity) obj;
	if (getEntityPk() == null) {
		if (other.getEntityPk() != null){
			return false;
		}
	} else if (!getEntityPk().equals(other.getEntityPk())){
		return false;
	}
    return true;
  }
  
  
  /**
   * OpensPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class OpensPk implements Serializable{
  
    private static final long serialVersionUID = 1L;
    
    /** 应用ID. */
    @NotNull
    private String appId;
    /** 账号ID. */
    @NotNull
    private UUID userId;
    /** OpenID. */
    @NotNull
    private String openId;

    /** 无参构造方法. */
    public OpensPk(){
    }

    /** 有参构造方法. */
    public OpensPk(String openId,UUID userId,String appId){
      super();
      this.openId = openId;
      this.userId = userId;
      this.appId = appId;
    }
    
    /** 应用ID(AppId). */
	@Column(name="app_id",nullable = false)
    public String getAppId() {
      return appId;
    }
    
    /** 应用ID(AppId). */
    public void setAppId(String appId) {
      this.appId = appId;
    }
    /** 账号ID(UserId). */
	@Column(name="user_id",nullable = false)
    public UUID getUserId() {
      return userId;
    }
    
    /** 账号ID(UserId). */
    public void setUserId(UUID userId) {
      this.userId = userId;
    }
    /** OpenID(OpenId). */
	@Column(name="open_id",nullable = false)
    public String getOpenId() {
      return openId;
    }
    
    /** OpenID(OpenId). */
    public void setOpenId(String openId) {
      this.openId = openId;
    }
      
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result
          + ((appId == null) ? 0 : appId.hashCode());
      result = prime * result
          + ((userId == null) ? 0 : userId.hashCode());
      result = prime * result
          + ((openId == null) ? 0 : openId.hashCode());
      return result;
    }
  
    @Override
    public boolean equals(Object obj) {
      if (this == obj){
        return true;
      }
      if (obj == null){
        return false;
      }
      if (getClass() != obj.getClass()){
        return false;
      }
      final OpensPk other = (OpensPk) obj;
      
      if (appId == null) {
        if (other.appId != null){
          return false;
        }
      } else if (!appId.equals(other.appId)){
        return false;
      }
      if (userId == null) {
        if (other.userId != null){
          return false;
        }
      } else if (!userId.equals(other.userId)){
        return false;
      }
      if (openId == null) {
        if (other.openId != null){
          return false;
        }
      } else if (!openId.equals(other.openId)){
        return false;
      }
      return true;
    }
  }
}
