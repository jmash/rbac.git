package com.gitee.jmash.rbac.model;

import java.util.List;
import java.util.UUID;
import com.gitee.jmash.rbac.entity.PermEntity;
import jmash.rbac.protobuf.ResourceType;

public class ResourcePermResult {
  
  /** 资源ID. */
  private UUID resourceId;
  /** 父资源ID. */
  private UUID parentId;
  /** 模块编码. */
  private String moduleCode;
  /** 资源编码. */
  private String resourceCode;
  /** 资源名称. */
  private String resourceName;
  /** 图标. */
  private String icon;
  /** 资源类型. */
  private ResourceType resourceType;
  /** 目标. */
  private String target;
  /** URL. */
  private String url;
  /** 是否隐藏. */
  private Boolean hidden;
  /** 权限实体. */
  private List<PermEntity> permEntities;

  public UUID getResourceId() {
    return resourceId;
  }

  public void setResourceId(UUID resourceId) {
    this.resourceId = resourceId;
  }

  public String getResourceName() {
    return resourceName;
  }

  public void setResourceName(String resourceName) {
    this.resourceName = resourceName;
  }

  public UUID getParentId() {
    return parentId;
  }

  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  public String getModuleCode() {
    return moduleCode;
  }

  public void setModuleCode(String moduleCode) {
    this.moduleCode = moduleCode;
  }

  public String getResourceCode() {
    return resourceCode;
  }

  public void setResourceCode(String resourceCode) {
    this.resourceCode = resourceCode;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public ResourceType getResourceType() {
    return resourceType;
  }

  public void setResourceType(ResourceType resourceType) {
    this.resourceType = resourceType;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  public List<PermEntity> getPermEntities() {
    return permEntities;
  }

  public void setPermEntities(List<PermEntity> permEntities) {
    this.permEntities = permEntities;
  }
}
