
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import com.gitee.jmash.core.orm.jpa.entity.TreeEntity;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import jmash.rbac.protobuf.RoleType;

/**
 * 本代码为自动生成工具生成 角色/职务表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_role")
public class RoleEntity extends CreateEntity<UUID> implements TreeEntity<UUID>{

  private static final long serialVersionUID = 1L;
  /** 超级管理员. */
  public static final String ADMINISTRATOR="administrator";
  /** 某租户管理员. */
  public static final String ADMIN="admin";
  /** 被测试角色. */
  public static final String TESTER = "tester";
  /** 系统角色(可通过API接口调用). */
  public static final String SYSTEM = "system";

  /** 角色ID/职务ID. */
  private UUID roleId = UUID.randomUUID();
  /** 角色编码/职务编码. */
  private String roleCode;
  /** 角色名称/职务名称. */
  private String roleName;
  /** 角色/职务. */
  private RoleType roleType;
  /** 父角色. */
  private UUID parentId = UUIDUtil.emptyUUID();
  /** 深度. */
  private Integer depth;
  /** 排序. */
  private Integer orderBy;
  /** 描述. */
  private String description;

  private List<RoleEntity> children = new ArrayList<>();

  /**
   * 默认构造函数.
   */
  public RoleEntity() {
    super();

  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.roleId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.roleId = pk;
  }

  /** 角色ID/职务ID(RoleId). */
  @Id
  @Column(name = "role_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getRoleId() {
    return roleId;
  }

  /** 角色ID/职务ID(RoleId). */
  public void setRoleId(UUID roleId) {
    this.roleId = roleId;
  }

  /** 角色编码/职务编码(RoleCode). */

  @Column(name = "role_code", nullable = false)
  public String getRoleCode() {
    return roleCode;
  }

  /** 角色编码/职务编码(RoleCode). */
  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }

  /** 角色名称/职务名称(RoleName). */

  @Column(name = "role_name", nullable = false)
  public String getRoleName() {
    return roleName;
  }

  /** 角色名称/职务名称(RoleName). */
  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  /** 角色/职务(RoleType). */

  @Column(name = "role_type", nullable = false)
  @Enumerated(EnumType.STRING)
  public RoleType getRoleType() {
    return roleType;
  }

  /** 角色/职务(RoleType). */
  public void setRoleType(RoleType roleType) {
    this.roleType = roleType;
  }

  /** 父角色(ParentId). */
  @Column(name = "parent_id", columnDefinition = "BINARY(16)")
  public UUID getParentId() {
    return parentId;
  }

  /** 父角色(ParentId). */
  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  /** 深度(Depth). */

  @Column(name = "depth_", nullable = false)
  public Integer getDepth() {
    return depth;
  }

  /** 深度(Depth). */
  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  /** 排序(OrderBy). */

  @Column(name = "order_by", nullable = false)
  public Integer getOrderBy() {
    return orderBy;
  }

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  /** 描述(Description). */
  @Column(name = "description_")
  public String getDescription() {
    return description;
  }

  /** 描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }

  @Transient
  public List<RoleEntity> getChildren() {
    return children;
  }

  public void setChildren(List<RoleEntity> children) {
    this.children = children;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final RoleEntity other = (RoleEntity) obj;
    if (roleId == null) {
      if (other.roleId != null) {
        return false;
      }
    } else if (!roleId.equals(other.roleId)) {
      return false;
    }
    return true;
  }



}
