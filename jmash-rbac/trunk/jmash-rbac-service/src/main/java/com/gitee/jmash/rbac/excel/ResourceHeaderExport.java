package com.gitee.jmash.rbac.excel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Cell;
import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.CellValueFormat;
import com.gitee.jmash.common.excel.write.HeaderExport;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.rbac.model.ResourcePermResult;
import jmash.protobuf.TableHead;
import jmash.rbac.protobuf.ResourceType;

public class ResourceHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "资源信息表";

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("资源ID", "resourceId", 4 * 1000));
    list.add(HeaderExport.field("父资源ID", "parentId", 4 * 1000));
    list.add(HeaderExport.field("模块编码", "moduleCode", 4 * 1000));
    list.add(HeaderExport.field("资源编码", "resourceCode", 4 * 1000));
    list.add(HeaderExport.field("资源名称", "resourceName", 4 * 1000));
    list.add(HeaderExport.field("资源图标", "icon", 4 * 1000));
    list.add(HeaderExport.dict("资源类型", "resourceType", 4 * 1000, getResourceTypeMap()));
    list.add(HeaderExport.field("目标", "target", 4 * 1000));
    list.add(HeaderExport.field("URL", "url", 4 * 1000));
    list.add(HeaderExport.dict("是否隐藏", "hidden", 4 * 1000, getHiddenMap()));
    list.add(HeaderExport.custom("操作编码", "operCodes", 10 * 1000, new CellValueFormat() {

      @Override
      public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
        ResourcePermResult resource = (ResourcePermResult) rowData;
        if (resource.getPermEntities() != null && !resource.getPermEntities().isEmpty()) {
          String perms = resource.getPermEntities().stream().map(perm -> {
            String[] parts = perm.getPermCode().split(":");
            return parts[parts.length - 1];
          }).collect(Collectors.joining(","));
          cell.setCellValue(perms);
        }
        return true;
      }

    }));
    return list;
  }

  /** 审核状态Map. */
  public static Map<String, String> getHiddenMap() {
    Map<String, String> hiddenMap = new LinkedHashMap<>();
    hiddenMap.put("true", "是");
    hiddenMap.put("false", "否");
    return hiddenMap;
  }

  /** 资源类型Map. */
  public static Map<String, String> getResourceTypeMap() {
    return ProtoEnumUtil.getEnumNameMap(ResourceType.class, 0);
  }

  /**
   * Map HeaderExport
   * 
   * @return
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** 增加表头. */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }
}
