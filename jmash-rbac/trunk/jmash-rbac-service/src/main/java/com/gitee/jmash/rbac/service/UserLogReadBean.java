
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FilePathUtil;
import com.gitee.jmash.rbac.dao.UserLogDao;
import com.gitee.jmash.rbac.entity.UserLogEntity;
import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import com.gitee.jmash.rbac.excel.UserLogHeaderExport;
import com.gitee.jmash.rbac.model.UserLogTotal;
import jakarta.enterprise.inject.Typed;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.executable.ValidateOnExecution;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import jmash.protobuf.TableHead;
import jmash.rbac.protobuf.UserLogExportReq;
import jmash.rbac.protobuf.UserLogReq;
import org.apache.commons.lang3.StringUtils;

/**
 * 安全日志 rbac_user_log读服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(UserLogRead.class)
@Transactional(TxType.SUPPORTS)
@JpaTenantService
@ValidateOnExecution
public class UserLogReadBean implements UserLogRead, JakartaTransaction {

  protected TenantEntityManager tem = new TenantEntityManager();
  
  protected UserLogDao userLogDao = new UserLogDao(this.tem);

  @PersistenceContext(unitName = "ReadRbac")
  public void setEntityManager(EntityManager entityManager) {    
    this.tem.setEntityManager(entityManager, false);
  }
	
  @Override
  @SuppressWarnings("unchecked")
  public <T extends TenantService> T setTenant(String tenant) {
    this.tem.setTenant(tenant);
    return (T) this;
  }
  
  @Override
  public EntityManager getEntityManager() {
    return this.tem.getEntityManager();
  }
  
  @Override
  public void setTenantOnly(String tenant) {
    this.tem.setTenantOnly(tenant);
  }
  
  @Override
  public String getTenant() {
    return  this.tem.getTenant();
  }

  @Override
  public UserLogEntity findById(UserLogPk entityId) {
    return userLogDao.find(entityId);
  }

  @Override
  public DtoPage<UserLogEntity,UserLogTotal> findPageByReq(UserLogReq req) {
    return userLogDao.findPageByReq(req);
  }

  @Override
  public List<UserLogEntity> findListByReq(UserLogReq req) {
    return userLogDao.findListByReq(req);
  }

  @Override
  public String exportUserLog(UserLogExportReq request) throws FileNotFoundException, IOException {
    List<UserLogEntity> list = userLogDao.findListByReq(request.getReq());
    return exportUserLog(list, request.getTitle(), request.getTableHeadsList(), request.getFileName());
  }

  private String exportUserLog(List<UserLogEntity> logList, String title, List<TableHead> tableHeads,
      String fileName) throws FileNotFoundException, IOException {
    ExcelExport excelExport = new ExcelExport();
    // 页签
    excelExport.createSheet(UserLogHeaderExport.TITLE);
    // 增加表头
    UserLogHeaderExport.addHeaderExport(excelExport, tableHeads);
    // 写标题
    title = StringUtils.isBlank(title) ? UserLogHeaderExport.TITLE : title;
    excelExport.writeTitle(title);
    // 写表头
    excelExport.writeHeader();
    // 写数据
    excelExport.writeListData(logList);
    // 增加数据校验
    excelExport.dictDataValidation(logList.size(), 3000);
    // 写入文件
    fileName = StringUtils.isBlank(fileName) ? title : fileName;
    String fileSrc = FilePathUtil.createNewTempFile(fileName + ".xlsx", "excel");
    String realFileSrc = FilePathUtil.realPath(fileSrc,true);
    excelExport.getWb().write(new FileOutputStream(realFileSrc));
    return realFileSrc;
  }

  @Override
  public void close() throws Exception {
    CDI.current().destroy(this);
  }
}
