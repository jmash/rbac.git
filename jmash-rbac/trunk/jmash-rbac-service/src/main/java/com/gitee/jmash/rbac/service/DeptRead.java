package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.DeptEntity;
import com.gitee.jmash.rbac.model.TreeResult;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import jmash.protobuf.TenantReq;
import jmash.rbac.protobuf.DeptExportReq;
import jmash.rbac.protobuf.DeptReq;

/**
 * 组织机构 rbac_dept服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface DeptRead extends TenantService {

  /** 根据主键查询. */
  public DeptEntity findById(@NotNull UUID entityId);

  /** 综合查询. */
  public List<DeptEntity> findListByReq(@NotNull @Valid DeptReq req);

  /** 树结构列表. */
  public List<TreeResult> findTreeList(@NotNull @Valid DeptReq req);

  /** 下载部门导入模板. */
  public String downloadDeptTemplate(TenantReq request)
      throws FileNotFoundException, IOException;

  /** 导出部门. */
  public String exportDept(DeptExportReq request) throws FileNotFoundException, IOException;

  /** 打印部门. */
  public String printDept(DeptExportReq req) throws FileNotFoundException, IOException;
}
