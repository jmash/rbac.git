package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import jmash.rbac.protobuf.OpenAppReq;
import com.gitee.jmash.rbac.model.OpenAppTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

 /**
 * 开发平台应用 rbac_open_app服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface OpenAppRead extends TenantService {

  /** 根据主键查询. */
  public OpenAppEntity findById(@NotNull UUID entityId);

  /** 查询页信息. */
  public DtoPage<OpenAppEntity,OpenAppTotal> findPageByReq(@NotNull @Valid OpenAppReq req);

  /** 综合查询. */
  public List<OpenAppEntity> findListByReq(@NotNull @Valid OpenAppReq req);

  /** 根据appId查询应用相关信息. */
  public OpenAppEntity findByAppId(@NotNull String appId);
  
}
