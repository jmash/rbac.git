package com.gitee.jmash.rbac.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.HeaderExport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.protobuf.TableHead;

public class UserLogHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "安全日志信息表";

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderExport> getHeaderExports() {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.field("日志", "logId", 4 * 1000));
    list.add(HeaderExport.field("表分区", "partition", 4 * 1000));
    list.add(HeaderExport.field("操作类型", "logName", 4 * 1000));
    list.add(HeaderExport.field("操作内容/日志信息", "logMsg", 4 * 1000));
    list.add(HeaderExport.field("环境参数", "envProps", 4 * 1000));
    list.add(HeaderExport.field("设备标识ID", "deviceId", 4 * 1000));
    list.add(HeaderExport.field("用户IP", "userIp", 4 * 1000));
    list.add(HeaderExport.field("代理IP", "proxyIp", 4 * 1000));
    list.add(HeaderExport.field("操作人","createBy",4*1000));
    list.add(HeaderExport.field("操作时间","createTime",4*1000));
    return list;
  }

  /**
   * Map HeaderExport
   * 
   * @return
   */
  public static Map<String, HeaderExport> getHeaderExportMap() {
    Map<String, HeaderExport> map = getHeaderExports().stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** 增加表头. */
  public static void addHeaderExport(ExcelExport excelExport, List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports()) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap();
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }
}
