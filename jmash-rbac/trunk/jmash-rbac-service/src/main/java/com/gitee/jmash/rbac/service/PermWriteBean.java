
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.rbac.dao.RolesPermsDao;
import com.gitee.jmash.rbac.entity.PermEntity;
import com.gitee.jmash.rbac.entity.RolesPermsEntity;
import com.gitee.jmash.rbac.entity.RolesPermsEntity.RolesPermsPk;
import com.gitee.jmash.rbac.mapper.PermMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.PermCreateReq;
import jmash.rbac.protobuf.PermUpdateReq;

/**
 * 权限表 rbac_perm写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(PermWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class PermWriteBean extends PermReadBean implements PermWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  protected RolesPermsDao rolesPermsDao = new RolesPermsDao(this.tem);

  @Override
  @PersistenceContext(unitName = "WriteRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }


  @Override
  public PermEntity insert(PermCreateReq perm) {
    PermEntity entity = PermMapper.INSTANCE.create(perm);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (perm.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(perm.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务(创建人及时间内部处理.)
    permDao.persist(entity);
    return entity;
  }

  @Override
  public PermEntity update(PermUpdateReq req) {
    PermEntity entity = permDao.find(UUIDUtil.fromString(req.getPermId()),req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getPermId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    // 4.执行业务
    permDao.merge(entity);
    return entity;
  }

  @Override
  public PermEntity delete(UUID entityId) {
    //移除角色权限
    rolesPermsDao.removeByPermId(entityId);
    //删除权限
    PermEntity entity = permDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      //移除角色权限
      rolesPermsDao.removeByPermId(entityId);
      //删除权限
      permDao.removeById(entityId);
      i++;
    }
    return i;
  }


  @Override
  public Integer grantPerm(UUID roleId, Set<String> permCodes) {
    int i = 0;
    for (String permCode : permCodes) {
      PermEntity entity = permDao.findByCode(permCode);
      if (null != entity) {
        rolesPermsDao.merge(new RolesPermsEntity(roleId, entity.getPermId()));
        i++;
      }
    }
    return i;
  }

  @Override
  public Integer revokePerm(UUID roleId, Set<String> permCodes) {
    int i = 0;
    for (String permCode : permCodes) {
      PermEntity entity = permDao.findByCode(permCode);
      if (null != entity) {
        //删除权限
        rolesPermsDao.removeById(new RolesPermsPk(roleId, entity.getPermId()));
        i++;
      }
    }
    return i;
  }

}
