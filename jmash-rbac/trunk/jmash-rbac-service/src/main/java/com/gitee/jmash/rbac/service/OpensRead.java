package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.OpensEntity;
import com.gitee.jmash.rbac.entity.OpensEntity.OpensPk;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import jmash.rbac.protobuf.OpensReq;
import jmash.rbac.protobuf.UserOpensReq;

/**
 * 三方登录 rbac_opens服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface OpensRead extends TenantService {

  /** 根据主键查询. */
  public OpensEntity findById(@NotNull OpensPk entityId);

  /** 获取用户登录某appId的unionId. */
  public List<String> findOpensUnionId(UserOpensReq request);

  /** 综合查询. */
  public List<OpensEntity> findListByReq(@NotNull @Valid OpensReq req);
}
