package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.ModuleCreateReq;
import jmash.rbac.protobuf.ModuleMoveKey;
import jmash.rbac.protobuf.ModuleUpdateReq;

 /**
 * 系统模块 rbac_module服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface ModuleWrite extends TenantService {
		
  /** 插入实体. */
  public ModuleEntity insert(@NotNull @Valid ModuleCreateReq  module);

  /** update 实体. */
  public ModuleEntity update(@NotNull @Valid ModuleUpdateReq  module);

  /** 根据主键删除. */
  public ModuleEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set< @NotNull UUID > entityIds);

  /** 移动排序 */
  public boolean moveOrderBy(@NotNull ModuleMoveKey moduleMoveKey);
  
 }
