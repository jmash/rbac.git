package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.RoleEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.RoleCreateReq;
import jmash.rbac.protobuf.RoleMoveKey;
import jmash.rbac.protobuf.RoleUpdateReq;

/**
 * 角色/职务表 rbac_role服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface RoleWrite extends TenantService {

  /** 插入实体. */
  public RoleEntity insert(@NotNull @Valid RoleCreateReq role);

  /** update 实体. */
  public RoleEntity update(@NotNull @Valid RoleUpdateReq role);

  /** 根据主键删除. */
  public RoleEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);

  /** 排序. */
  public boolean moveOrderBy(@NotNull RoleMoveKey roleMoveKey);

  /** 给用户分配角色. */
  public Integer assignUser(@NotNull UUID userId, @Valid Set<String> roleCodes);
  
  /** 取消用户分配角色. */
  public Integer deassignUser(@NotNull UUID userId, @Valid Set<String> roleCodes);

}
