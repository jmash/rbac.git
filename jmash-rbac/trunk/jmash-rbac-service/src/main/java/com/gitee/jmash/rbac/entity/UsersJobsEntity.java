
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import com.gitee.jmash.rbac.entity.UsersJobsEntity.UsersJobsPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import org.hibernate.annotations.TenantId;

/**
 * 本代码为自动生成工具生成 用户部门职位表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_users_jobs")
@IdClass(UsersJobsPk.class)
public class UsersJobsEntity implements Serializable, JpaTenantIdentifier {

  private static final long serialVersionUID = 1L;

  /** 租户. */
  private TenantIdentifier tenantId;
  /** 账号. */
  private UUID userId;
  /** 部门ID. */
  private UUID deptId;
  /** 岗位ID. */
  private UUID jobId;
  /** 排序. */
  private Integer orderBy = 1;
  /** 开始时间. */
  private LocalDateTime startTime = LocalDateTime.now();
  /** 结束时间. */
  private LocalDateTime endTime;
  /** 状态. */
  private Boolean status = true;

  /**
   * 默认构造函数.
   */
  public UsersJobsEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UsersJobsPk getEntityPk() {
    UsersJobsPk pk = new UsersJobsPk();
    pk.setDeptId(this.deptId);
    pk.setJobId(this.jobId);
    pk.setUserId(this.userId);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(UsersJobsPk pk) {
    this.deptId = pk.getDeptId();
    this.jobId = pk.getJobId();
    this.userId = pk.getUserId();
  }

  @TenantId
  @Column(name = "tenant_id")
  public TenantIdentifier getTenantId() {
    return tenantId;
  }

  public void setTenantId(TenantIdentifier tenantId) {
    this.tenantId = tenantId;
  }

  /** 账号(UserId). */
  @Id
  @Column(name = "user_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getUserId() {
    return userId;
  }

  /** 账号(UserId). */
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /** 部门ID(DeptId). */
  @Id
  @Column(name = "dept_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getDeptId() {
    return deptId;
  }

  /** 部门ID(DeptId). */
  public void setDeptId(UUID deptId) {
    this.deptId = deptId;
  }

  /** 岗位ID(JobId). */
  @Id
  @Column(name = "job_id", columnDefinition = "BINARY(16)")
  public UUID getJobId() {
    return jobId;
  }

  /** 岗位ID(JobId). */
  public void setJobId(UUID jobId) {
    this.jobId = jobId;
  }

  /** 排序(OrderBy). */

  @Column(name = "order_by", nullable = false)
  public Integer getOrderBy() {
    return orderBy;
  }

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  /** 开始时间(StartTime). */

  @Column(name = "start_time", nullable = false)
  public LocalDateTime getStartTime() {
    return startTime;
  }

  /** 开始时间(StartTime). */
  public void setStartTime(LocalDateTime startTime) {
    this.startTime = startTime;
  }

  /** 结束时间(EndTime). */
  @Column(name = "end_time")
  public LocalDateTime getEndTime() {
    return endTime;
  }

  /** 结束时间(EndTime). */
  public void setEndTime(LocalDateTime endTime) {
    this.endTime = endTime;
  }

  /** 状态(Status). */

  @Column(name = "status_", nullable = false)
  public Boolean getStatus() {
    return status;
  }

  /** 状态(Status). */
  public void setStatus(Boolean status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final UsersJobsEntity other = (UsersJobsEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * UsersJobsPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class UsersJobsPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 部门ID. */
    @NotNull
    private UUID deptId;
    /** 岗位ID. */
    private UUID jobId;
    /** 账号. */
    @NotNull
    private UUID userId;

    /** 部门ID(DeptId). */
    @Column(name = "dept_id", nullable = false)
    public UUID getDeptId() {
      return deptId;
    }

    /** 部门ID(DeptId). */
    public void setDeptId(UUID deptId) {
      this.deptId = deptId;
    }

    /** 岗位ID(JobId). */
    @Column(name = "job_id")
    public UUID getJobId() {
      return jobId;
    }

    /** 岗位ID(JobId). */
    public void setJobId(UUID jobId) {
      this.jobId = jobId;
    }

    /** 账号(UserId). */
    @Column(name = "user_id", nullable = false)
    public UUID getUserId() {
      return userId;
    }

    /** 账号(UserId). */
    public void setUserId(UUID userId) {
      this.userId = userId;
    }

    @Override
    public int hashCode() {
      return Objects.hash(deptId, jobId, userId);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      UsersJobsPk other = (UsersJobsPk) obj;
      return Objects.equals(deptId, other.deptId) && Objects.equals(jobId, other.jobId)
          && Objects.equals(userId, other.userId);
    }
  }
}
