
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.jpa.entity.BasicEntity;
import com.gitee.jmash.core.orm.jpa.entity.TreeEntity;
import com.gitee.jmash.core.orm.tenant.JpaTenantIdentifier;
import com.gitee.jmash.core.orm.tenant.TenantIdentifier;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.Objects;
import java.util.UUID;
import org.hibernate.annotations.TenantId;


/**
 * 本代码为自动生成工具生成 组织机构表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_dept")
public class DeptEntity extends BasicEntity<UUID> implements TreeEntity<UUID>, JpaTenantIdentifier {

  private static final long serialVersionUID = 1L;

  /** 租户. */
  private TenantIdentifier tenantId;
  /** 部门ID. */
  private UUID deptId = UUID.randomUUID();
  /** 部门编码. */
  private String deptCode;
  /** 部门名称. */
  private String deptName;
  /** 部门类型. */
  private String deptType;
  /** 父部门. */
  private UUID parentId = UUIDUtil.emptyUUID();
  /** 部门描述. */
  private String description;
  /** 深度. */
  private Integer depth = 1;
  /** 排序. */
  private Integer orderBy = 1;
  /** 状态. */
  private Boolean status = true;
  /** 开放访问. */
  private Boolean isOpen = false;


  /**
   * 默认构造函数.
   */
  public DeptEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.deptId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.deptId = pk;
  }

  /** 部门ID(DeptId). */
  @Id
  @Column(name = "dept_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getDeptId() {
    return deptId;
  }

  /** 部门ID(DeptId). */
  public void setDeptId(UUID deptId) {
    this.deptId = deptId;
  }

  @TenantId
  @Column(name = "tenant_id")
  public TenantIdentifier getTenantId() {
    return tenantId;
  }

  public void setTenantId(TenantIdentifier tenantId) {
    this.tenantId = tenantId;
  }

  /** 部门编码(DeptCode). */
  @Column(name = "dept_code", nullable = false)
  public String getDeptCode() {
    return deptCode;
  }

  /** 部门编码(DeptCode). */
  public void setDeptCode(String deptCode) {
    this.deptCode = deptCode;
  }

  /** 部门名称(DeptName). */

  @Column(name = "dept_name", nullable = false)
  public String getDeptName() {
    return deptName;
  }

  /** 部门名称(DeptName). */
  public void setDeptName(String deptName) {
    this.deptName = deptName;
  }

  /** 部门类型(DeptType). */
  @Column(name = "dept_type", nullable = false)
  public String getDeptType() {
    return deptType;
  }

  /** 部门类型(DeptType). */
  public void setDeptType(String deptType) {
    this.deptType = deptType;
  }

  /** 父部门(ParentId). */
  @Column(name = "parent_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getParentId() {
    return parentId;
  }

  /** 父部门(ParentId). */
  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  /** 部门描述(Description). */
  @Column(name = "description_", nullable = false)
  public String getDescription() {
    return description;
  }

  /** 部门描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }

  /** 深度(Depth). */
  @Column(name = "depth_", nullable = false)
  public Integer getDepth() {
    return depth;
  }

  /** 深度(Depth). */
  public void setDepth(Integer depth) {
    this.depth = depth;
  }

  /** 排序(OrderBy). */
  @Column(name = "order_by", nullable = false)
  public Integer getOrderBy() {
    return orderBy;
  }

  /** 排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  /** 状态(Status). */
  @Column(name = "status_", nullable = false)
  public Boolean getStatus() {
    return status;
  }

  /** 状态(Status). */
  public void setStatus(Boolean status) {
    this.status = status;
  }

  /** 开放访问(Open). */
  @Column(name = "is_open", nullable = false)
  public Boolean getIsOpen() {
    return isOpen;
  }

  /** 开放访问(Open). */
  public void setIsOpen(Boolean isOpen) {
    this.isOpen = isOpen;
  }

  @Override
  public int hashCode() {
    return Objects.hash(deptId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DeptEntity other = (DeptEntity) obj;
    return Objects.equals(deptId, other.deptId);
  }

}
