
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.CreateEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;


 /**
 * 本代码为自动生成工具生成 操作日志表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_log")
@IdClass(LogPk.class)
public class LogEntity  extends CreateEntity<LogPk>  {

  private static final long serialVersionUID = 1L;
  
  /** 日志. */
  private UUID logId= UUID.randomUUID()  ;
  /** 表分区. */
  private Integer partition ;
  /** 日志名称. */
  private String logName ;
  /** 日志级别. */
  private String logLevel ;
  /** 日志信息. */
  private String logMsg ;
  /** 环境参数. */
  private String envProps ;
  /** 日志内容. */
  private String logContent ;
  /** 用户IP. */
  private String userIp ;
  /** 代理IP. */
  private String proxyIp ;

  /**
  * 默认构造函数.
  */
  public LogEntity(){
    super();
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public LogPk  getEntityPk(){
    LogPk pk=new LogPk();
    pk.setLogId(this.logId);
    pk.setPartition(this.partition);
    return pk;
  }
  /** 实体主键. */
  public void setEntityPk(LogPk  pk){
    this.logId=pk.getLogId();
    this.partition=pk.getPartition();
  }
  
   /** 日志(LogId). */
  @Id
  @Column(name="log_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getLogId() {
    return logId;
  }
  /** 日志(LogId). */
  public void setLogId(UUID logId) {
    this.logId = logId;
  }
   /** 表分区(Partition). */
  @Id

  @Column(name="partition_",nullable = false)
  public Integer getPartition() {
    return partition;
  }
  /** 表分区(Partition). */
  public void setPartition(Integer partition) {
    this.partition = partition;
  }
   /** 日志名称(LogName). */

  @Column(name="log_name",nullable = false)
  public String getLogName() {
    return logName;
  }
  /** 日志名称(LogName). */
  public void setLogName(String logName) {
    this.logName = logName;
  }
   /** 日志级别(LogLevel). */

  @Column(name="log_level",nullable = false)
  public String getLogLevel() {
    return logLevel;
  }
  /** 日志级别(LogLevel). */
  public void setLogLevel(String logLevel) {
    this.logLevel = logLevel;
  }
   /** 日志信息(LogMsg). */

  @Column(name="log_msg",nullable = false)
  public String getLogMsg() {
    return logMsg;
  }
  /** 日志信息(LogMsg). */
  public void setLogMsg(String logMsg) {
    this.logMsg = logMsg;
  }
   /** 环境参数(EnvProps). */
  @Column(name="env_props")
  @Lob
  public String getEnvProps() {
    return envProps;
  }
  /** 环境参数(EnvProps). */
  public void setEnvProps(String envProps) {
    this.envProps = envProps;
  }
   /** 日志内容(LogContent). */
  @Column(name="log_content")
  @Lob
  public String getLogContent() {
    return logContent;
  }
  /** 日志内容(LogContent). */
  public void setLogContent(String logContent) {
    this.logContent = logContent;
  }
   /** 用户IP(UserIp). */

  @Column(name="user_ip",nullable = false)
  public String getUserIp() {
    return userIp;
  }
  /** 用户IP(UserIp). */
  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }
   /** 代理IP(ProxyIp). */

  @Column(name="proxy_ip",nullable = false)
  public String getProxyIp() {
    return proxyIp;
  }
  /** 代理IP(ProxyIp). */
  public void setProxyIp(String proxyIp) {
    this.proxyIp = proxyIp;
  }
	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final LogEntity other = (LogEntity) obj;
	if (getEntityPk() == null) {
		if (other.getEntityPk() != null){
			return false;
		}
	} else if (!getEntityPk().equals(other.getEntityPk())){
		return false;
	}
    return true;
  }
  
  
  /**
   * LogPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class LogPk implements Serializable{
  
    private static final long serialVersionUID = 1L;
    
    /** 日志. */
    @NotNull
    private UUID logId;
    /** 表分区. */
    @NotNull
    private Integer partition;
    
    /** 日志(LogId). */
	@Column(name="log_id",nullable = false)
    public UUID getLogId() {
      return logId;
    }
    
    /** 日志(LogId). */
    public void setLogId(UUID logId) {
      this.logId = logId;
    }
    /** 表分区(Partition). */
	@Column(name="partition_",nullable = false)
    public Integer getPartition() {
      return partition;
    }
    
    /** 表分区(Partition). */
    public void setPartition(Integer partition) {
      this.partition = partition;
    }
      
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result
          + ((logId == null) ? 0 : logId.hashCode());
      result = prime * result
          + ((partition == null) ? 0 : partition.hashCode());
      return result;
    }
  
    @Override
    public boolean equals(Object obj) {
      if (this == obj){
        return true;
      }
      if (obj == null){
        return false;
      }
      if (getClass() != obj.getClass()){
        return false;
      }
      final LogPk other = (LogPk) obj;
      
      if (logId == null) {
        if (other.logId != null){
          return false;
        }
      } else if (!logId.equals(other.logId)){
        return false;
      }
      if (partition == null) {
        if (other.partition != null){
          return false;
        }
      } else if (!partition.equals(other.partition)){
        return false;
      }
      return true;
    }
  }

}
