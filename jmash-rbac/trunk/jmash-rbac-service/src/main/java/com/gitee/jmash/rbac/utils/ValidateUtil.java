package com.gitee.jmash.rbac.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtil {

  /** 验证电子邮件. */
  public static boolean validateEmail(String email) {
    Pattern pattern = Pattern.compile(".+@.+\\..+");
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }

  /** 验证手机号. */
  public static boolean validatePhone(String phone) {
    String regex = "^1[3456789][0-9]{9}$"; // 正则表达式，用于匹配中国的手机号码
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(phone);
    return matcher.matches();
  }

}
