package com.gitee.jmash.rbac.excel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import com.gitee.jmash.common.excel.read.CellValueReader;
import com.gitee.jmash.common.excel.read.HeaderField;
import com.gitee.jmash.common.excel.read.ReaderError;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.rbac.dao.ModuleDao;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.service.ModuleWrite;
import jmash.rbac.protobuf.ModuleCreateReq;
import jmash.rbac.protobuf.ModuleReq;
import jmash.rbac.protobuf.ResourceImportReq;
import jmash.rbac.protobuf.ResourceType;

public class ResourceHeaderImport {

  /**
   * 规则Header
   * 
   * @return
   */
  public static List<HeaderField> getHeaderImports(CellValueReader moduleCellReader) {
    List<HeaderField> list = new ArrayList<HeaderField>();
    list.add(HeaderField.field("资源ID", "resourceId", true));
    list.add(HeaderField.field("父资源ID", "parentId", true));
    list.add(HeaderField.field("模块编码", "moduleId", true, moduleCellReader));
    list.add(HeaderField.field("资源编码", "resourceCode", true));
    list.add(HeaderField.field("资源名称", "resourceName", true));
    list.add(HeaderField.field("资源图标", "icon", true));
    list.add(HeaderField.dict("资源类型", "resourceType", true, getResourceTypeMap()));
    list.add(HeaderField.field("目标", "target", true));
    list.add(HeaderField.field("URL", "url", true));
    list.add(HeaderField.dict("是否隐藏", "hidden", true, getHiddenMap()));
    list.add(HeaderField.field("操作编码", "operCodes", true, new CellValueReader() {
      @Override
      public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
          List<ReaderError> errors) {
        String operCodes = cell.getStringCellValue();
        if (StringUtils.isBlank(operCodes)) {
          return List.of();
        }
        return List.of(operCodes.split(","));
      }
    }));
    return list;
  }
  
  //
  
  
  /** 审核状态Map. */
  public static  CellValueReader moduleReader(ResourceImportReq req,ModuleWrite moduleWrite,ModuleDao moduleDao) {
    Map<String, String> codeIdMap = moduleDao.findMapCodeIdByReq(ModuleReq.getDefaultInstance());
    CellValueReader moduleCellReader =  new CellValueReader() {
      @Override
      public Object cellValueRead(String header, Cell cell, FormulaEvaluator eval,
          List<ReaderError> errors) {        
        String moduleCode = cell.getStringCellValue();
        if (StringUtils.isBlank(moduleCode)) {
          return UUIDUtil.emptyUUIDStr();
        }
        if (codeIdMap.containsKey(moduleCode)) {
          return codeIdMap.get(moduleCode);
        }
        ModuleCreateReq moduleReq = ModuleCreateReq.newBuilder().setModuleCode(moduleCode).setTenant(req.getTenant())
        .setRequestId(UUID.randomUUID().toString()).build();
        ModuleEntity entity=moduleWrite.insert(moduleReq);
        codeIdMap.put(moduleCode,entity.getModuleId().toString());
        return entity.getModuleId().toString();
      }
    };
    return moduleCellReader;
  }
  
  

  /** 审核状态Map. */
  public static Map<String, String> getHiddenMap() {
    Map<String, String> hiddenMap = new LinkedHashMap<>();
    hiddenMap.put("是", "true");
    hiddenMap.put("否", "false");
    return hiddenMap;
  }

  /** 资源类型Map. */
  public static Map<String, String> getResourceTypeMap() {
    return ProtoEnumUtil.getEnumCodeMap(ResourceType.class, 0);
  }
}
