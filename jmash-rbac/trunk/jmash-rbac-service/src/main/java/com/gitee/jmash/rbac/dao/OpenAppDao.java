
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import com.gitee.jmash.rbac.model.OpenAppTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.rbac.protobuf.OpenAppReq;
import org.apache.commons.lang3.StringUtils;

/**
 * OpenApp实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class  OpenAppDao extends BaseDao<OpenAppEntity, UUID> {

  public OpenAppDao() {
    super();
  }

  public OpenAppDao(TenantEntityManager tem) {
    super(tem);
  }
	
  
  /**
   * 查询记录数.
   */
  public Integer findCount(OpenAppReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select count(s) ");
    Object obj = this.findSingleResultByParams(query, sqlBuilder.getParams());
    return (obj == null) ? 0 : Integer.valueOf(obj.toString());
  }
  
  /**
   * 综合查询.
   */
  public List<OpenAppEntity> findListByReq(OpenAppReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<OpenAppEntity, OpenAppTotal> findPageByReq(OpenAppReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        OpenAppTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(OpenAppReq req) {
    StringBuilder sql = new StringBuilder(" from OpenAppEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();
	
	

	

	String orderSql = "";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  /**
   * 根据appId查询应用相关信息
   * @param appId appId
   * @return 应用信息
   */
  public OpenAppEntity findByAppId(String appId){
    String sql = "select s from OpenAppEntity s where s.appId = ?1";
    return this.findSingle(sql,appId);
  }
  

}
