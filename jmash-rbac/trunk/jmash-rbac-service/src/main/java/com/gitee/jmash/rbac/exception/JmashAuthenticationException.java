
package com.gitee.jmash.rbac.exception;

import jakarta.validation.ValidationException;
import java.util.UUID;

/**
 * 认证异常.
 *
 * @author CGD
 *
 */
public class JmashAuthenticationException extends ValidationException {

  private static final long serialVersionUID = 1L;

  private int code = 0;

  private UUID accountId;

  public JmashAuthenticationException() {
    super();
  }

  public JmashAuthenticationException(int code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }

  public JmashAuthenticationException(int code, String message) {
    super(message);
    this.code = code;
  }

  /** 用户ID. */
  public JmashAuthenticationException(UUID accountId, int code, String message) {
    super(message);
    this.code = code;
    this.accountId = accountId;
  }

  public JmashAuthenticationException(int code, Throwable cause) {
    super(cause);
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

}
