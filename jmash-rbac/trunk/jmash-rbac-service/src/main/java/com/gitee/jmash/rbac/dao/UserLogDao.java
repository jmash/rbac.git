
package com.gitee.jmash.rbac.dao;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.UserLogEntity;
import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import com.gitee.jmash.rbac.model.UserLogTotal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmash.rbac.protobuf.UserLogReq;
import org.apache.commons.lang3.StringUtils;

/**
 * UserLog实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class  UserLogDao extends BaseDao<UserLogEntity, UserLogPk> {

  public UserLogDao() {
    super();
  }

  public UserLogDao(TenantEntityManager tem) {
    super(tem);
  }
	
  /**
   * 综合查询.
   */
  public List<UserLogEntity> findListByReq(UserLogReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<UserLogEntity, UserLogTotal> findPageByReq(UserLogReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        UserLogTotal.class, sqlBuilder.getParams());
  }

  /** 批量删除N年前数据日志. */
  public int batchDelete (LocalDateTime dateTime) {
    String sql = "delete from UserLogEntity s where s.createTime <= ?1 ";
    return this.executeUpdate(sql,dateTime);
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(UserLogReq req) {
    StringBuilder sql = new StringBuilder(" from UserLogEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();
    
    if(req.getPartition()>0) {
      sql.append(" and s.partition = :partition ");
      params.put("partition", req.getPartition());
    }
    
    if (!TypeUtil.isEmpty(req.getStartCreateTime())) {
      sql.append(" and s.createTime >= :startCreateTime ");
      params.put("startCreateTime", TypeUtil.INSTANCE.toLocalDateTime(req.getStartCreateTime()));
    }
    
    if (!TypeUtil.isEmpty(req.getEndCreateTime())) {
      sql.append(" and s.createTime <= :endCreateTime ");
      params.put("endCreateTime", TypeUtil.INSTANCE.toLocalDateTime(req.getEndCreateTime()));
    }

    if(StringUtils.isNotBlank(req.getLikeLogName())) {
      sql.append(" and s.logName like :likeLogName ");
      params.put("likeLogName","%"+req.getLikeLogName()+"%");
    }

    if (!TypeUtil.isEmpty(req.getCreateBy())) {
      sql.append(" and s.createBy = :createBy ");
      params.put("createBy", req.getCreateBy());
    }

    String orderSql = " order by s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

}
