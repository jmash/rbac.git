
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.rbac.entity.OpenAppEntity;
import com.gitee.jmash.rbac.model.OpenAppTotal;
import com.xyvcard.weixin.wechat.model.MiniLoginResp;
import com.xyvcard.weixin.wechat.model.PhoneInfo;
import java.util.List;
import jmash.rbac.protobuf.OpenAppCreateReq;
import jmash.rbac.protobuf.OpenAppModel;
import jmash.rbac.protobuf.OpenAppPage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import xyvcard.wechat.protobuf.MiniAppPhoneResp;
import xyvcard.wechat.protobuf.MiniAppUserOpenIdResp;

/**
 * OpenAppMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface OpenAppMapper extends BeanMapper, ProtoMapper {

  OpenAppMapper INSTANCE = Mappers.getMapper(OpenAppMapper.class);


  List<OpenAppModel> listOpenApp(List<OpenAppEntity> list);

  OpenAppPage pageOpenApp(DtoPage<OpenAppEntity, OpenAppTotal> page);

  OpenAppModel model(OpenAppEntity entity);

  OpenAppEntity create(OpenAppCreateReq req);

  OpenAppEntity clone(OpenAppEntity entity);

  /** 用户OpenId. */
  MiniLoginResp userOpenId(MiniAppUserOpenIdResp loginResp);

  /** 用户PhoneNumber. */
  PhoneInfo userPhoneNumber(MiniAppPhoneResp loginResp) ;
 
}
