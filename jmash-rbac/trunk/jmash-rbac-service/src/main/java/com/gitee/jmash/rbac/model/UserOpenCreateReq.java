package com.gitee.jmash.rbac.model;

import jakarta.validation.constraints.NotNull;
import jmash.rbac.protobuf.OpensType;

/**
 * 通过OpenID创建用户请求
 */
public class UserOpenCreateReq {
  

  //租户
  @NotNull
  private String tenant;
  //用户目录默认后台用户jmash,前台用户默认user
  @NotNull
  private String directoryId = "user";
  //手机号
  @NotNull
  private String mobilePhone;
  //地区
  private String countryCode;
  //小程序appid
  @NotNull
  private String appid;
  //类型
  @NotNull
  private OpensType openType;
  //openid
  @NotNull
  private String openid;
  //unionid
  private String unionid;
  //昵称
  private String nickName;
  
  public String getTenant() {
    return tenant;
  }
  public void setTenant(String tenant) {
    this.tenant = tenant;
  }  
  public String getDirectoryId() {
    return directoryId;
  }
  public void setDirectoryId(String directoryId) {
    this.directoryId = directoryId;
  }
  public String getMobilePhone() {
    return mobilePhone;
  }
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }
  public String getCountryCode() {
    return countryCode;
  }
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  public String getAppid() {
    return appid;
  }
  public void setAppid(String appid) {
    this.appid = appid;
  }  
  public OpensType getOpenType() {
    return openType;
  }
  public void setOpenType(OpensType openType) {
    this.openType = openType;
  }
  public String getOpenid() {
    return openid;
  }
  public void setOpenid(String openid) {
    this.openid = openid;
  }
  public String getUnionid() {
    return unionid;
  }
  public void setUnionid(String unionid) {
    this.unionid = unionid;
  }
  public String getNickName() {
    return nickName;
  }
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
}
