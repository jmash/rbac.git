
package com.gitee.jmash.rbac.dao;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.core.orm.jpa.TreeBaseDao;
import com.gitee.jmash.rbac.entity.DeptEntity;
import com.gitee.jmash.rbac.model.TreeResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import jmash.rbac.protobuf.DeptReq;
import org.apache.commons.lang3.StringUtils;

/**
 * Dept实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class DeptDao extends TreeBaseDao<DeptEntity, UUID> {

  public DeptDao() {
    super();
  }

  public DeptDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 综合查询.
   */
  public List<DeptEntity> findListByReq(DeptReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  public List<TreeResult> findTreeListByReq(DeptReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder
        .getQuerySql("select s.deptId as value,s.deptName as label,s.parentId as parentId ");
    return this.findDtoListByParams(query, TreeResult.class, sqlBuilder.getParams());
  }

  /**
   * Create SQL By Req .
   */
  public SqlBuilder createSql(DeptReq req) {
    StringBuilder sql = new StringBuilder(" from DeptEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (req.getHasStatus()) {
      sql.append(" and s.status = :status ");
      params.put("status", req.getStatus());
    }

    if (StringUtils.isNotBlank(req.getLikeDeptName())&&StringUtils.isEmpty(req.getParentId())) {
      sql.append(" and s.deptName like :likeDeptName ");
      params.put("likeDeptName", "%" + req.getLikeDeptName() + "%");
    }

    if (!TypeUtil.isEmpty(req.getCreateBy())) {
      sql.append(" and s.createBy = :createBy ");
      params.put("createBy", req.getCreateBy());
    }

    if (StringUtils.isNotBlank(req.getDeptType())) {
      sql.append(" and s.deptType = :deptType ");
      params.put("deptType", req.getDeptType());
    }

    if (req.getHasOpen()) {
      sql.append(" and s.isOpen = :isOpen ");
      params.put("isOpen", req.getIsOpen());
    }

    String orderSql = " order by s.depth,s.orderBy";

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

  public List<DeptEntity> findListByDeptId(List<UUID> deptIds) {
    StringBuilder sql = new StringBuilder("select s from DeptEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();
//    if (this.hasTenantIdentifier()) {
//      sql.append(" and s.tenantId = :tenantId ");
//      params.put("tenantId", this.getTenantIdentifier());
//    }

    if (deptIds.size() > 0) {
      sql.append(" and s.deptId in (:deptId)");
      params.put("deptId", deptIds);
    }

    String orderSql = " order by s.deptId ";
    sql.append(orderSql);
    return this.findListByParams(sql.toString(), params);
  }

  /**
   * 部门编码重复检查.
   *
   * @param deptId 部门ID ,新增ID传null.
   * @param deptCode 部门编码
   * @return 是否重复
   */
  public boolean deptCodeExist(UUID deptId, String deptCode) {
    Map<String, Object> params = new HashMap<>();
    StringBuilder sql =
        new StringBuilder("select count(s) from DeptEntity s where s.deptCode = :deptCode ");
    params.put("deptCode", deptCode);
    if (deptId != null) {
      sql.append(" and s.deptId <> :deptId ");
      params.put("deptId", deptId);
    }
//    if (this.hasTenantIdentifier()) {
//      sql.append(" and s.tenantId = :tenantId ");
//      params.put("tenantId", this.getTenantIdentifier());
//    }
    int count = Integer.parseInt(this.findSingleResultByParams(sql.toString(), params).toString());
    return count > 0;
  }
}
