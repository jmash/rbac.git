package com.gitee.jmash.rbac.excel;

import com.gitee.jmash.common.excel.ExcelExport;
import com.gitee.jmash.common.excel.write.CellValueFormat;
import com.gitee.jmash.common.excel.write.HeaderExport;
import com.gitee.jmash.core.lib.ProtoEnumUtil;
import com.gitee.jmash.rbac.entity.UserEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import jmash.protobuf.Entry;
import jmash.protobuf.Gender;
import jmash.protobuf.TableHead;
import jmash.rbac.protobuf.UserStatus;
import org.apache.poi.ss.usermodel.Cell;

// 用户导出.
public class UserHeaderExport {

  /** 默认表头. */
  public static final String TITLE = "用户信息表";

  /** 规则Header. */
  public static List<HeaderExport> getHeaderExports(boolean isTemplate) {
    List<HeaderExport> list = new ArrayList<HeaderExport>();
    list.add(HeaderExport.serial("序号"));
    list.add(HeaderExport.field("用户ID", "userId", 6 * 1000));
    list.add(HeaderExport.field("目录ID", "directoryId", 3 * 1000));
    list.add(HeaderExport.field("用户名", "loginName", 3 * 1000));
    list.add(HeaderExport.field("手机号", "mobilePhone", 3 * 1000));
    list.add(HeaderExport.field("电子邮件", "email", 4 * 1000));
    list.add(HeaderExport.field("姓名", "realName", 4 * 1000));
    list.add(HeaderExport.field("昵称", "nickName", 3 * 1000));
    list.add(HeaderExport.field("出生日期", "birthDate", 3 * 1000));
    list.add(HeaderExport.dict("性别", "gender", 2 * 1000, getGenderMap()));
    list.add(HeaderExport.dict("审核状态", "approved", 3 * 1000, getApprovedMap()));
    list.add(HeaderExport.dict("用户状态", "status", 3 * 1000, getUserStatusMap()));
    //非模板.
    if (!isTemplate) {
      list.add(HeaderExport.custom("被锁时间", "lastLockoutTime", 3 * 1000, new CellValueFormat() {
        @Override
        public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
          UserEntity user = (UserEntity) rowData;
          if (UserStatus.locked.equals(user.getStatus())) {
            this.setCellObject(cell, user.getLastLockoutTime(), header);
          }
          return true;
        }
      }));
      list.add(HeaderExport.custom("上次登录时间", "lastLoginTime", 5 * 1000, new CellValueFormat() {
        @Override
        public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
          UserEntity user = (UserEntity) rowData;
          if (user.getLastLoginTime().getYear() > 1970) {
            this.setCellObject(cell, user.getLastLoginTime(), header);
          }
          return true;
        }
      }));
      list.add(HeaderExport.custom("删除时间", "deleteTime", 3 * 1000, new CellValueFormat() {
        @Override
        public boolean setCellValue(Cell cell, HeaderExport header, int rowNum, Object rowData) {
          UserEntity user = (UserEntity) rowData;
          if (user.getDeleted()) {
            this.setCellObject(cell, user.getDeleteTime(), header);
          }
          return true;
        }
      }));
    }
    return list;
  }



  /** 增加表头. */
  public static void addHeaderExport(boolean isTemplate, ExcelExport excelExport,
      List<TableHead> tableHeads) {
    if (tableHeads.isEmpty()) {
      for (HeaderExport header : getHeaderExports(isTemplate)) {
        excelExport.addHeader(header);
      }
    } else {
      Map<String, HeaderExport> map = getHeaderExportMap(isTemplate);
      for (TableHead tableHead : tableHeads) {
        if (map.containsKey(tableHead.getProp())) {
          excelExport.addHeader(map.get(tableHead.getProp()));
        }
      }
    }
  }

  /** Map HeaderExport. */
  public static Map<String, HeaderExport> getHeaderExportMap(boolean isTemplate) {
    Map<String, HeaderExport> map = getHeaderExports(isTemplate).stream()
        .collect(Collectors.toMap(HeaderExport::getField, Function.identity()));
    return map;
  }

  /** TableHead List. */
  public static List<TableHead> getTableHeadList() {
    List<TableHead> tableHeads = getHeaderExports(false).stream()
        .map(
            obj -> TableHead.newBuilder().setLabel(obj.getHeader()).setProp(obj.getField()).build())
        .collect(Collectors.toList());
    return tableHeads;
  }


  /** 审核状态Map. */
  public static Map<String, String> getApprovedMap() {
    return UserHeaderExport.getApproved().stream()
        .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
  }

  /** 审核状态列表. */
  public static List<Entry> getApproved() {
    List<Entry> list = new ArrayList<>();
    list.add(Entry.newBuilder().setKey("true").setValue("已审核").build());
    list.add(Entry.newBuilder().setKey("false").setValue("未审核").build());
    return list;
  }

  /** 性别Map. */
  public static Map<String, String> getGenderMap() {
    return ProtoEnumUtil.getEnumNameMap(Gender.class, 0);
  }

  /** 用户状态Map. */
  public static Map<String, String> getUserStatusMap() {
    return ProtoEnumUtil.getEnumNameMap(UserStatus.class, 0);
  }

}
