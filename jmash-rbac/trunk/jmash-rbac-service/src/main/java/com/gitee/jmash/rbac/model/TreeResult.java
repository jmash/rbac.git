package com.gitee.jmash.rbac.model;

import jakarta.persistence.Column;
import java.util.UUID;

public class TreeResult {
  //ID
  private UUID value;
  //名称
  private String label;

  //父级ID
  private UUID parentId;

  private int orderBy;

  public UUID getValue() {
    return value;
  }

  public void setValue(UUID value) {
    this.value = value;
  }
  @Column(name="labeL")
  public String  getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public UUID getParentId() {
    return parentId;
  }

  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  public int getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(int orderBy) {
    this.orderBy = orderBy;
  }
}
