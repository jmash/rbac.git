
package com.gitee.jmash.rbac.dao;

import com.crenjoy.proto.utils.TypeUtil;
import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.LogEntity;
import com.gitee.jmash.rbac.entity.LogEntity.LogPk;
import com.gitee.jmash.rbac.model.LogTotal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmash.rbac.protobuf.LogReq;
import org.apache.commons.lang3.StringUtils;

/**
 * Log实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class  LogDao extends BaseDao<LogEntity, LogPk> {

  public LogDao() {
    super();
  }

  public LogDao(TenantEntityManager tem) {
    super(tem);
  }
	
  /**
   * 综合查询.
   */
  public List<LogEntity> findListByReq(LogReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<LogEntity, LogTotal> findPageByReq(LogReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        LogTotal.class, sqlBuilder.getParams());
  }

  /** 批量删除指定日期之前数据日志. */
  public int batchDelete (LocalDateTime dateTime) {
    String sql = "delete from LogEntity s where s.createTime <= ?1";
    return this.executeUpdate(sql,dateTime);
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(LogReq req) {
    StringBuilder sql = new StringBuilder(" from LogEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if(req.getPartition()>0) {
      sql.append(" and s.partition = :partition ");
      params.put("partition", req.getPartition());
    }
    
    if (!TypeUtil.isEmpty(req.getStartCreateTime())) {
      sql.append(" and s.createTime >= :startCreateTime ");
      params.put("startCreateTime", TypeUtil.INSTANCE.toLocalDateTime(req.getStartCreateTime()));
    }
    if (!TypeUtil.isEmpty(req.getEndCreateTime())) {
      sql.append(" and s.createTime <= :endCreateTime ");
      params.put("endCreateTime", TypeUtil.INSTANCE.toLocalDateTime(req.getEndCreateTime()));
    }
    
    if(StringUtils.isNotBlank(req.getLogLevel())) {
      sql.append(" and s.logLevel = :logLevel ");
      params.put("logLevel",req.getLogLevel());
    }
    
    if(StringUtils.isNotBlank(req.getLikeLogName())) {
      sql.append(" and s.logName like :likeLogName ");
      params.put("likeLogName","%"+req.getLikeLogName()+"%");
    }

    if(StringUtils.isNotBlank(req.getLikeLogMsg())) {
      sql.append(" and s.logMsg like :likeLogMsg ");
      params.put("likeLogMsg","%"+req.getLikeLogMsg()+"%");
    }

    if (!TypeUtil.isEmpty(req.getCreateBy())) {
      sql.append(" and s.createBy = :createBy ");
      params.put("createBy", req.getCreateBy());
    }

    String orderSql = " order by s.createTime desc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

}
