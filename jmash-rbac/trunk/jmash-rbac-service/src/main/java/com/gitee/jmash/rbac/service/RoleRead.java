package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.RoleEntity;
import com.gitee.jmash.rbac.entity.RolesDutyEntity;
import com.gitee.jmash.rbac.model.TreeResult;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.RoleReq;
import jmash.rbac.protobuf.VerifyRoleReq;

/**
 * 角色/职务表 rbac_role服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface RoleRead extends TenantService {

  /** 根据主键查询. */
  public RoleEntity findById(@NotNull UUID entityId);

  /** 综合查询. */
  public List<RoleEntity> findListByReq(@NotNull @Valid RoleReq req);

  /**
   * 获取用户角色ID和角色编码列表（含父角色）.
   *
   * @param userId 用户ID
   * @param scope 会话权限范围(动态角色冲突)
   */
  public Map<UUID, String> findUserRoleMap(UUID userId, String scope);

  /**
   * 获取用户角色ID和角色编码列表（含父角色）.
   *
   * @param userId 用户ID
   * @param scope 会话权限范围(动态角色冲突)
   */
  public Map<UUID, String> findUserRoleMap(UUID userId, Set<String> scope);

  /** 角色下拉选择树数据. */
  public List<TreeResult> findTreeList(RoleReq roleReq);

  /** 角色职责分离实体. */
  public List<RolesDutyEntity> findDutyList(UUID roleId);

  /** 校验角色编码是否存在. */
  public boolean checkRoleCode(@NotNull VerifyRoleReq req);

  /** 查询动态互斥角色列表 */
  public List<RoleEntity> getDsdRoleList();
  
  /** 检查是否系统角色登录(无需验证码). */
  public boolean checkSystemRole(@NotBlank String directoryId, @NotBlank String userName);
}
