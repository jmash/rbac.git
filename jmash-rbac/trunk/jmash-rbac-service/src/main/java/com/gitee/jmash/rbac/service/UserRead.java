package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.TokenEntity;
import com.gitee.jmash.rbac.entity.TokenEntity.TokenPk;
import com.gitee.jmash.rbac.entity.UserEntity;
import com.gitee.jmash.rbac.model.UserTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import jmash.protobuf.TenantReq;
import jmash.rbac.protobuf.UserDeptJobInfoRes;
import jmash.rbac.protobuf.UserExportReq;
import jmash.rbac.protobuf.UserInfo;
import jmash.rbac.protobuf.UserInfoPage;
import jmash.rbac.protobuf.UserKey;
import jmash.rbac.protobuf.UserModel;
import jmash.rbac.protobuf.UserReq;
import jmash.rbac.protobuf.VerifyUserReq;
import org.eclipse.microprofile.jwt.JsonWebToken;

/**
 * 用户 rbac_user服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface UserRead extends TenantService {

  /** 根据主键查询. */
  public UserEntity findById(@NotNull UUID entityId);

  /** 根据主键获取前端用户模型. */
  public UserModel findUserById(UUID entityId);

  /** 查询页信息. */
  public DtoPage<UserEntity, UserTotal> findPageByReq(@NotNull @Valid UserReq req);

  /** 综合查询. */
  public List<UserEntity> findListByReq(@NotNull @Valid UserReq req);

  /** 查询用户. */
  public UserEntity findByUserName(@NotBlank String directoryId, @NotBlank String userName);

  /** 用户名/手机号/邮箱名称是否已存在. */
  public boolean existUserName(@NotNull @Valid VerifyUserReq req);

  /** 查询directoryId可选列表. */
  public List<String> findDirectoryIds();

  /** 查询用户列表. */
  public List<UserInfo> findUserListByReq(UserReq req);

  /** 查询用户信息分页. */
  public UserInfoPage findUserPageByReq(UserReq req);

  /** 登录. */
  public UserEntity loginByUserName(@NotBlank String directoryId, @NotBlank String userName,
      @NotBlank String pwd);

  /** 根据主键查询. */
  public TokenEntity findTokenById(@NotNull TokenPk entityId);

  /** access_token 查询. */
  public TokenEntity findTokenByAccessToken(@NotNull String clientId, @NotNull String accessToken);

  /** 当前登录用户Token实体 */
  public TokenEntity findTokenByWebToken(@NotNull JsonWebToken webToken);

  /** 下载用户导入模板,返回相对文件路径. */
  public String downloadUserTemplate(TenantReq request) throws FileNotFoundException, IOException;

  /** 导出,返回相对文件路径. */
  public String exportUser(UserExportReq request) throws FileNotFoundException, IOException;

  /** 打印,返回相对文件路径. */
  public String printUser(UserExportReq request) throws FileNotFoundException, IOException;

  /** 获取用户信息和部门岗位信息. */
  public UserDeptJobInfoRes getUserDeptInfo(UserKey req);

}
