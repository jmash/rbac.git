
package com.gitee.jmash.rbac.mapper;

import com.crenjoy.proto.mapper.BeanMapper;
import com.crenjoy.proto.mapper.ProtoMapper;
import com.gitee.jmash.rbac.entity.OpensEntity;
import com.gitee.jmash.rbac.entity.OpensEntity.OpensPk;
import java.util.List;
import jmash.rbac.protobuf.OpensKey;
import jmash.rbac.protobuf.OpensModel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * OpensMapper.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Mapper
public interface OpensMapper extends BeanMapper,ProtoMapper {

  OpensMapper INSTANCE = Mappers.getMapper(OpensMapper.class);
  
  List<OpensModel> listOpens(List<OpensEntity> list);
    
  OpensModel model(OpensEntity entity);
  
  OpensPk pk(OpensKey key);  
  
  OpensKey key(OpensModel model);  

  OpensEntity clone(OpensEntity entity);
  
  
}
