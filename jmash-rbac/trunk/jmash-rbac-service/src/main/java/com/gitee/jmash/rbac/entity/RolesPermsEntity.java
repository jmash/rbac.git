
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.rbac.entity.RolesPermsEntity.RolesPermsPk;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;


/**
 * 本代码为自动生成工具生成 角色权限表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_roles_perms")
@IdClass(RolesPermsPk.class)
public class RolesPermsEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 角色ID. */
  private UUID roleId = UUID.randomUUID();
  /** 权限ID. */
  private UUID permId = UUID.randomUUID();

  /**
   * 默认构造函数.
   */
  public RolesPermsEntity() {
    super();
  }

  public RolesPermsEntity(UUID roleId, UUID permId) {
    super();
    this.roleId = roleId;
    this.permId = permId;
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public RolesPermsPk getEntityPk() {
    RolesPermsPk pk = new RolesPermsPk();
    pk.setRoleId(this.roleId);
    pk.setPermId(this.permId);
    return pk;
  }

  /** 实体主键. */
  public void setEntityPk(RolesPermsPk pk) {
    this.roleId = pk.getRoleId();
    this.permId = pk.getPermId();
  }

  /** 角色ID(RoleId). */
  @Id
  @Column(name = "role_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getRoleId() {
    return roleId;
  }

  /** 角色ID(RoleId). */
  public void setRoleId(UUID roleId) {
    this.roleId = roleId;
  }

  /** 权限ID(PermId). */
  @Id
  @Column(name = "perm_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getPermId() {
    return permId;
  }

  /** 权限ID(PermId). */
  public void setPermId(UUID permId) {
    this.permId = permId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getEntityPk() == null) ? 0 : getEntityPk().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final RolesPermsEntity other = (RolesPermsEntity) obj;
    if (getEntityPk() == null) {
      if (other.getEntityPk() != null) {
        return false;
      }
    } else if (!getEntityPk().equals(other.getEntityPk())) {
      return false;
    }
    return true;
  }


  /**
   * RolesPermsPK（直接对应数据库的配置 由生成工具产生）.
   *
   * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
   */
  @Embeddable
  public static class RolesPermsPk implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 角色ID. */
    @NotNull
    private UUID roleId;
    /** 权限ID. */
    @NotNull
    private UUID permId;

    public RolesPermsPk() {
      super();
    }

    public RolesPermsPk(UUID roleId, UUID permId) {
      super();
      this.roleId = roleId;
      this.permId = permId;
    }

    /** 角色ID(RoleId). */
    @Column(name = "role_id", nullable = false)
    public UUID getRoleId() {
      return roleId;
    }

    /** 角色ID(RoleId). */
    public void setRoleId(UUID roleId) {
      this.roleId = roleId;
    }

    /** 权限ID(PermId). */
    @Column(name = "perm_id", nullable = false)
    public UUID getPermId() {
      return permId;
    }

    /** 权限ID(PermId). */
    public void setPermId(UUID permId) {
      this.permId = permId;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
      result = prime * result + ((permId == null) ? 0 : permId.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final RolesPermsPk other = (RolesPermsPk) obj;

      if (roleId == null) {
        if (other.roleId != null) {
          return false;
        }
      } else if (!roleId.equals(other.roleId)) {
        return false;
      }
      if (permId == null) {
        if (other.permId != null) {
          return false;
        }
      } else if (!permId.equals(other.permId)) {
        return false;
      }
      return true;
    }
  }

}
