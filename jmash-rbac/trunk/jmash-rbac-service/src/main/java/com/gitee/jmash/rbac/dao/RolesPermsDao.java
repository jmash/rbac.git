
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.RolesPermsEntity;
import com.gitee.jmash.rbac.entity.RolesPermsEntity.RolesPermsPk;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * RolesPerms实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class RolesPermsDao extends BaseDao<RolesPermsEntity, RolesPermsPk> {

  public RolesPermsDao() {
    super();
  }

  public RolesPermsDao(TenantEntityManager tem) {
    super(tem);
  }

  /** 获取角色对应的操作 */
  public List<String> findRolePerms(UUID roleId) {
    String sql = "select p.permCode from PermEntity p where p.permId in (select s.permId from RolesPermsEntity s where s.roleId = ?1)";
    return this.findList(sql,String.class,roleId);
  }
  
  /** 移除角色权限. */
  public int removeByRoleId(UUID roleId) {
	  String sql="delete from RolesPermsEntity s where s.roleId = ?1 ";
	  return this.executeUpdate(sql, roleId);
  }

  /** 移除权限. */
  public int removeByPermId(UUID permId) {
    String sql="delete from RolesPermsEntity s where s.permId = ?1 ";
    return this.executeUpdate(sql, permId);
  }
  
  /** 更新角色权限. */
  public void updateRolesPerms(UUID roleId,Set<UUID> permIds){
	  this.removeByRoleId(roleId);
	  for(UUID permId: permIds) {
		  RolesPermsEntity entity=new RolesPermsEntity(roleId,permId);
		  this.persist(entity);
	  }
  }

}
