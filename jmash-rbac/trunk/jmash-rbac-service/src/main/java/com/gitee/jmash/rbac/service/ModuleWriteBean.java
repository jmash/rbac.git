
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.mapper.ModuleMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.ModuleCreateReq;
import jmash.rbac.protobuf.ModuleMoveKey;
import jmash.rbac.protobuf.ModuleUpdateReq;

/**
 * 系统模块 rbac_module写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(ModuleWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class ModuleWriteBean extends ModuleReadBean implements ModuleWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public ModuleEntity insert(ModuleCreateReq module) {
    ModuleEntity entity = ModuleMapper.INSTANCE.create(module);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (module.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(module.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }
    entity.setOrderBy(moduleDao.getMaxOrderBy(Collections.emptyMap()));
    // 4.执行业务(创建人及时间内部处理.)
    moduleDao.persist(entity);
    return entity;
  }

  @Override
  public ModuleEntity update(ModuleUpdateReq req) {
    ModuleEntity entity =
        moduleDao.find(UUIDUtil.fromString(req.getModuleId()), req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getModuleId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    moduleDao.merge(entity);
    return entity;
  }

  @Override
  public ModuleEntity delete(UUID entityId) {
    boolean isRelation = resourceDao.isRelationModule(entityId);
    if (isRelation) {
      throw new ValidationException("模块存在关联不能删除");
    } else {
      ModuleEntity entity = moduleDao.removeById(entityId);
      return entity;
    }
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      boolean isRelation = resourceDao.isRelationModule(entityId);
      if (!isRelation) {
        moduleDao.removeById(entityId);
        i++;
      }
    }
    return i;
  }

  @Override
  public boolean moveOrderBy(ModuleMoveKey moduleMoveKey) {
    UUID entityId = UUIDUtil.fromString(moduleMoveKey.getModuleId());
    ModuleEntity entity = findById(entityId);
    if (null == entity) {
      throw new ValidationException("找不到实体:" + entityId.toString());
    }
    return moduleDao.moveOrderBy(moduleMoveKey.getUp(), entity,Collections.emptyMap());
  }
}
