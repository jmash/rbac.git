
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.core.orm.jpa.entity.OrderBy;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.UUID;


 /**
 * 本代码为自动生成工具生成 系统模块表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_module")
public class ModuleEntity  implements OrderBy<UUID>  {

  private static final long serialVersionUID = 1L;
  
  /** 模块ID. */
  private UUID moduleId= UUID.randomUUID()  ;
  /** 模块编码. */
  private String moduleCode ;
  /** 模块名称. */
  private String moduleName ;
  /** 模块排序. */
  private Integer orderBy = 1;
  /** 模块描述. */
  private String description ;

  /**
  * 默认构造函数.
  */
  public ModuleEntity(){
    super();
  }
  
  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID  getEntityPk(){
    return this.moduleId;
  }
  /** 实体主键. */
  public void setEntityPk(UUID  pk){
    this.moduleId=pk;
  }
  
   /** 模块ID(ModuleId). */
  @Id
  @Column(name="module_id",nullable = false,columnDefinition = "BINARY(16)")
  public UUID getModuleId() {
    return moduleId;
  }
  /** 模块ID(ModuleId). */
  public void setModuleId(UUID moduleId) {
    this.moduleId = moduleId;
  }
   /** 模块编码(ModuleCode). */

  @Column(name="module_code",nullable = false)
  public String getModuleCode() {
    return moduleCode;
  }
  /** 模块编码(ModuleCode). */
  public void setModuleCode(String moduleCode) {
    this.moduleCode = moduleCode;
  }
   /** 模块名称(ModuleName). */

  @Column(name="module_name",nullable = false)
  public String getModuleName() {
    return moduleName;
  }
  /** 模块名称(ModuleName). */
  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }
   /** 模块排序(OrderBy). */

  @Column(name="order_by")
  public Integer getOrderBy() {
    return orderBy;
  }
  /** 模块排序(OrderBy). */
  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }
   /** 模块描述(Description). */
  @Column(name="description_")
  public String getDescription() {
    return description;
  }
  /** 模块描述(Description). */
  public void setDescription(String description) {
    this.description = description;
  }
	
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
	result = prime * result + ((moduleId == null) ? 0 : moduleId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ModuleEntity other = (ModuleEntity) obj;
			if (moduleId == null) {
				if (other.moduleId != null){
				  return false;
				}
			} else if (!moduleId.equals(other.moduleId)){
			  return false;
			}
    return true;
  }
  
  

}
