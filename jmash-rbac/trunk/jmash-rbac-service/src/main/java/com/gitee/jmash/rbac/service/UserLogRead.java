package com.gitee.jmash.rbac.service;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.UserLogEntity;
import com.gitee.jmash.rbac.entity.UserLogEntity.UserLogPk;
import com.gitee.jmash.rbac.model.UserLogTotal;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import jmash.rbac.protobuf.UserLogExportReq;
import jmash.rbac.protobuf.UserLogReq;

 /**
 * 安全日志 rbac_user_log服务Read接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface UserLogRead extends TenantService {

  /** 根据主键查询. */
  public UserLogEntity findById(@NotNull UserLogPk entityId);

  /** 查询页信息. */
  public DtoPage<UserLogEntity,UserLogTotal> findPageByReq(@NotNull @Valid UserLogReq req);

  /** 综合查询. */
  public List<UserLogEntity> findListByReq(@NotNull @Valid UserLogReq req);

  /** 导出日志. */
  public String exportUserLog(UserLogExportReq request) throws FileNotFoundException, IOException;
 }
