
package com.gitee.jmash.rbac;

import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperties;

/**
 * 认证配置
 *
 * @author CGD
 *
 */
@ConfigProperties(prefix = "jmash.auth")
@ApplicationScoped
public class AuthProps {

  /** 登录账户锁定，失败次数. */
  private int lockFailCount = 5;
  /** 失败登录锁间隔时间(秒)2小时. */
  private int loginFailUnlockInterval = 2 * 60 * 60;
  /** Token/Session 过期失效时间 7200秒 2小时. */
  private int expiresIn = 7200;

  public int getLockFailCount() {
    return lockFailCount;
  }

  public void setLockFailCount(int lockFailCount) {
    this.lockFailCount = lockFailCount;
  }

  public int getLoginFailUnlockInterval() {
    return loginFailUnlockInterval;
  }

  public void setLoginFailUnlockInterval(int loginFailUnlockInterval) {
    this.loginFailUnlockInterval = loginFailUnlockInterval;
  }

  public int getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(int expiresIn) {
    this.expiresIn = expiresIn;
  }
  
}
