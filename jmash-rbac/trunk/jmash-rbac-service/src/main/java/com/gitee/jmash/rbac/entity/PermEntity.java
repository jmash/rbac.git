
package com.gitee.jmash.rbac.entity;


import com.gitee.jmash.common.text.StringUtil;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.io.Serializable;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;


/**
 * 本代码为自动生成工具生成 权限表表 .
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Entity
@Table(name = "rbac_perm")
public class PermEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 权限ID. */
  private UUID permId = UUID.randomUUID();
  /** 权限编码. */
  private String permCode;
  /** 权限名称. */
  private String permName;

  /** 权限前缀,例如： rbac:user:add, rabc:user_log:add; */
  public static String permCodePrefix(final String moduleCode, final String resourceCode) {
    String resource = StringUtils.indexOf(resourceCode, "_") >= 0 ? resourceCode.toLowerCase()
        : StringUtil.camelToUnderline(resourceCode);
    return StringUtil.camelToUnderline(moduleCode) + ":" + resource + ":";
  }

  /**
   * 默认构造函数.
   */
  public PermEntity() {
    super();
  }

  /** 实体主键. */
  @Transient
  @JsonbTransient
  public UUID getEntityPk() {
    return this.permId;
  }

  /** 实体主键. */
  public void setEntityPk(UUID pk) {
    this.permId = pk;
  }

  /** 权限ID(PermId). */
  @Id
  @Column(name = "perm_id", nullable = false, columnDefinition = "BINARY(16)")
  public UUID getPermId() {
    return permId;
  }

  /** 权限ID(PermId). */
  public void setPermId(UUID permId) {
    this.permId = permId;
  }

  /** 权限编码(PermCode). */

  @Column(name = "perm_code", nullable = false)
  public String getPermCode() {
    return permCode;
  }

  /** 权限编码(PermCode). */
  public void setPermCode(String permCode) {
    this.permCode = permCode;
  }

  /** 权限名称(PermName). */

  @Column(name = "perm_name", nullable = false)
  public String getPermName() {
    return permName;
  }

  /** 权限名称(PermName). */
  public void setPermName(String permName) {
    this.permName = permName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((permId == null) ? 0 : permId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final PermEntity other = (PermEntity) obj;
    if (permId == null) {
      if (other.permId != null) {
        return false;
      }
    } else if (!permId.equals(other.permId)) {
      return false;
    }
    return true;
  }



}
