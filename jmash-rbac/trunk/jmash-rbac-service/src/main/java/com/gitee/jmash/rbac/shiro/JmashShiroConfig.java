package com.gitee.jmash.rbac.shiro;

import com.gitee.jmash.core.shiro.cache.RedisCacheManager;
import com.gitee.jmash.core.shiro.cache.RedisSessionDao;
import com.gitee.jmash.rbac.client.shiro.authc.JmashCredentialsMatcher;
import com.gitee.jmash.rbac.client.shiro.session.JmashSessionFactory;
import com.gitee.jmash.rbac.shiro.authz.JmashAuthorizingRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.session.mgt.DefaultSessionManager;

public class JmashShiroConfig {

  /**
   * Shiro 配置
   */
  public static void config() {
    // 认证授权
    JmashAuthorizingRealm realm = new JmashAuthorizingRealm();
    JmashCredentialsMatcher hcm = new JmashCredentialsMatcher();
    realm.setCredentialsMatcher(hcm);

    // Session存储
    DefaultSessionManager dsm = new DefaultSessionManager();
    dsm.setGlobalSessionTimeout((long)7200 * 1000);
    dsm.setSessionDAO(new RedisSessionDao());
    dsm.setSessionFactory(new JmashSessionFactory());

    // 安全工具类
    DefaultSecurityManager sdm = new DefaultSecurityManager();
    sdm.setRealm(realm);
    sdm.setSessionManager(dsm);
   

    RedisCacheManager cacheManager = new RedisCacheManager();
    cacheManager.init();
    sdm.setCacheManager(cacheManager);

    SecurityUtils.setSecurityManager(sdm);
  }

}
