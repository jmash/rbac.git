
package com.gitee.jmash.rbac.service;

import com.gitee.jmash.common.lock.DistributedLock;
import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.jaxrs.ParamsValidationException;
import com.gitee.jmash.core.orm.cdi.JpaTenantService;
import com.gitee.jmash.core.transaction.JakartaTransaction;
import com.gitee.jmash.core.utils.FieldMaskUtil;
import com.gitee.jmash.rbac.entity.OperationEntity;
import com.gitee.jmash.rbac.entity.PermEntity;
import com.gitee.jmash.rbac.mapper.OperationMapper;
import jakarta.enterprise.inject.Typed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import jakarta.transaction.Transactional.TxType;
import jakarta.validation.ValidationException;
import jakarta.validation.executable.ValidateOnExecution;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import jmash.rbac.protobuf.OperationCreateReq;
import jmash.rbac.protobuf.OperationMoveKey;
import jmash.rbac.protobuf.OperationUpdateReq;

/**
 * 操作表 rbac_operation写服务.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
@Typed(OperationWrite.class)
@Transactional(TxType.REQUIRED)
@JpaTenantService
@ValidateOnExecution
public class OperationWriteBean extends OperationReadBean
    implements OperationWrite, JakartaTransaction {

  @Inject
  DistributedLock lock;

  @Override
  @PersistenceContext(unitName = "WriteRbac")
  public void setEntityManager(EntityManager entityManager) {
    this.tem.setEntityManager(entityManager, true);
  }

  @Override
  public OperationEntity insert(OperationCreateReq operation) {
    OperationEntity entity = OperationMapper.INSTANCE.create(operation);
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (operation.getValidateOnly()) {
      return entity;
    }
    // 3.检查是否重复请求.
    if (!lock.lock(operation.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    Integer orderBy = operationDao.getMaxOrderBy(Collections.emptyMap());
    entity.setOrderBy(orderBy);
    // 4.执行业务(创建人及时间内部处理.)
    operationDao.persist(entity);
    return entity;
  }

  @Override
  public OperationEntity update(OperationUpdateReq req) {
    OperationEntity entity =
        operationDao.find(UUIDUtil.fromString(req.getOperationId()), req.getValidateOnly());
    if (null == entity) {
      throw new ValidationException("找不到实体:" + req.getOperationId());
    }
    // 无需更新,返回当前数据库数据.
    if (req.getUpdateMask().getPathsCount() == 0) {
      return entity;
    }
    // 更新掩码属性
    FieldMaskUtil.copyMask(entity, req, req.getUpdateMask());
    // 1.业务校验.
    // 2.仅校验,不执行.
    if (req.getValidateOnly()) {
      return entity;
    }

    // 3.检查是否重复请求.
    if (!lock.lock(req.getRequestId(), 60)) {
      throw new ParamsValidationException("requestId", "客户端发起重复请求");
    }

    // 4.执行业务
    operationDao.merge(entity);
    return entity;
  }

  @Override
  public OperationEntity delete(UUID entityId) {
    if (hasRelation(entityId)) {
      throw new ValidationException("操作存在关联，不能删除！");
    }
    OperationEntity entity = operationDao.removeById(entityId);
    return entity;
  }

  @Override
  public Integer batchDelete(Set<UUID> entityIds) {
    int i = 0;
    for (UUID entityId : entityIds) {
      if (!hasRelation(entityId)) {
        operationDao.removeById(entityId);
        i++;
      }
    }
    return i;
  }

  /** 检查是否存在关联数据. */
  private boolean hasRelation(UUID entityId) {
    OperationEntity entity = findById(entityId);
    // 根据编码检查关联数据
    List<PermEntity> permList = permDao.findByOperationCode(entity.getOperationCode());
    if (permList.size() > 0) {
      return true;
    }
    return false;
  }

  @Override
  public boolean moveOrderBy(OperationMoveKey operationMoveKey) {
    UUID entityId = UUIDUtil.fromString(operationMoveKey.getOperationId());
    OperationEntity entity = findById(entityId);
    if (null == entity) {
      throw new ValidationException("找不到实体:" + entityId.toString());
    }
    return operationDao.moveOrderBy(operationMoveKey.getUp(), entity,Collections.emptyMap());
  }

}
