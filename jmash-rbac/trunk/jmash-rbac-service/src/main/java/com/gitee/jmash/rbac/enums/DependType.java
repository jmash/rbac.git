package com.gitee.jmash.rbac.enums;

public enum DependType {
	
	/** 前端权限依赖 */
	frontend,
	
	/** 后端权限依赖 */
	backend;
	
}
