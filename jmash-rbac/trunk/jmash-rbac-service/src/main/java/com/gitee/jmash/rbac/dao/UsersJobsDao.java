
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.common.utils.UUIDUtil;
import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.UsersJobsEntity;
import com.gitee.jmash.rbac.entity.UsersJobsEntity.UsersJobsPk;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import jmash.rbac.protobuf.UserJobs;
import org.apache.commons.lang3.StringUtils;

/**
 * UsersJobs实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class UsersJobsDao extends BaseDao<UsersJobsEntity, UsersJobsPk> {

  public UsersJobsDao() {
    super();
  }

  public UsersJobsDao(TenantEntityManager tem) {
    super(tem);
  }

  /** 获取用户岗位. */
  public List<UUID> findUserRoles(UUID userId) {
    String sql = "select s.jobId as roleId from UsersJobsEntity s where  s.userId = ?1 ";
    return this.findList(sql, UUID.class, userId);
  }

  /** 获取用户部门岗位. */
  public List<UsersJobsEntity> findUserJobs(UUID userId) {
    String sql =
        "select s from UsersJobsEntity s where s.userId = ?1 and s.status = true order by s.orderBy asc ";
    return this.findList(sql, UsersJobsEntity.class, userId);
  }

  /** 获取用户部门岗位. */
  public List<UsersJobsEntity> findUserJobs(UUID deptId, UUID userId) {
    String sql =
        "select s from UsersJobsEntity s where s.userId = ?1 and s.deptId = ?2 and s.status = true order by s.orderBy asc ";
    return this.findList(sql, UsersJobsEntity.class, userId, deptId);
  }

  /** 获取部门是否关联用户. */
  public int hasExistUserByDeptID(UUID deptId) {
    String sql = "select count(s) from UsersJobsEntity s where s.deptId = ?1 and s.status = true ";
    return Integer.parseInt(this.findSingleResult(sql, deptId).toString());
  }

  /** 获取岗位是否关联用户. */
  public int hasExistJobByJobId(UUID jobId) {
    String sql = "select count(s) from UsersJobsEntity s where s.jobId = ?1 and s.status = true ";
    return Integer.parseInt(this.findSingleResult(sql, jobId).toString());
  }

  /** 移除用户部门岗位. */
  public int removeByUserId(UUID userId) {
    // 避免死锁 https://blog.csdn.net/weixin_39183543/article/details/134777281
    List<UsersJobsEntity> list =
        this.findList("from UsersJobsEntity s where s.userId = ?1 ", userId);
    return this.removeAll(list);
  }

  /** 新增用户部门岗位. */
  public void addUserDeptJob(UUID userId, List<String> deptIds, UUID jobId) {
    if (null == deptIds || deptIds.isEmpty()) {
      return;
    }
    if (jobId == null) {
      return;
    }
    for (int i = 1; i <= deptIds.size(); i++) {
      String deptId = deptIds.get(i - 1);
      if (StringUtils.isBlank(deptId)) {
        continue;
      }
      UsersJobsEntity entity = new UsersJobsEntity();
      entity.setUserId(userId);
      entity.setDeptId(UUIDUtil.fromString(deptId));
      entity.setJobId(jobId);
      entity.setOrderBy(i);
      entity.setStartTime(LocalDateTime.now());
      entity.setStatus(true);
      this.persist(entity);
    }
  }

  /** 新增用户部门岗位. */
  public void addUserJobs(UUID userId, List<UserJobs> userJobsList) {
    if (null == userJobsList || userJobsList.isEmpty()) {
      return;
    }
    for (int i = 1; i <= userJobsList.size(); i++) {
      UserJobs userJobs = userJobsList.get(i - 1);
      if (StringUtils.isBlank(userJobs.getDeptId()) || StringUtils.isBlank(userJobs.getJobId())) {
        continue;
      }
      UsersJobsEntity entity = new UsersJobsEntity();
      entity.setUserId(userId);
      entity.setDeptId(UUIDUtil.fromString(userJobs.getDeptId()));
      entity.setJobId(UUIDUtil.fromString(userJobs.getJobId()));
      entity.setOrderBy(i);
      entity.setStartTime(LocalDateTime.now());
      entity.setStatus(true);
      this.persist(entity);
    }
  }

  /** 更新用户部门岗位. */
  public void updateUserJobs(UUID userId, List<UserJobs> userJobsList) {
    this.removeByUserId(userId);
    this.addUserJobs(userId, userJobsList);
  }

}
