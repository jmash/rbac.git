package com.gitee.jmash.rbac.service;

import java.util.Set;
import java.util.UUID;
import com.gitee.jmash.core.orm.tenant.TenantService;
import com.gitee.jmash.rbac.entity.ResourceEntity;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jmash.rbac.protobuf.ResourceCreateReq;
import jmash.rbac.protobuf.ResourceImportReq;
import jmash.rbac.protobuf.ResourceMoveKey;
import jmash.rbac.protobuf.ResourceUpdateReq;

/**
 * 资源表 rbac_resource服务Write接口.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public interface ResourceWrite extends TenantService {

  /** 插入实体. */
  public ResourceEntity insert(@NotNull @Valid ResourceCreateReq resource);

  /** update 实体. */
  public ResourceEntity update(@NotNull @Valid ResourceUpdateReq resource);

  /** 根据主键删除. */
  public ResourceEntity delete(@NotNull UUID entityId);

  /** 根据主键数组删除. */
  public Integer batchDelete(@NotNull @Valid Set<@NotNull UUID> entityIds);
  
  /** 导入资源. */
  public String importResource(ResourceImportReq req) throws Exception;

  /** 移动排序.*/
  public boolean moveOrderBy(@NotNull ResourceMoveKey req) throws Exception;
  
}
