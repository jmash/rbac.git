
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.jpa.BaseDao;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.UserSecretEntity;
import java.util.UUID;

/**
 * UserSecret实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class UserSecretDao extends BaseDao<UserSecretEntity, UUID> {

  public UserSecretDao() {
    super();
  }

  public UserSecretDao(TenantEntityManager tem) {
    super(tem);
  }

  /** 业务主键查询. */
  public UserSecretEntity findByKey(UUID userId, String secretType) {
    String sql = "select s from UserSecretEntity s where s.userId = ?1 and s.secretType = ?2";
    return this.findSingle(sql, userId, secretType);
  }

}
