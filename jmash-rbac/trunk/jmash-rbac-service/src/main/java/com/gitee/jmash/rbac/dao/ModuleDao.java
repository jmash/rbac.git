
package com.gitee.jmash.rbac.dao;

import com.gitee.jmash.core.orm.DtoPage;
import com.gitee.jmash.core.orm.jpa.OrderBaseDao;
import com.gitee.jmash.core.orm.jpa.SqlBuilder;
import com.gitee.jmash.core.orm.jpa.TenantEntityManager;
import com.gitee.jmash.rbac.entity.ModuleEntity;
import com.gitee.jmash.rbac.model.ModuleTotal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import jmash.rbac.protobuf.ModuleReq;

/**
 * Module实体的Dao层（使用JPA实现）.
 *
 * @author <a href="mailto:service@crenjoy.com">crenjoy</a>
 */
public class ModuleDao extends OrderBaseDao<ModuleEntity, UUID> {

  public ModuleDao() {
    super();
  }

  public ModuleDao(TenantEntityManager tem) {
    super(tem);
  }

  /**
   * 查询Map.
   */
  public Map<UUID, ModuleEntity> findMapByReq(ModuleReq req) {
    List<ModuleEntity> list = findListByReq(req);
    return list.stream().collect(
        Collectors.toMap(ModuleEntity::getModuleId, module -> module, (key1, key2) -> key1));
  }

  /**
   * 查询Map Code:Id.
   */
  public Map<String, String> findMapCodeIdByReq(ModuleReq req) {
    List<ModuleEntity> list = findListByReq(req);
    return list.stream().collect(Collectors.toMap(ModuleEntity::getModuleCode,
        module -> module.getModuleId().toString(), (key1, key2) -> key1));
  }


  /**
   * 综合查询.
   */
  public List<ModuleEntity> findListByReq(ModuleReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    return this.findListByParams(query, sqlBuilder.getParams());
  }

  /**
   * 综合查询Page.
   */
  public DtoPage<ModuleEntity, ModuleTotal> findPageByReq(ModuleReq req) {
    SqlBuilder sqlBuilder = createSql(req);
    String query = sqlBuilder.getQuerySql("select s ");
    String totalQuery = sqlBuilder.getTotalSql("select count(s) as totalSize ");
    return this.findDtoPageByParams(req.getCurPage(), req.getPageSize(), query, totalQuery,
        ModuleTotal.class, sqlBuilder.getParams());
  }

  /** Create SQL By Req . */
  public SqlBuilder createSql(ModuleReq req) {
    StringBuilder sql = new StringBuilder(" from ModuleEntity s where 1=1  ");
    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(req.getModuleCode())) {
      sql.append(" and s.moduleCode = :moduleCode ");
      params.put("moduleCode", req.getModuleCode());
    }

    if (StringUtils.isNotBlank(req.getLikeModuleName())) {
      sql.append(" and s.moduleName like :moduleName ");
      params.put("moduleName", "%" + req.getLikeModuleName() + "%");
    }

    String orderSql = " order by s.orderBy asc ";
    if (StringUtils.isNotBlank(req.getOrderName())) {
      orderSql = String.format(" order by %s %s ", req.getOrderName(),
          req.getOrderAsc() ? " asc " : " desc ");
    }

    return SqlBuilder.build().setSql(sql).setParams(params).setOrderSql(orderSql);
  }

}
